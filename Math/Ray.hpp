//------------------------------------//
// 
// Ray.hpp
// 
// Author: ayufan (ayufan(at)o2.pl)
// Project: Core
// Date: 2007-08-14
// 
//------------------------------------//

#pragma once

#include <QDataStream>
#include "Math.hpp"
#include "Vec3.hpp"
#include "Matrix.hpp"

//------------------------------------//
// ray structure

struct MATH_EXPORT ray
{
	// Fields
	vec3 Origin, At;

	// Constructor
	ray() {
	}
	ray(const vec3 &origin, const vec3 &at);

	// Methods
	void assign(const vec3 &origin, const vec3 &at);
	ray transform(const matrix &m) const;
	vec3 hit(float time) const;

	// Friends
	bool operator == (const ray &b) const {
		return Origin == b.Origin && At == b.At;
	}
	bool operator != (const ray &b) const {
		return Origin != b.Origin || At != b.At;
	}

public:
    friend QDataStream &operator<<(QDataStream &ds, const ray &rhs)
    {
      return ds << rhs.Origin << rhs.At;
    }

    friend QDataStream &operator>>(QDataStream &ds, ray &rhs)
    {
      return ds >> rhs.Origin >> rhs.At;
    }
};

Q_DECLARE_METATYPE(ray)
