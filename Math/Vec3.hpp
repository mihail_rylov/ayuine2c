#ifndef VEC3_HPP
#define VEC3_HPP

#include <QDataStream>
#include "Math.hpp"
#include "Vec2.hpp"

#include <Core/Serializer.hpp>

struct MATH_EXPORT vec3
{
    // Fields
public:
    float X, Y, Z;

    // Constructors
public:
    vec3() {
        X = 0;
        Y = 0;
        Z = 0;
    }
    vec3(const float v[]) {
        X = v[0];
        Y = v[1];
        Z = v[2];
    }
    vec3(float _x, float _y, float _z) {
        X = _x;
        Y = _y;
        Z = _z;
    }
    vec3(float _z) {
        X = _z;
        Y = _z;
        Z = _z;
    }
    vec3(const vec2 &v, float _z){
        X = v.X;
        Y = v.Y;
        Z = _z;
    }

        // Operators
public:

    // Indexer
public:

    float &operator [] (unsigned index)	{
        return (&X)[index];
    }
    float operator [] (unsigned index) const	{
        return (&X)[index];
    }
#if 0
    operator bool () const {
        return !empty();
    }
#endif
    bool operator ! () const {
        return empty();
    }


    // Properties
public:
    float lengthSq() const {
        return X * X + Y * Y + Z * Z;
    }
    float length() const {
        return sqrt(X * X + Y * Y + Z * Z);
    }
    bool empty() const {
        return X == 0.0f && Y == 0.0f && Z == 0.0f;
    }

    // Methods
public:
    vec3 normalize() const {
        float temp;
        return normalize(temp);
    }
    vec3 normalize(float &l) const;
    vec3 rotateX(float angle) const;
    vec3 rotateY(float angle) const;
    vec3 rotateZ(float angle) const;
    vec3 orthogonalize(const vec3 &v) const;
    void normalVectors(vec3 &right) const;
    void normalVectors(vec3 &right, vec3 &up) const;
    void axis(vec3 &xv, vec3 &yv) const;
    vec3 reflect(const vec3 &normal) const;
    vec3 round(float ep) const;

    // Functions
public:
    static vec3 cubeVector(CubeFace::Enum face, float s, float t);
    static vec3 lerp(const vec3 &v1, const vec3 &v2, float amount = 0.5f);
    static vec3 lerp3(const vec3 &v1, const vec3 &v2, vec3 &v3, float amount = 0.5f);
    static vec3 minimize(const vec3 &v1, const vec3 &v2) {
        return vec3(qMin(v1.X, v2.X), qMin(v1.Y, v2.Y), qMin(v1.Z, v2.Z));
    }
    static vec3 maximize(const vec3 &v1, const vec3 &v2) {
        return vec3(qMax(v1.X, v2.X), qMax(v1.Y, v2.Y), qMax(v1.Z, v2.Z));
    }

    // Friends

    vec3 &operator += (const vec3 &v) {
        X += v.X;
        Y += v.Y;
        Z += v.Z;
        return *this;
    }
    vec3 &operator += (const float v)	{
        X += v;
        Y += v;
        Z += v;
        return *this;
    }
    vec3 &operator -= (const vec3 &v) {
        X -= v.X;
        Y -= v.Y;
        Z -= v.Z;
        return *this;
    }
    vec3 &operator -= (const float v)	{
        X -= v;
        Y -= v;
        Z -= v;
        return *this;
    }
    vec3 &operator *= (const vec3 &v)	{
        X *= v.X;
        Y *= v.Y;
        Z *= v.Z;
        return *this;
    }
    vec3 &operator *= (const float v)	{
        X *= v;
        Y *= v;
        Z *= v;
        return *this;
    }
    vec3 &operator /= (const vec3 &v) {
        X /= v.X;
        Y /= v.Y;
        Z /= v.Z;
        return *this;
    }
    vec3 &operator /= (const float v)	{
        X /= v;
        Y /= v;
        Z /= v;
        return *this;
    }

    vec3 operator + (const vec3 &b) const {
        return vec3(X + b.X, Y + b.Y, Z + b.Z);
    }
    vec3 operator + (const float b) const {
        return vec3(X + b, Y + b, Z + b);
    }
    vec3 operator - (const vec3 &b) const {
        return vec3(X - b.X, Y - b.Y, Z - b.Z);
    }
    vec3 operator - (const float b) const {
        return vec3(X - b, Y - b, Z - b);
    }
    vec3 operator * (const vec3 &b) const {
        return vec3(X * b.X, Y * b.Y, Z * b.Z);
    }
    vec3 operator * (const float b) const {
        return vec3(X * b, Y * b, Z * b);
    }
    vec3 operator / (const vec3 &b) const {
        return vec3(X / b.X, Y / b.Y, Z / b.Z);
    }
    vec3 operator / (const float b) const {
        return vec3(X / b, Y / b, Z / b);
    }
    float operator ^ (const vec3 &b) const {
        return X * b.X + Y * b.Y + Z * b.Z;
    }
    float operator ^ (const vec2 &b) const {
        return X * b.X + b.Y * Y + Z;
    }
    vec3 operator % (const vec3 &b) const {
        return vec3(Y * b.Z - b.Y * Z, Z * b.X - b.Z * X, X * b.Y - b.X * Y);
    }
    float operator % (const vec2 &b) const {
        return X * b.X + b.Y * Y;
    }
    bool operator == (const vec3 &b) const {
        return fabs(X - b.X) < Epsf && fabs(Y - b.Y) < Epsf && fabs(Z - b.Z) < Epsf;
    }
    bool operator != (const vec3 &b) const {
        return X != b.X || Y != b.Y || Z != b.Z;
    }
    bool operator < (const vec3 &b) const {
        return X - b.X < -Epsf && Y - b.Y < -Epsf && Z - b.Z < -Epsf;
    }

    static float dot(const vec3 &a, const vec3 &b) {
        return a.X * b.X + a.Y * b.Y + a.Z * b.Z;
    }
    static float dot(const vec3 &a, const vec2 &b) {
        return a.X * b.X + b.Y * a.Y + a.Z;
    }
    static vec3 cross(const vec3 &a, const vec3 &b) {
        return vec3(a.Y * b.Z - b.Y * a.Z, a.Z * b.X - b.Z * a.X, a.X * b.Y - b.X * a.Y);
    }
    static float cross(const vec3 &a, const vec2 &b) {
        return a.X * b.X + b.Y * a.Y;
    }

public:
    friend QDataStream &operator<<(QDataStream &ds, const vec3 &rhs)
    {
      return ds << rhs.X << rhs.Y << rhs.Z;
    }

    friend QDataStream &operator>>(QDataStream &ds, vec3 &rhs)
    {
      return ds >> rhs.X >> rhs.Y >> rhs.Z;
    }
};

MATH_EXPORT extern const vec3 vec3Zero;

inline vec3 operator + (const vec3 &v) {
    return vec3(+v.X, +v.Y, +v.Z);
}
inline vec3 operator - (const vec3 &v) {
    return vec3(-v.X, -v.Y, -v.Z);
}
inline vec3 operator + (const float a, const vec3 &b) {
    return vec3(a + b.X, a + b.Y, a + b.Z);
}
inline vec3 operator - (const float a, const vec3 &b) {
    return vec3(a - b.X, a - b.Y, a - b.Z);
}
inline vec3 operator * (const float a, const vec3 &b) {
    return vec3(a * b.X, a * b.Y, a * b.Z);
}
inline vec3 operator / (const float a, const vec3 &b) {
    return vec3(a / b.X, a / b.Y, a / b.Z);
}

typedef QVector<vec3> Vec3List;

inline void swapOrder(vec3& value)
{
  swapOrder(value.X);
  swapOrder(value.Y);
  swapOrder(value.Z);
}

Q_DECLARE_RAW_VECTOR_SERIALIZER(vec3)

Q_DECLARE_METATYPE(vec3)
Q_DECLARE_METATYPE(Vec3List)

#endif // VEC3_HPP
