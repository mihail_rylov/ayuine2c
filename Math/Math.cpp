#include "Math.hpp"
#include "Box.hpp"
#include "Euler.hpp"
#include "Matrix.hpp"
#include "Ray.hpp"
#include "Sphere.hpp"
#include "Vec2.hpp"
#include "Vec3.hpp"
#include "Vec4.hpp"

static int registerMathTypes()
{
  qRegisterMetaTypeStreamOperators<box>();
  qRegisterMetaTypeStreamOperators<euler>();
  qRegisterMetaTypeStreamOperators<matrix>();
  qRegisterMetaTypeStreamOperators<ray>();
  qRegisterMetaTypeStreamOperators<sphere>();
  qRegisterMetaTypeStreamOperators<vec2>();
  qRegisterMetaTypeStreamOperators<Vec2List>();
  qRegisterMetaTypeStreamOperators<vec3>();
  qRegisterMetaTypeStreamOperators<Vec3List>();
  qRegisterMetaTypeStreamOperators<vec4>();
  qRegisterMetaTypeStreamOperators<Vec4List>();
  return 0;
}

Q_CONSTRUCTOR_FUNCTION(registerMathTypes)
