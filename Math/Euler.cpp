#include "Euler.hpp"
#include <cmath>

//------------------------------------//
// euler: Constructor

euler::euler(const vec3 &dir, float r) {
    if(dir.X == 0 && dir.Y == 0) {
        Yaw = 0;
        Pitch = dir.Z > 0 ? Pi / 2.0f : 3.0f * Pi / 2.0f;
    }
    else {
        Yaw = atan2f(dir.Y, dir.X);
        if(Yaw < 0)
            Yaw = (Yaw + 2.0f * Pi);

        Pitch = (atan2f(dir.Z, vec2(dir.X, dir.Y).length()));
        if(Pitch < 0)
            Pitch = (Pitch + 2.0f * Pi);
    }
    Roll = (r);
}

euler::euler(const vec3 &dir, const vec3 &up) {
    if(dir.X == 0 && dir.Y == 0) {
        Yaw = (0);
        Pitch = ((float)(dir.Z > 0 ? Pi / 2.0f : 3.0f * Pi / 2.0f));
    }
    else {
        Yaw = (atan2f(dir.Y, dir.X));
        if(Yaw < 0)
            Yaw = (Yaw + 2.0f * Pi);

        Pitch = (atan2f(dir.Z, vec2(dir.X, dir.Y).length()));
        if(Pitch < 0)
            Pitch = (Pitch + 2.0f * Pi);
    }
    Roll = (0.0f);
}

//------------------------------------//
// euler: Methods

vec3 euler::at() const {
    float sp, sy, cp, cy;

    sy = sinf(-Yaw);
    cy = cosf(-Yaw);
    sp = sinf(-Pitch);
    cp = cosf(-Pitch);

    return vec3(cp * cy, cp * sy, -sp);
}

vec3 euler::right() const {
    float sp, sy, sr, cp, cy, cr;

    sy = sinf(-Yaw);
    cy = cosf(-Yaw);
    sp = sinf(-Pitch);
    cp = cosf(-Pitch);
    sr = sinf(-Roll);
    cr = cosf(-Roll);

    return vec3(- sr * sp * cy + cr * sy, - sr * sp * sy - cr * cy, -sr * cp);
}

vec3 euler::up() const {
    float sp, sy, sr, cp, cy, cr;

    sy = sinf(-Yaw);
    cy = cosf(-Yaw);
    sp = sinf(-Pitch);
    cp = cosf(-Pitch);
    sr = sinf(-Roll);
    cr = cosf(-Roll);

    return vec3(cr * sp * cy + sr * sy, cr * sp * sy - sr * cy, cr * cp);
}

vec3 euler::rotate(const vec3 &in) const {
    vec3 in2(in);

    if(Yaw != 0)
        in2 = in2.rotateZ(Yaw);

    if(Pitch != 0)
        in2 = in2.rotateY(Pitch);

    if(Roll != 0)
        in2 = in2.rotateX(Roll);

    return in2;
}

vec3 euler::rotate(const vec3 &in, const vec3 &around) const {
    vec3 in2(in - around);

    if(Yaw != 0)
        in2 = in2.rotateZ(Yaw);

    if(Pitch != 0)
        in2 = in2.rotateY(Pitch);

    if(Roll != 0)
        in2 = in2.rotateX(Roll);

    return in2 + around;
}
