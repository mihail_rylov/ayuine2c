//------------------------------------//
// 
// Poly.cpp
// 
// Author: ayufan (ayufan(at)o2.pl)
// Project: Core
// Date: 2007-08-14
// 
//------------------------------------//

#include "Poly.hpp"
#include <stdexcept>

//------------------------------------//
// Poly: Functions

vec4 Poly::plane(const PolyVerts &Poly) {
  return vec4(Poly.at(0), Poly.at(1), Poly.at(2));
}

vec4 Poly::plane(const vec3 *verts, unsigned count) {
  if(count < 3)
    throw std::runtime_error("more verts");
  return vec4(verts[0], verts[1], verts[2]);
}

PolyVerts Poly::build(const PolyVerts &points) {
  throw std::runtime_error("not supported");
}

PolyVerts Poly::portal(const vec4 &plane, float size) {
  vec3 origin, right, up;
  PolyVerts verts;

  // Pobierz wektory normalne
  plane.normal().normalVectors(right, up);

  // Oblicz wektory
  origin = plane.normal() * (-plane.W);
  right *= fabs(size);
  up *= fabs(size);

  // Oblicz wierzcho�ki
  if(size > 0) {
    verts.append(origin - right - up);
    verts.append(origin + right - up);
    verts.append(origin + right + up);
    verts.append(origin - right + up);
  }
  else {
    verts.append(origin - right + up);
    verts.append(origin + right + up);
    verts.append(origin + right - up);
    verts.append(origin - right - up);
  }

  return verts;
}

PolyVerts Poly::portal(const vec3 &at, const vec2& size, const vec3& origin)
{
  vec3 right, up;

  // Pobierz wektory normalne
  at.normalVectors(right, up);

  // Przeskaluj wierzcho�ki
  right *= size.X;
  up *= size.Y;

  // Oblicz portal
  PolyVerts verts;
  verts.append(origin - right - up);
  verts.append(origin + right - up);
  verts.append(origin + right + up);
  verts.append(origin - right + up);
  return verts;
}

PolyVerts Poly::portal(Axis::Enum axis, const vec2& size, const vec3& origin)
{
  vec3 at;
  at[axis] = 1.0f;
  return portal(at, size, origin);
}

Side::Enum Poly::split(const PolyVerts &Poly, const vec4 &plane, PolyVerts *front, PolyVerts *back) {
#if 0
  if(Poly.size() < 3)
    throw std::runtime_error("more verts");

  float *distsIter;
  char *sidesIter;
  const vec3 *polyIter;
  int side[3] = {0, 0, 0};

  // Zaalokuj dane
  float dists[sizeof(float) * (Poly.size() + 1)];
  char sides[sizeof(float) * (Poly.size() + 1)];
  distsIter = dists;
  sidesIter = sides;
  //distsIter = dists = (float*)_alloca(sizeof(float) * (Poly.size() + 1));
  //sidesIter = sides = (char*)_alloca(sizeof(char) * (Poly.size() + 1));
  polyIter = &Poly[0];

  // Sprawd�, ka�dy wierzcho�ek i zapisz informacje o nim
  for(int i = (int)Poly.size() - 1; i >= 0; i--) {
    side[*sidesIter++ = plane.test(*polyIter++, *distsIter++)]++;
  }

  // Wszystkie wierzcho�ki z przodu (czyli zwracamy przedni� cz��)
  if(side[Side::Front] != 0 && side[Side::Back] == 0) {
    if(front)
      *front = Poly;
    if(back)
      back->clear();
    return Side::Front;
  }

  // Wszystkie wierzcho�ki z ty�u (czyli zwracamy tylni� cz��)
  if(side[Side::Back] != 0 && side[Side::Front] == 0) {
    if(back)
      *back = Poly;
    if(front)
      front->clear();
    return Side::Back;
  }

  // Wszystkie wierzcho�ki na p�aszczyznie
  if(side[Side::Front] == 0 && side[Side::Back] == 0) {
    if(front)
      front->clear();
    if(back)
      back->clear();
    return Side::Front;
  }

  // Wierzcho�ki po obu stronach p�aszczyzny
  vec3 *frontTemp = front != nullptr ? (vec3*)_alloca(sizeof(vec3) * (Poly.size() + 4)) : nullptr;
  vec3 *frontIter = frontTemp;
  vec3 *backTemp = back != nullptr ? (vec3*)_alloca(sizeof(vec3) * (Poly.size() + 4)) : nullptr;
  vec3 *backIter = backTemp;

  // Popraw dane
  sides[Poly.size()] = sides[0];
  dists[Poly.size()] = dists[0];
  sidesIter = sides;
  distsIter = dists;
  polyIter = &Poly[0];

  // Dodaj wierzcho�ki do odpowiednich nowych polign�w
  for(int i = (int)Poly.size() - 1; i >= 0; i--, sidesIter++, distsIter++, polyIter++) {
    // Prz�d
    if((*sidesIter == Side::Front || *sidesIter == Side::On) && frontIter != nullptr)
      *frontIter++ = *polyIter;

    // Ty�
    if((*sidesIter == Side::Back || *sidesIter == Side::On) && backIter != nullptr)
      *backIter++ = *polyIter;

    // Na
    if(sidesIter[0] == sidesIter[1] || sidesIter[1] == Side::On)
      continue;

    // Przecina
    vec3 temp = vec3::lerp(polyIter[0], i > 0 ? polyIter[1] : Poly[0], distsIter[0] / (distsIter[0] - distsIter[1]));

    // Dodaj do przodu i do ty�u
    if(frontIter != nullptr)
      *frontIter++ = temp;
    if(backIter != nullptr)
      *backIter++ = temp;
  }
  // Zapisz podzielone poligony
  if(front)
    *front = PolyVerts(frontTemp, frontIter);
  if(back)
    *back = PolyVerts(backTemp, backIter);
  return Side::Split;
#endif
  return Side::Front;
}

PolyVerts Poly::add(unsigned count, ...) {
  throw std::runtime_error("not supported");
}

PolyVerts Poly::merge(unsigned count, ...) {
  throw std::runtime_error("not supported");
}

void Poly::removeCollinear(PolyVerts &Poly) {
  throw std::runtime_error("not supported");

  /*
  if (Poly.Length < 3)
   THROW new InvalidpolyException("Not enough Poly vertices");

  Vector3 prev = (Poly[Poly.Length - 1] - Poly[0]).Normalize;
  int count = 0;
  int* indices = stackalloc int[Poly.Length];
  float epsilon = 1.0f - MathLib.Ep;

  for(int i = 0; i < Poly.Length; i++)
  {
   Vector3 curr = (Poly[i == Poly.Length - 1 ? 0 : i + 1] - Poly[i]);
   float length = curr.Length;

   // Sprawdz odleglosc miedzy wierzcholkami
   if(length < MathLib.Ep)
    continue;

   // Znormalizuj wektor
   curr /= length;

   // Wierzcholek lezy na tej samej prostej
   if(MathLib.Abs(curr ^ prev) > epsilon)
    continue;

   indices[count++] = i;
   prev = curr;
  }

  if(count < 3)
   return null;
  if(count == Poly.Length)
   return Poly;

  Vector3[] newpoly = new Vector3[count];

  for(int i = count - 1; i >= 0; i--)
   newpoly[i] = Poly[indices[i]];

  return newpoly;
*/
}

float Poly::area(const PolyVerts &Poly) {
  if(Poly.size() < 3)
    throw std::runtime_error("more verts");

  float area = 0;

  for(int i = (int)Poly.size() - 1; i >= 2; i--) {
    float a = (Poly[0] - Poly[i - 1]).length();
    float b = (Poly[0] - Poly[i - 0]).length();
    float c = (Poly[i] - Poly[i - 1]).length();
    float p = (a + b + c) / 2.0f;

    area += sqrt(p * (p - a) * (p - b) * (p - c));
  }
  return area;
}

bool Poly::isTiny(const PolyVerts &Poly, float size) {
  if(Poly.size() < 3)
    throw std::runtime_error("more verts");

  const vec3*	curr = &Poly[0];
  vec3	prev = Poly[Poly.size() - 1];

  // Kwadrat
  size *= size;

  // Sprawd� ka�d� kraw�d�
  for(int i = (int)Poly.size() - 1; i >= 0; i--, prev = *curr++) {
    if((*curr - prev).lengthSq() < size)
      return true;
  }
  return false;
}

bool Poly::isHuge(const PolyVerts &Poly, float size) {
  if(Poly.size() < 3)
    throw std::runtime_error("more verts");

  const vec3*	curr = &Poly[0];
  vec3	prev = Poly[Poly.size() - 1];

  // Kwadrat
  size *= size;

  // Sprawd� ka�d� kraw�d�
  for(int i = (int)Poly.size() - 1; i >= 0; i--, prev = *curr++) {
    if((*curr - prev).lengthSq() > size)
      return true;
  }
  return false;
}

bool Poly::isPlanar(const PolyVerts &Poly) {
  if(Poly.size() < 3)
    throw std::runtime_error("more verts");

  const vec3*	curr = &Poly[0];
  vec4	plane = Poly::plane(Poly);

  // Sprawd� ka�d� kraw�d�
  for(int i = (int)Poly.size() - 1; i >= 0; i--, curr++) {
    if(fabs(plane ^ *curr) > Epsf)
      return false;
  }
  return true;
}

bool Poly::isConvex(const PolyVerts &Poly) {
  if(Poly.size() < 3)
    throw std::runtime_error("more verts");

  const vec3*	curr = &Poly[0];
  vec3	prev = Poly[Poly.size() - 1];
  vec4	plane = Poly::plane(Poly);

  // Sprawd� ka�d� kraw�d�
  for(int i = (int)Poly.size() - 1; i >= 0; i--, prev = *curr++) {
    vec4	edge(plane, prev, *curr);
    const vec3*	curr2 = &Poly[i];

    // Sprawd� wszystkie wierzcho�ki
    for(int j = i - 1; j >= 0; j--, curr2--) {
      if((edge ^ *curr2--) > Epsf)
        return false;
    }
  }
  return true;
}

PolyEdges Poly::edges(const PolyVerts &Poly) {
  if(Poly.size() < 3)
    throw std::runtime_error("more verts");

  const vec3*	curr = &Poly[0];
  vec3	prev = Poly[Poly.size() - 1];
  vec4	plane = Poly::plane(Poly);

  PolyEdges edges(Poly.size());

  // Wyznacz ka�d� kraw�d�
  for(int i = (int)Poly.size() - 1; i >= 0; i--, prev = *curr++)
    edges.append(vec4(plane, prev, *curr));
  return edges;
}

Side::Enum Poly::test(const PolyVerts &verts, const vec4 &plane) {
  if(verts.size() < 3)
    throw std::runtime_error("more verts");

  const vec3*	curr = &verts[0];
  unsigned side = Side::On;

  // Sprawd� ka�dy wierzcho�ek
  for(int i = (int)verts.size() - 1; i >= 0; i--)
    side |= plane.test(*curr++);
  return (Side::Enum)side;
}

bool Poly::test(const PolyVerts &verts, const vec3 &point) {
  if(verts.size() < 3)
    throw std::runtime_error("more verts");

  const vec3*	curr = &verts[0];
  vec3	prev = verts[verts.size() - 1];
  vec4	plane = Poly::plane(verts);

  // Sprawd� ka�d� kraw�d�
  for(int i = (int)verts.size() - 1; i >= 0; i--, prev = *curr++) {
    if((vec4(plane, prev, *curr) ^ point) > Epsf)
      return false;
  }
  return true;
}

bool Poly::test(const vec3 *verts, unsigned count, const vec3 &point) {
  if(verts == NULL)
    throw std::runtime_error("no verts");
  if(count < 3)
    throw std::runtime_error("more verts");

  const vec3*	curr = &verts[0];
  vec3	prev = verts[count - 1];
  vec4	plane = Poly::plane(verts, count);

  // Sprawd� ka�d� kraw�d�
  for(int i = (int)count - 1; i >= 0; i--, prev = *curr++) {
    if((vec4(plane, prev, *curr) ^ point) > Epsf)
      return false;
  }
  return true;
}

bool Poly::test(const vec4 *edges, unsigned count, const vec3 &point) {
  if(edges == NULL)
    throw std::runtime_error("no edges");
  if(count < 3)
    throw std::runtime_error("more verts");

  // Sprawd� ka�d� kraw�d�
  for(int i = count - 1; i >= 0; i--) {
    if((edges[i] ^ point) > Epsf)
      return false;
  }
  return true;
}

bool Poly::test(const PolyVerts &a, const PolyVerts &b) {
  throw std::runtime_error("not supported");
}
