//------------------------------------//
// 
// SoundException.hpp
// 
// Author: ayufan (ayufan(at)o2.pl)
// Project: Sound
// Date: 2008-03-08
// 
//------------------------------------//

#pragma once

//------------------------------------//
// SoundException class

class AYUAPI SoundException : public Exception
{
	// Fields
private:
	unsigned m_errCode;

	// Constructor
public:
	SoundException(unsigned errCode);

	// Destructor
public:
	virtual ~SoundException() throw();

	// Methods
protected:
	virtual string message() const;

public:
	//! Zwraca kod b�edu (format OpenAL)
	unsigned errCode() const {
		return m_errCode;
	}

	// Functions
public:
};
