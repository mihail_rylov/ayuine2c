#ifndef SPOTLIGHT_HPP
#define SPOTLIGHT_HPP

#include "Pipeline.hpp"
#include "Light.hpp"
#include <MaterialSystem/ShaderState.hpp>
#include <Math/Camera.hpp>

class Frame;

class PIPELINE_EXPORT SpotLight : public Light
{
public:
  SpotLight();

public:
  bool set(bool transparent) const;  
  void draw(RenderShader* shader, Frame& frame) const;

  const Camera& camera() const;
  void setCamera(const Camera& camera);

public:
  bool test(const box& boxBounds, const sphere& sphereBounds) const;

private:
  Camera m_camera;
};

#endif // SPOTLIGHT_HPP
