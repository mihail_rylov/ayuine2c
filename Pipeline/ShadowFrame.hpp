#ifndef SHADOWFRAME_HPP
#define SHADOWFRAME_HPP

#include "Pipeline.hpp"
#include "BaseShadowFrame.hpp"

class RenderTarget;

class PIPELINE_EXPORT ShadowFrame : public BaseShadowFrame
{
public:
  Q_INVOKABLE ShadowFrame();
  ~ShadowFrame();

public:
  bool begin(const ShadowTextureRef& shadowTexture);

public:
  QSharedPointer<BaseRenderTarget> allocRenderTarget(unsigned size);
  QSharedPointer<BaseRenderTarget> cleanRenderTarget();

private:
  QHash<unsigned, QSharedPointer<BaseRenderTarget> > depthTarget;
  QSharedPointer<RenderTarget> cleanTarget;
};

#endif // SHADOWFRAME_HPP
