#include "ForwardShadedFrame.hpp"
#include "Light.hpp"
#include "Fragment.hpp"

#include <Render/RenderSystem.hpp>
#include <Render/RenderShader.hpp>

#include <MaterialSystem/MaterialShader.hpp>
#include <MaterialSystem/LightShader.hpp>
#include <MaterialSystem/VertexShaderCompiler.hpp>

ForwardShadedFrame::ForwardShadedFrame()
{
}

void ForwardShadedFrame::draw()
{
  prepareFrame(true);

  getRenderSystem().setCamera(camera());
  getRenderSystem().setFrame(deltaTime(), frameTime());

  renderOrderedFrags(OrderNegative);

  getRenderSystem().setFillMode(RenderSystem::Solid);

  for(Light* light = lights(); light; light = light->next())
  {
    if(light->set(false))
    {
      renderSolidFrags(light, true);
    }
  }

  renderTransparentFrags(true);

  renderOrderedFrags(OrderPositive);
}

bool ForwardShadedFrame::hasLighting() const
{
  return true;
}

Frame::FrameType ForwardShadedFrame::type() const
{
  return ForwardShaded;
}

void compileLightColor(MaterialShaderCompiler& compiler, const QSharedPointer<ShaderBlock>& output)
{
  bool hasRgb = false;
  bool hasAlpha = false;

  if(output)
  {
    if(ShaderBlock::InputLink colorLink = output->input("Color"))
    {
      compiler.ps("outColor.rgb = %1;", colorLink.varName(compiler));
      hasRgb = true;
    }

    if(ShaderBlock::InputLink alphaLink = output->input("Alpha"))
    {
      compiler.ps("outColor.a = %1;", alphaLink.varName(compiler));
      hasAlpha = true;
    }
  }
  else if(compiler.materialLink)
  {
    if(ShaderBlock::InputLink alphaLink = compiler.materialLink->input("Alpha"))
    {
      compiler.ps("outColor.a = %1;", alphaLink.varName(compiler));
      hasAlpha = true;
    }
    else if(ShaderBlock::InputLink opacityLink = compiler.materialLink->input("Opacity"))
    {
      compiler.ps("outColor.a = %1;", opacityLink.varName(compiler));
      hasAlpha = true;
    }
  }

  if(!hasRgb)
    compiler.ps("outColor.rgb = 0.0f;");

  if(!hasAlpha)
    compiler.ps("outColor.a = 1.0f;");
}

void ForwardShadedFrame::compileLightColor(MaterialShaderCompiler& compiler, const QSharedPointer<ShaderBlock>& output)
{
  ::compileLightColor(compiler, output);
}

void compileSolidColor(MaterialShaderCompiler &compiler, const QSharedPointer<ShaderBlock>& output);

void ForwardShadedFrame::compileSolidColor(MaterialShaderCompiler &compiler, const QSharedPointer<ShaderBlock>& output)
{
  ::compileSolidColor(compiler, output);
}

bool ForwardShadedFrame::bindShader(GeneralShader *materialShader, MaterialSystem::Quality quality, VertexBuffer::Types vertexType)
{
  if(!materialShader)
    return false;

  unsigned materialIndex = vertexToIndex(vertexType, quality);

  RenderShaderRef shader = materialShader->shader(materialIndex, NULL);

  if(!shader)
  {
    shader = readShader(NULL, materialShader, quality, vertexType);
    if(shader)
      materialShader->setShader(materialIndex, NULL, shader);
  }

  if(!shader)
  {
    VertexShaderCompiler compiler(vertexType);
    compiler.quality = quality;
    compileSolidColor(compiler, materialShader->findOutput());
    shader = compiler.shader();
    materialShader->setShader(materialIndex, NULL, shader);

    writeShader(compiler.code(), NULL, materialShader, quality, vertexType);
  }

  if(!shader)
  {
    return false;
  }

  getRenderSystem().setShader(shader.data());
  return true;
}

bool ForwardShadedFrame::bindLightShader(GeneralShader *materialShader, LightShader *lightShader, MaterialSystem::Quality quality, VertexBuffer::Types vertexType)
{
  if(!materialShader || !lightShader)
    return false;

  unsigned materialIndex = vertexToIndex(vertexType, quality);

  RenderShaderRef shader = materialShader->shader(materialIndex, lightShader);

  if(!shader)
  {
    shader = readShader(materialShader, lightShader, quality, vertexType);
    if(shader)
      materialShader->setShader(materialIndex, lightShader, shader);
  }

  if(!shader)
  {
    VertexShaderCompiler compiler(vertexType);
    compiler.quality = quality;
    compiler.materialLink = materialShader->findOutput();
    compileLightColor(compiler, lightShader->findFinal());
    shader = compiler.shader();
    materialShader->setShader(materialIndex, lightShader, shader);

    writeShader(compiler.code(), materialShader, lightShader, quality, vertexType);
  }

  if(!shader)
  {
    return false;
  }

  getRenderSystem().setShader(shader.data());
  return true;
}
