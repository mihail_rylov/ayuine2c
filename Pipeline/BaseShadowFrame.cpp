#include "BaseShadowFrame.hpp"
#include "BaseShadowFrame_p.hpp"

#include <Render/RenderSystem.hpp>

BaseShadowFrame::BaseShadowFrame()
{
  m_frameTime = 0.0f;
  connect(&getRenderSystem(), SIGNAL(deviceLost()), SLOT(deviceLost()));
}

BaseShadowFrame::~BaseShadowFrame()
{
}

BaseShadowFrame::UpdateMode BaseShadowFrame::update(QObject* owner, ShadowTextureRef& renderTarget)
{
  // find object shadow
  for(int i = 0; i < m_shadows.size(); ++i)
  {
    if(m_shadows[i].owner == owner)
    {
      m_shadows[i].frameTime = m_frameTime;
      renderTarget = m_shadows[i].renderTarget;
      return Partial;
    }
  }

  ShadowTexture* best = NULL;
  float bestTime = 0;

  // find free shadow
  for(int i = 0; i < m_shadows.size(); ++i)
  {
    float time = m_frameTime - m_shadows[i].frameTime;
    if(time < 4.0f)
      continue;
    if(time > bestTime)
    {
      best = &m_shadows[i];
      bestTime = time;
    }
  }

  // return free shadow
  if(best)
  {
    best->owner = owner;
    best->frameTime = m_frameTime;
    renderTarget = best->renderTarget;
    return Full;
  }

  // alloc new shadow
  ShadowTexture newShadow;
  newShadow.owner = owner;
  newShadow.renderTarget = allocRenderTarget(256);
  newShadow.frameTime = m_frameTime;
  m_shadows.append(newShadow);
  renderTarget = newShadow.renderTarget;
  return Full;
}

ShadowTextureRef BaseShadowFrame::find(QObject *owner)
{
  for(int i = 0; i < m_shadows.size(); ++i)
  {
    if(m_shadows[i].owner == owner)
    {
      return m_shadows[i].renderTarget;
    }
  }
  return ShadowTextureRef();
}

bool BaseShadowFrame::free(QObject *owner)
{
  bool found = false;

  for(int i = 0; i < m_shadows.size(); ++i)
  {
    if(m_shadows[i].owner == owner)
    {
      m_shadows[i].owner = NULL;
      found = true;
    }
  }

  return found;
}

void BaseShadowFrame::nextFrame(float dt)
{
  m_frameTime += dt;
}

unsigned BaseShadowFrame::shadowCount() const
{
  return m_shadows.size();
}

unsigned BaseShadowFrame::shadowUsedCount() const
{
  unsigned used = 0;

  for(int i = 0; i < m_shadows.size(); ++i)
  {
    if(m_frameTime - m_shadows[i].frameTime < 4.0f)
      ++used;
  }
  return used;
}

unsigned BaseShadowFrame::estimatedMemUsage() const
{
  unsigned memUsage = 0;

  for(int i = 0; i < m_shadows.size(); ++i)
  {
    if(qobject_cast<CubeRenderTarget*>(m_shadows[i].renderTarget.data()))
    {
      memUsage += m_shadows[i].renderTarget->width() * m_shadows[i].renderTarget->width() * 6 * 4;
    }
    else if(qobject_cast<RenderTarget*>(m_shadows[i].renderTarget.data()))
    {
      memUsage += m_shadows[i].renderTarget->width() * m_shadows[i].renderTarget->height() * 4;
    }
  }
  return memUsage;
}

void BaseShadowFrame::deviceLost()
{
  m_shadows.clear();
}
