#include "DepthFrame.hpp"
#include "Fragment.hpp"

#include <Render/RenderSystem.hpp>

#include <MaterialSystem/MaterialShader.hpp>
#include <MaterialSystem/MaterialShaderCompiler.hpp>

DepthFrame::DepthFrame()
{
}

Frame::FrameType DepthFrame::type() const
{
  return Depth;
}

void DepthFrame::draw()
{
  prepareFrame(false);

  getRenderSystem().setCamera(camera());
  getRenderSystem().setFrame(deltaTime(), frameTime());

  getRenderSystem().setFillMode(RenderSystem::Solid);
  getRenderSystem().setBlendState(RenderSystem::Opaque);

  renderSolidFrags(NULL);
}

void DepthFrame::compile(MaterialShaderCompiler &compiler, const QSharedPointer<ShaderBlock>& output)
{
  compiler.ps("outColor.r = %1;", compiler.getp(MaterialShaderCompiler::pDepth));
  compiler.ps("outColor.g = %1;", compiler.getp(MaterialShaderCompiler::pDepth2));
  compiler.ps("outColor.b = frac(outColor.r / 10);");
  compiler.ps("outColor.a = 1.0f;");

  if(output)
  {
    if(ShaderBlock::InputLink alphaLink = output->input("Alpha"))
      compiler.ps("outColor.a = %1;", alphaLink.varName(compiler));
    else if(ShaderBlock::InputLink alphaLink = output->input("Opacity"))
      compiler.ps("outColor.a = %1;", alphaLink.varName(compiler));
  }
}
