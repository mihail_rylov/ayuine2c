#include "ShadowFrame.hpp"

#include <Render/RenderTarget.hpp>
#include <Render/RenderSystem.hpp>

ShadowFrame::ShadowFrame()
{
  cleanTarget = QSharedPointer<RenderTarget>(new RenderTarget(1, 1, CubeRenderTarget::Rgb));

  if(getRenderSystem().beginScene())
  {
    getRenderSystem().setRenderTarget(cleanTarget.data());
    getRenderSystem().clear(RenderSystem::Target, QColor(Qt::white));
    getRenderSystem().setRenderTargets(NULL, 0);
    getRenderSystem().endScene();
  }
}

ShadowFrame::~ShadowFrame()
{
}

QSharedPointer<BaseRenderTarget> ShadowFrame::allocRenderTarget(unsigned size)
{  
  QSharedPointer<RenderTarget> renderTarget(new RenderTarget(size, size, RenderTarget::RgFloat));

  if(depthTarget.contains(size))
  {
    renderTarget->setDepthFormat(depthTarget[size].data());
  }
  else
  {
    renderTarget->setDepthFormat(RenderTarget::Depth16);
    depthTarget[size] = renderTarget;
  }

  return renderTarget;
}

bool ShadowFrame::begin(const ShadowTextureRef &shadowTexture)
{
  if(RenderTarget* renderTarget = qobject_cast<RenderTarget*>(shadowTexture.data()))
  {
    getRenderSystem().setRenderTarget(renderTarget);
    getRenderSystem().clear(RenderSystem::ClearOptions(RenderSystem::Target | RenderSystem::Depth), Qt::white);
    return BaseShadowFrame::begin();
  }
  return false;
}

QSharedPointer<BaseRenderTarget> ShadowFrame::cleanRenderTarget()
{
  return cleanTarget;
}
