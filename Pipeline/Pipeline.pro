TEMPLATE = lib
TARGET = Pipeline

include(../ayuine2c.pri)

LIBS += -lMath -lCore -lRender -lMaterialSystem

HEADERS += \
    Fragment.hpp \
    Pipeline.hpp \
    Fragment_p.hpp \
    TexturedFragment.hpp \
    Frame.hpp \
    Light.hpp \
    SheetFragment.hpp \
    BoxFragment.hpp \
    SphereFragment.hpp \
    PointFragment.hpp \
    GridFragment.hpp \
    InstancedFragment.hpp \
    AmbientLight.hpp \
    PointLight.hpp \
    SimpleFrame.hpp \
    Frame_p.hpp \
    WireFrame.hpp \
    SolidFrame.hpp \
    DepthFrame.hpp \
    CubeShadowFrame.hpp \
    BaseShadowFrame.hpp \
    ShadowFrame.hpp \
    ForwardShadedFrame.hpp \
    LitShadedFrame.hpp \
    ColorPickFrame.hpp \
    BaseShadowFrame_p.hpp \
    DeferredShadedFrame.hpp \
    BloomComponent.hpp \
    HdrComponent.hpp \
    SSAOComponent.hpp \
    SkyFragment.hpp \
    SpotLight.hpp \
    ConeFragment.hpp

SOURCES += \
    Fragment.cpp \
    Pipeline.cpp \
    TexturedFragment.cpp \
    Frame.cpp \
    Light.cpp \
    SheetFragment.cpp \
    BoxFragment.cpp \
    SphereFragment.cpp \
    PointFragment.cpp \
    GridFragment.cpp \
    InstancedFragment.cpp \
    AmbientLight.cpp \
    PointLight.cpp \
    SimpleFrame.cpp \
    WireFrame.cpp \
    SolidFrame.cpp \
    DepthFrame.cpp \
    CubeShadowFrame.cpp \
    BaseShadowFrame.cpp \
    ShadowFrame.cpp \
    ForwardShadedFrame.cpp \
    LitShadedFrame.cpp \
    ColorPickFrame.cpp \
    DeferredShadedFrame.cpp \
    BloomComponent.cpp \
    HdrComponent.cpp \
    SSAOComponent.cpp \
    SkyFragment.cpp \
    SpotLight.cpp \
    ConeFragment.cpp

RESOURCES += \
    PipelineShaders.qrc

OTHER_FILES += \
    Bloom.fx \
    Hdr.fx \
    SSAO.fx
