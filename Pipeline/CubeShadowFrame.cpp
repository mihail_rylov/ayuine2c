#include "CubeShadowFrame.hpp"
#include <Render/RenderTarget.hpp>
#include <Render/RenderSystem.hpp>

CubeShadowFrame::CubeShadowFrame()
{
  cleanTarget = QSharedPointer<CubeRenderTarget>(new CubeRenderTarget(1, CubeRenderTarget::Rgb));

  if(getRenderSystem().beginScene())
  {
    for(int i = 0; i < 6; ++i)
    {
      getRenderSystem().setRenderTarget(cleanTarget.data(), (CubeFace::Enum)i);
      getRenderSystem().clear(RenderSystem::Target, QColor(Qt::white));
    }
    getRenderSystem().setRenderTargets(NULL, 0);
    getRenderSystem().endScene();
  }
}

CubeShadowFrame::~CubeShadowFrame()
{
}

QSharedPointer<BaseRenderTarget> CubeShadowFrame::allocRenderTarget(unsigned size)
{
  QSharedPointer<CubeRenderTarget> renderTarget(new CubeRenderTarget(size, CubeRenderTarget::RgFloat));

  if(depthTarget.contains(size))
  {
    renderTarget->setDepthFormat(depthTarget[size].data());
  }
  else
  {
    renderTarget->setDepthFormat(CubeRenderTarget::Depth16);
    depthTarget[size] = renderTarget;
  }

  return renderTarget;
}

QSharedPointer<BaseRenderTarget> CubeShadowFrame::cleanRenderTarget()
{
  return cleanTarget;
}

bool CubeShadowFrame::begin(const ShadowTextureRef &shadowTexture, CubeFace::Enum cubeFace)
{
  if(CubeRenderTarget* cubeRenderTarget = qobject_cast<CubeRenderTarget*>(shadowTexture.data()))
  {
    getRenderSystem().setRenderTarget(cubeRenderTarget, cubeFace);
    getRenderSystem().clear(RenderSystem::ClearOptions(RenderSystem::Target | RenderSystem::Depth), Qt::white);
    if(BaseShadowFrame::begin())
    {
      clear();
      return true;
    }
  }
  return false;
}
