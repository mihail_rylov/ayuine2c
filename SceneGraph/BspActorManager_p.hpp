#ifndef BSPACTORMANAGER_P_HPP
#define BSPACTORMANAGER_P_HPP

#include <Math/Frustum.hpp>
#include <Math/mathlib.hpp>
#include <Math/Vec3.hpp>
#include <Math/Vec4.hpp>

#include <QRect>

struct BspPlane
{
  Axis::Enum Plane;
  float Split;

  BspPlane()
  {
    Plane = Axis::NonAxial;
    Split = 0;
  }
  BspPlane(Axis::Enum plane, float split)
    : Plane(plane), Split(split)
  {
  }

  bool leaf() const
  {
    return Plane == Axis::NonAxial;
  }
};

struct BspNode : BspPlane
{
  unsigned Childs[2];
  QVector<unsigned> Zones;

  BspNode()
  {
    Childs[0] = Childs[1] = ~0;
  }

  unsigned childForOrigin(const vec3& origin) const
  {
    return Childs[origin[Plane] >= Split];
  }
};

struct BspPortal
{
  box Bounds;
  unsigned Zone;

  BspPortal()
  {
    Zone = ~0;
  }
};

struct BspZone
{
  box Bounds;
  QVector<BspPortal> Portals;
  QVector<Actor*> Actors;
  BspZone *Next, *Next2;
  unsigned Tick, Tick2;
  QRectF Visible;
  unsigned Depth;

  BspZone()
  {
    Next = Next2 = NULL;
    Tick = Tick2 = 0;
  }
};

struct BspPlaneEx : BspPlane
{
  vec3 Mins, Maxs;

  BspPlaneEx()
  {
  }
  BspPlaneEx(Axis::Enum plane, float split, const vec3& mins, const vec3& maxs)
    : BspPlane(plane, split), Mins(mins), Maxs(maxs)
  {
    Mins[plane] = split;
    Maxs[plane] = split;
  }
};

#endif // BSPACTORMANAGER_P_HPP
