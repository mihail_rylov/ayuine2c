#ifndef MESHACTOR_HPP
#define MESHACTOR_HPP

#include "Actor.hpp"

class SCENEGRAPH_EXPORT MeshActor : public Actor
{
  Q_OBJECT
  Q_CLASSINFO("IconFileName", ":/SceneGraph/IconMesh.png")
  Q_PROPERTY(bool movable READ movable WRITE setMovable)
  Q_PROPERTY(bool collidable READ collidable WRITE setCollidable)
  Q_PROPERTY(bool gravity READ gravity WRITE setGravity)
  Q_PROPERTY(bool castShadows READ castShadows WRITE setCastShadows)
  // Q_PROPERTY(bool autoSector READ autoSector WRITE setAutoSector)

public:
  MeshActor();
  ~MeshActor();

public:
  const QMetaObject* type() const;

  bool autoSector() const;
  void setAutoSector(bool autoSector);

  bool movable() const;
  virtual void setMovable(bool movable);

  bool collidable() const;
  virtual void setCollidable(bool collidable);

  bool gravity() const;
  virtual void setGravity(bool gravity);

  bool castShadows() const;
  virtual void setCastShadows(bool castShadows);

signals:
  void movableChanged(Actor* actor = NULL);
  void collidableChanged(Actor* actor = NULL);
  void gravityChanged(Actor* actor = NULL);
  void castShadowsChanged(Actor* actor = NULL);

public:
  bool extendBounds() const;
  bool supportsZones() const;
  bool zonesFromPoint() const;

private:
  bool m_movable, m_autoSector;
  bool m_collidable, m_gravity;
  bool m_castShadows;
};

#endif // MESHACTOR_HPP
