#include "BspActorManager.hpp"
#include "BspActorManager_p.hpp"
#include "NaiveActorManager.hpp"
#include "PortalActor.hpp"
#include "SectorActor.hpp"

static const int MaxNodes = 4096;
static BspNode* Nodes[MaxNodes];
static unsigned NodeCount = 0;

static QVector<Actor*> actorLists[4];
static unsigned actorListIndex = 0;

static const QRectF rectClip(-1, -1, 2, 2);

static QVector<Actor*>& allocateActorList()
{
  QVector<Actor*>& actorList = actorLists[qMin<unsigned>(actorListIndex++, 4)];
  actorList.resize(0);
  return actorList;
}

static void freeActorList()
{
  --actorListIndex;
}

BspActorManager::BspActorManager()
{
}

BspActorManager::~BspActorManager()
{
}

unsigned BspActorManager::findNode(const vec3& origin)
{
  unsigned index = 0;
  while(index < m_bspNodes.size())
  {
    BspNode& node = m_bspNodes[index];
    if(node.leaf())
      return index;
    index = node.childForOrigin(origin);
  }
  return ~0;
}

void BspActorManager::buildBsp_r(unsigned node, QVector<BspPlaneEx>& planes)
{
  if(planes.isEmpty())
  {
    m_bspNodes[node].Zones.reserve(4);
    return;
  }

  BspPlane bestPlane;
  unsigned bestScore = ~0;

  // Wyznacz p�aszczyzn� podzia�u
  foreach(const BspPlaneEx &plane, planes)
  {
    int left = 0, right = 0, split = 0;

    // Zlicz po kt�rej stronie znajduj� si� sektory
    foreach(const BspPlaneEx &planeEx, planes)
    {
      // Okre�l rodzaj podzia�u
      if(planeEx.Maxs[plane.Plane] <= plane.Split)
        left++;
      else if(plane.Split <= planeEx.Mins[plane.Plane])
        right++;
      else
        split++;
    }

    // Wyznacz wynik
    unsigned score = qAbs(left - right) * 2 + split * 5;
    if(score < bestScore)
    {
      bestPlane = plane;
      bestScore = score;

      if(left > 0 && right > 0)
      {
        break;
      }
    }
  }
  Q_ASSERT(bestScore != ~0);

  QVector<BspPlaneEx> left, right;
  unsigned leftNode, rightNode;
  left.reserve(planes.size());
  right.reserve(planes.size());

  // Zapisz p�aszczyzn� podzia�u
  (BspPlane&)m_bspNodes[node] = bestPlane;

  // Podziel na dwie grupy
  foreach(const BspPlaneEx &planeEx, planes)
  {
    // Ta sama p�aszczyzna?
    if(planeEx.Plane == bestPlane.Plane &&
       qFuzzyCompare(planeEx.Split, bestPlane.Split))
      continue;

    // Dodaj na lew� stron�
    if(planeEx.Maxs[bestPlane.Plane] <= bestPlane.Split)
      left.push_back(planeEx);

    // Dodaj na praw� stron�
    else if(bestPlane.Split <= planeEx.Mins[bestPlane.Plane])
      right.push_back(planeEx);

    // Przetnij na dwie cz�ci
    else
    {
      BspPlaneEx leftp = planeEx;
      BspPlaneEx rightp = planeEx;
      leftp.Maxs[bestPlane.Plane] = bestPlane.Split;
      rightp.Mins[bestPlane.Plane] = bestPlane.Split;
      left.push_back(leftp);
      right.push_back(rightp);
    }
  }

  // Wyczy�� tablic�
  planes.clear();

  // Wyznacz lewe dziecko
  m_bspNodes[node].Childs[0] = leftNode = m_bspNodes.size();
  m_bspNodes.push_back(BspNode());
  buildBsp_r(leftNode, left);
  left.clear();

  // Wyznacz prawe dziecko
  m_bspNodes[node].Childs[1] = rightNode = m_bspNodes.size();
  m_bspNodes.push_back(BspNode());
  buildBsp_r(rightNode, right);
  right.clear();
}

void BspActorManager::addZoneToBspTree(BspZone& zone, unsigned zoneIndex)
{
  if(m_bspNodes.isEmpty())
    return;

  BspNode* node = &m_bspNodes[0];
  NodeCount = 0;
  while(node)
  {
    if(node->leaf())
    {
      node->Zones.push_back(zoneIndex);
      node = NodeCount > 0 ? Nodes[--NodeCount] : NULL;
      continue;
    }

    // Okre�l stron�
    if(zone.Bounds.Maxs[node->Plane] <= node->Split)
      node = &m_bspNodes[node->Childs[0]];
    else if(node->Split <= zone.Bounds.Mins[node->Plane])
      node = &m_bspNodes[node->Childs[1]];
    else
    {
      Q_ASSERT(NodeCount < MaxNodes);
      Nodes[NodeCount++] = &m_bspNodes[node->Childs[0]];
      node = &m_bspNodes[node->Childs[1]];
    }
  }
  Q_ASSERT(NodeCount == 0);
}

BspZone* BspActorManager::findZones(const box& bounds)
{
  if(m_bspNodes.isEmpty() || m_bspZones.isEmpty())
    return NULL;

  BspZone* zoneList = NULL;
  BspNode* node = &m_bspNodes[0];
  NodeCount = 0;
  ++m_bspTick;
  while(node)
  {
    if(node->leaf())
    {
      foreach(unsigned zoneIndex, node->Zones)
      {
        BspZone& zone = m_bspZones[zoneIndex];
        if(zone.Tick == m_bspTick)
          continue;
        zone.Tick = m_bspTick;
        zone.Next = zoneList;
        zoneList = &zone;
      }
      node = NodeCount > 0 ? Nodes[--NodeCount] : NULL;
      continue;
    }

    // Okre�l stron�
    if(bounds.Maxs[node->Plane] <= node->Split)
      node = &m_bspNodes[node->Childs[0]];
    else if(node->Split <= bounds.Mins[node->Plane])
      node = &m_bspNodes[node->Childs[1]];
    else
    {
      Q_ASSERT(NodeCount < MaxNodes);
      Nodes[NodeCount++] = &m_bspNodes[node->Childs[0]];
      node = &m_bspNodes[node->Childs[1]];
    }
  }
  Q_ASSERT(NodeCount == 0);
  return zoneList;
}

void BspActorManager::compile()
{
  QVector<BspPlaneEx> planes;
  QVector<box> autoSectors;

  unsigned normalCount = 0, autoCount = 0;
  unsigned normalPortalCount = 0, autoPortalCount = 0;

  ActorManager::compile();

  // Uaktualnij zakresy
  updateBounds();

  // Wyczy�� tablice
  m_bspZones.clear();
  m_bspZones.reserve(200);
  m_bspNodes.clear();
  m_bspNodes.reserve(1000);
  m_bspTick = 0;

  // Zarezerwuj miesjce
  planes.reserve(1000);

  // Pobierz wszystkich aktor�w
  foreach(QSharedPointer<Actor> actor, m_actors)
  {
    if(QSharedPointer<SectorActor> sectorActor = actor.objectCast<SectorActor>())
    {
      // Dodaj stref�
      BspZone zone;
      zone.Bounds = sectorActor->boxBounds();
      m_bspZones.push_back(zone);

      // Dodaj p�aszczyzny
      planes.append(BspPlaneEx(Axis::X, zone.Bounds.Mins.X, zone.Bounds.Mins, zone.Bounds.Maxs));
      planes.append(BspPlaneEx(Axis::Y, zone.Bounds.Mins.Y, zone.Bounds.Mins, zone.Bounds.Maxs));
      planes.append(BspPlaneEx(Axis::Z, zone.Bounds.Mins.Z, zone.Bounds.Mins, zone.Bounds.Maxs));
      planes.append(BspPlaneEx(Axis::X, zone.Bounds.Maxs.X, zone.Bounds.Mins, zone.Bounds.Maxs));
      planes.append(BspPlaneEx(Axis::Y, zone.Bounds.Maxs.Y, zone.Bounds.Mins, zone.Bounds.Maxs));
      planes.append(BspPlaneEx(Axis::Z, zone.Bounds.Maxs.Z, zone.Bounds.Mins, zone.Bounds.Maxs));

      if(sectorActor->autoSector())
      {
        autoSectors.append(zone.Bounds);
        ++autoCount;
      }
      else
      {
        ++normalCount;
      }
    }
    else if(actor->autoSector())
    {
      // Dodaj stref�
      BspZone zone;
      zone.Bounds = actor->boxBounds();
      m_bspZones.push_back(zone);

      // Dodaj p�aszczyzny
      planes.append(BspPlaneEx(Axis::X, zone.Bounds.Mins.X, zone.Bounds.Mins, zone.Bounds.Maxs));
      planes.append(BspPlaneEx(Axis::Y, zone.Bounds.Mins.Y, zone.Bounds.Mins, zone.Bounds.Maxs));
      planes.append(BspPlaneEx(Axis::Z, zone.Bounds.Mins.Z, zone.Bounds.Mins, zone.Bounds.Maxs));
      planes.append(BspPlaneEx(Axis::X, zone.Bounds.Maxs.X, zone.Bounds.Mins, zone.Bounds.Maxs));
      planes.append(BspPlaneEx(Axis::Y, zone.Bounds.Maxs.Y, zone.Bounds.Mins, zone.Bounds.Maxs));
      planes.append(BspPlaneEx(Axis::Z, zone.Bounds.Maxs.Z, zone.Bounds.Mins, zone.Bounds.Maxs));

      zone.Bounds.inflate(0.1f);
      autoSectors.append(zone.Bounds);
      ++autoCount;
    }
  }

  // Dodaj g��wn� ga���
  BspNode node;
  m_bspNodes.push_back(node);

  // Wygeneruj drzewo BSP
  buildBsp_r(0, planes);

  // Dodaj co najmniej jedn� stref�
  if(m_bspZones.isEmpty())
  {
    BspZone zone;
    zone.Bounds = boxBounds();
    m_bspZones.push_back(zone);
  }

  // Dodaj strefy do drzewa
  for(int i = 0; i < m_bspZones.size(); ++i)
  {
    addZoneToBspTree(m_bspZones[i], i);
  }

  // Dodaj portale do stref
  foreach(QSharedPointer<Actor> actor, m_actors)
  {
    if(QSharedPointer<PortalActor> portalActor = actor.objectCast<PortalActor>())
    {
      if(portalActor->boxBounds().empty())
        continue;

      BspPortal portal;

      // Znajd� strefy w kt�rych le�y portal
      BspZone* zoneList = findZones(portalActor->boxBounds());

      // Dodaj portal do wszystkich stref
      for(BspZone* zone = zoneList; zone; zone = zone->Next)
      {
        for(BspZone* otherZone = zone->Next; otherZone; otherZone = otherZone->Next)
        {
          // Wyznacz cz�� wsp�ln� dla wszystkich stref
          portal.Bounds = box::intersect(box::intersect(zone->Bounds, otherZone->Bounds), portalActor->boxBounds());
          if(portal.Bounds.empty())
            continue;

          // Dodaj portal do zone
          portal.Zone = otherZone - m_bspZones.constData();
          zone->Portals.push_back(portal);

          // Dodaj portal do otherZone
          portal.Zone = zone - m_bspZones.constData();
          otherZone->Portals.push_back(portal);
          ++normalPortalCount;
        }
      }
    }
  }

  foreach(const box &zoneA, autoSectors)
  {
    foreach(const box &zoneB, autoSectors)
    {
      if(zoneA == zoneB)
        continue;

      box zoneAB = box::intersect(zoneA, zoneB);
      if(zoneAB.empty())
        continue;

      BspPortal portal;

      // Znajd� strefy w kt�rych le�y portal
      BspZone* zoneList = findZones(zoneAB);

      // Dodaj portal do wszystkich stref
      for(BspZone* zone = zoneList; zone; zone = zone->Next)
      {
        for(BspZone* otherZone = zone->Next; otherZone; otherZone = otherZone->Next)
        {
          // Wyznacz cz�� wsp�ln� dla wszystkich stref
          portal.Bounds = box::intersect(box::intersect(zone->Bounds, otherZone->Bounds), zoneAB);
          if(portal.Bounds.empty())
            continue;

          // Dodaj portal do zone
          portal.Zone = otherZone - m_bspZones.constData();
          zone->Portals.push_back(portal);

          // Dodaj portal do otherZone
          portal.Zone = zone - m_bspZones.constData();
          otherZone->Portals.push_back(portal);
          ++autoPortalCount;
        }
      }
    }
  }

  foreach(QSharedPointer<Actor> actor, m_actors)
  {
    actor->m_zoneList.clear();
    actor->m_zoneTick = 0;
    addToZones(actor.data());
  }

  qCritical() << "[BSP] Sectors:" << normalCount << "/" << autoCount << " Portals:" << normalPortalCount << "/" << autoPortalCount;
}

void BspActorManager::removeFromZones(Actor* actor)
{
  // Odepnij z listy stref
  foreach(unsigned zoneIndex, actor->m_zoneList)
  {
    BspZone& zone = m_bspZones[zoneIndex];

    int index = zone.Actors.indexOf(actor);
    if(index >= 0)
      zone.Actors.remove(index);
  }

  // Wyczy�� list� stref
  actor->m_zoneList.resize(0);
}

void BspActorManager::addToZones(Actor* actor)
{
  if(actor->m_zoneList.size())
    return;
  if(!actor->supportsZones())
    return;

  // Znajd� strefy w kt�rych le�y aktor
  if(actor->zonesFromPoint())
  {
    BspZone* zoneList = findZones(actor->origin(), actor->sphereBounds().Radius);
    for(BspZone* zone = zoneList; zone; zone = zone->Next)
    {
      zone->Actors.push_back(actor);
      actor->m_zoneList.push_back(zone - m_bspZones.constData());
    }
  }
  else
  {
    BspZone* zoneList = findZones(actor->boxBounds());
    for(BspZone* zone = zoneList; zone; zone = zone->Next)
    {
      zone->Actors.push_back(actor);
      actor->m_zoneList.push_back(zone - m_bspZones.constData());
    }
  }
}

void BspActorManager::findVisibleZones_r(unsigned zoneIndex, const frustum& f, const QRectF& s,
                                         QVector<unsigned> &depth, BspZone*& zoneList)
{
  BspZone& zone = m_bspZones[zoneIndex];
  if(zone.Tick == m_bspTick)
  {
    // Dodaj widoczny obszar
    zone.Visible = s.unite(zone.Visible);
  }
  else
  {
    // Oznacz jako odwiedzony
    zone.Tick = m_bspTick;
    zone.Visible = s;

    // Dodaj do listy
    zone.Next = zoneList;
    zoneList = &zone;
  }

  // Dodaj do kolejki odwiedzonych
  depth.push_back(zoneIndex);

  // Sprawd� wszystkie portale
  foreach(const BspPortal &portal, zone.Portals)
  {
    // Nie cofamy si�!
    if(depth.contains(portal.Zone))
      continue;

    // Przytnij portal do widoku i sprawd� widoczno�� w kolejnym sektorze
    QRectF newS = s.intersect(f.screenRect(portal.Bounds));
    if(newS.isEmpty())
      continue;
    findVisibleZones_r(portal.Zone, f, newS, depth, zoneList);
  }

  // Zdejmij z kolejki
  depth.pop_back();
}

BspZone* BspActorManager::findVisibleZones(const vec3& origin, const frustum& f)
{
  unsigned currentNode = findNode(origin);
  BspZone* zoneList = NULL;

  ++m_bspTick;

  if(currentNode == ~0 || m_bspNodes[currentNode].Zones.empty())
  {
    // Zwr�c wszystkie strefy
    for(unsigned i = 0; i < m_bspZones.size(); ++i)
    {
      BspZone* zone = &m_bspZones[i];
      zone->Next = zoneList;
      zone->Tick = m_bspTick;
      zone->Visible = QRectF(-1, -1, 2, 2);
      zoneList = zone;
    }
    return zoneList;
  }

  QVector<unsigned> depth;
  BspNode& node = m_bspNodes[currentNode];

  // Wyznacz widoczno��
  foreach(unsigned zone, node.Zones)
  {
    findVisibleZones_r(zone, f, rectClip, depth, zoneList);
    Q_ASSERT(depth.isEmpty());
  }
  return zoneList;
}

void BspActorManager::findVisibleActors(const vec3& origin, const frustum& f, QVector<Actor*> &actorList)
{
  actorList.resize(0);

  // Przygotuj wszystkich widocznych aktor�w
  for(BspZone* zone = findVisibleZones(origin, f); zone; zone = zone->Next)
  {
    // Dodaj widocznych aktor�w ze strefy
    foreach(Actor* actor, zone->Actors)
    {
      if(actor->m_zoneTick == m_bspTick)
        continue;
      actor->m_zoneTick = m_bspTick;
      actorList.push_back(actor);
    }
  }
}

void BspActorManager::findActors(const box& bounds, QVector<Actor*> &actorList)
{
  actorList.resize(0);

  // Przygotuj wszystkich aktor�w w zasi�gu
  for(BspZone* zone = findZones(bounds); zone; zone = zone->Next)
  {
    // Dodaj wszystkich aktor�w w zasi�gu
    foreach(Actor* actor, zone->Actors)
    {
      if(actor->m_zoneTick == m_bspTick)
        continue;
      actor->m_zoneTick = m_bspTick;
      actorList.push_back(actor);
    }
  }
}

BspZone* BspActorManager::findZones(const vec3& origin, float distance)
{
  unsigned currentNode = findNode(origin);
  if(currentNode == ~0)
    return NULL;

  QVector<unsigned> depth;
  BspNode& node = m_bspNodes[currentNode];
  BspZone* zoneList = NULL;
  Camera camera;
  sphere bounds(origin, distance);

  // Okre�l tick startowy
  unsigned startTick = ++m_bspTick;

  for(unsigned i = 0; i < 6; ++i)
  {
    BspZone* localZoneList = NULL;

    // Wyznacz kamer�
    Camera::cubeCamera(camera, origin, (CubeFace::Enum)i, distance);

    // Wyznacz widoczno��
    foreach(unsigned zone, node.Zones)
    {
      findVisibleZones_r(zone, camera.culler, rectClip, depth, localZoneList);
      Q_ASSERT(depth.empty());
    }

    // Przepnij list�
    for(BspZone* zone = localZoneList; zone; zone = zone->Next)
    {
      if(zone->Tick2 >= startTick)
        continue;
      zone->Tick2 = startTick;
      if(!zone->Bounds.test(bounds))
        continue;
      zone->Next2 = zoneList;
      zoneList = zone;
    }

    // Kolejny tick
    ++m_bspTick;
  }

  // Napraw list�
  for(BspZone* zone = zoneList; zone; zone = zone->Next)
  {
    zone->Next = zone->Next2;
  }

  return zoneList;
}

void BspActorManager::generateFrame(Frame& frame, const LocalCamera& localCamera)
{
  if(m_boundsDirty)
  {
    updateBounds();
  }

  QVector<Actor*>& actorList = allocateActorList();

  LocalCamera camera(localCamera, transform());

  // Znajd� widocznych aktor�w
  findVisibleActors(camera.viewOrigin, camera.culler, actorList);

  // Wy�wietl widocznych aktor�w
  foreach(Actor* actor, actorList)
  {
    if(actor->removedFromScene())
      continue;
    if(camera.culler.test(actor->sphereBounds()))
      actor->generateFrame(frame, camera);
  }

  freeActorList();
}

void BspActorManager::invalidate(const box& boxBoundsParent, Actor* owner)
{
  box boxBounds = boxBoundsParent.transform(transform1());

  QVector<Actor*>& actorList = allocateActorList();

  // Znajd� widocznych aktor�w
  findActors(boxBounds, actorList);

  // Wy�wietl widocznych aktor�w
  foreach(Actor* actor, actorList)
  {
    if(actor->removedFromScene())
      continue;
    if(actor->boxBounds().test(boxBounds))
      actor->invalidate(boxBounds, owner);
  }

  freeActorList();
}

const BspNode & BspActorManager::bspNode(unsigned node) const
{
  return m_bspNodes.at(node);
}

unsigned BspActorManager::bspNodeCount() const
{
  return m_bspNodes.size();
}

const BspZone &BspActorManager::bspZone(unsigned zone) const
{
  return m_bspZones.at(zone);
}

unsigned BspActorManager::bspZoneCount() const
{
  return m_bspZones.size();
}

void BspActorManager::generateDebugFrame(Frame &frame, const LocalCamera &localCamera)
{
#if 0
  QVector<Actor*>& actorList = allocateActorList();

  LocalCamera camera(localCamera, transform());

  // Znajd� widocznych aktor�w
  findVisibleActors(camera.viewOrigin, camera.culler, actorList);

  // Wy�wietl widocznych aktor�w
  foreach(Actor* actor, actorList)
    actor->generateDebugFrame(frame, camera);

  freeActorList();
#else
  ActorManager::generateDebugFrame(frame, localCamera);
#endif
}
