#include "SpotLightFragment_p.hpp"

#include <Render/VertexBuffer.hpp>
#include <Render/RenderSystem.hpp>
#include <Render/PrimitiveDrawer.hpp>

#include <Math/Vec3.hpp>

SpotLightFragment::SpotLightFragment()
{
}

SpotLightFragment::~SpotLightFragment()
{
}

VertexBuffer::Types SpotLightFragment::vertexType() const
{
  return VertexBuffer::Vertex;
}

void SpotLightFragment::draw()
{
  const int MaxPoints = 16;
  static QVector<vec3> vertices;
  static QVector<quint16> indices;
  static bool initialized = false;

  if(!initialized)
  {
    initialized = true;

    vertices << vec3(0,0,0);
    vertices << vec3(0,0,1);

    for(int i = 0; i < MaxPoints; ++i)
    {
      vec3 v(qSin(2 * i * M_PI / MaxPoints), qCos(2 * i * M_PI / MaxPoints), 1.0f);
      vertices << v;

      indices << 2+i << 2+(i+1)%MaxPoints;
    }

    indices << 0 << 1;

    indices << 0 << 2 + 0 * MaxPoints / 4;
    indices << 0 << 2 + 1 * MaxPoints / 4;
    indices << 0 << 2 + 2 * MaxPoints / 4;
    indices << 0 << 2 + 3 * MaxPoints / 4;

    indices << 1 << 2 + 0 * MaxPoints / 4;
    indices << 1 << 2 + 1 * MaxPoints / 4;
    indices << 1 << 2 + 2 * MaxPoints / 4;
    indices << 1 << 2 + 3 * MaxPoints / 4;
  }

  if(getRenderSystem().setVertexData(vertexType(), vertices.constData(), sizeof(vec3) * vertices.size()))
  {
    int index = getRenderSystem().setIndexData(indices.constData(), indices.size());
    if(index >= 0)
    {
      getRenderSystem().draw(IndexedPrimitiveDrawer(PrimitiveDrawer::LineList, 0, vertices.size(),
                                                    0, indices.size() / 2, index));
    }
  }
}
