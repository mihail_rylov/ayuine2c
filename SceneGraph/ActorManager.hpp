#ifndef ACTORMANAGER_HPP
#define ACTORMANAGER_HPP

#include <QObject>

#include <Math/Camera.hpp>

#include "Actor.hpp"

class SCENEGRAPH_EXPORT ActorManager : public Actor
{
  Q_OBJECT
  Q_CLASSINFO("IconFileName", ":/SceneGraph/IconSector.png")
  Q_PROPERTY(ActorRefList actors READ actors WRITE setActors DESIGNABLE false)
  Q_PROPERTY(bool grouped READ grouped WRITE setGrouped)
  Q_PROPERTY(bool autoSector READ autoSector WRITE setAutoSector)

public:
  ActorManager();
  ~ActorManager();

public:
  bool autoSector() const;
  void setAutoSector(bool autoSector);

  bool isSelectable() const;

  bool grouped() const;
  void setGrouped(bool grouped);

  void setHidden(bool hidden);

public:
  bool extendBounds() const;
  bool supportsZones() const;
  bool zonesFromPoint() const;

public:
  bool addActor(const QSharedPointer<Actor>& actor);
  bool removeActor(const QSharedPointer<Actor>& actor);
  void removeActors();
  void setBoundsDirty();
  void update(float dt);
  bool beginGame();
  void endGame();
  int indexOf(const QSharedPointer<Actor>& actor) const;
  int indexOf(Actor* actor) const;
  void invalidate(const box& boxBounds, Actor* owner);
  bool collide(const ray& dir, CollideInfo& collide, DebugView* debugView = NULL) const;
  const QMetaObject* type() const;
  void compile();
  void prepare();

public:
  void generateFrame(Frame& frame, const LocalCamera& localCamera);
  void generateDebugFrame(Frame& frame, const LocalCamera& localCamera);
  void generateColorPickFrame(ColorPickFrame& frame, const LocalCamera& localCamera);

signals:
  void groupedChanged(ActorManager* manager = NULL);
  void actorAdded(ActorManager* manager = NULL, Actor* actor = NULL);
  void actorAboutToBeRemoved(ActorManager* manager = NULL, Actor* actor = NULL);
  void actorRemoved(ActorManager* manager = NULL, Actor* actor = NULL);
  void actorsAboutToReset(ActorManager* manager = NULL);
  void actorsReset(ActorManager* manager = NULL);

public:
  ActorRefList actors() const;
  void setActors(const ActorRefList& actors);

protected:
  void setScene(const QSharedPointer<Scene>& scene);

protected:
  virtual void updateBounds();

public:
  virtual void addToZones(Actor* actor) = 0;
  virtual void removeFromZones(Actor* actor) = 0;

protected:
  QVector<QSharedPointer<Actor> > m_actors;
  bool m_boundsDirty;
  bool m_actorsDirty;
  bool m_grouped;
  bool m_autoSector;

  friend class Actor;
  friend class Scene;
};

Q_DECLARE_OBJECTREF(ActorManager)

#endif // ACTORMANAGER_HPP
