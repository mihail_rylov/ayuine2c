CONFIG(release,debug|release): {
	OBJECTS_DIR=$$OUT_PWD/../obj/$$TARGET/release
	contains(DEFINES, QT_MOC) {
		DESTDIR=$$OBJECTS_DIR
	} else {
		DESTDIR=$$OUT_PWD/../bin/release
		QMAKE_MOC=$$OUT_PWD/../obj/ExtMoc/release/ExtMoc
		windows:QMAKE_MOC=$${QMAKE_MOC}.exe
	}
} else {
	OBJECTS_DIR=$$OUT_PWD/../obj/$$TARGET/debug
	contains(DEFINES, QT_MOC) {
		DESTDIR=$$OBJECTS_DIR
	} else {
		DESTDIR=$$OUT_PWD/../bin/debug
		QMAKE_MOC=$$OUT_PWD/../obj/ExtMoc/debug/ExtMoc
		windows:QMAKE_MOC=$${QMAKE_MOC}.exe
	}
}


defineTest(addDepend) {
  in = $$1
  out = $$basename(in)
  in = $$replace(in, /, \\\\)
  eval($${out}.depends = $$in)
  eval($${out}.target = $(DESTDIR)$$out)
  eval($${out}.commands = $$QMAKE_COPY $$in $(DESTDIR))
  export($${out}.depends)
  export($${out}.target)
  export($${out}.commands)
  QMAKE_EXTRA_TARGETS += $$out
  POST_TARGETDEPS += $(DESTDIR)$$out
  export(QMAKE_EXTRA_TARGETS)
  export(POST_TARGETDEPS)
}

win32-msvc2010:{
  QMAKE_CXXFLAGS_WARN_ON = -W3
  QMAKE_CXXFLAGS_WARN_ON += -wd4100
  QMAKE_CXXFLAGS_WARN_ON += -wd4996
}
else:win32-g++:{
  QMAKE_CXXFLAGS_WARN_ON += -Wno-unknown-pragmas
  QMAKE_CXXFLAGS_WARN_ON += -Wno-unused-function
  QMAKE_CXXFLAGS_WARN_ON += -Wno-unused-variable
  QMAKE_CXXFLAGS_WARN_ON += -Wno-comment
  QMAKE_CXXFLAGS_WARN_ON += -fcheck-new -fno-strict-aliasing
  addDepend($(QTDIR)bin/mingwm10.dll)
  addDepend($(QTDIR)bin/libgcc_s_dw2-1.dll)
}

!editor:DEFINES += $$upper($$TARGET)_EXPORTS

INCLUDEPATH += $$PWD/lib/include $$PWD/ $$_PRO_FILE_PWD_/
LIBS += -L$$PWD/lib/msvc -L$$DESTDIR

precompile_header {
	PRECOMPILED_HEADER = $${TARGET}.hpp
	PRECOMPILED_SOURCE = $${TARGET}.cpp
}

editor {
  INCLUDEPATH += $$PWD/Editor/
  LIBS += -lEditor
}

# include(LuaBindings/Lua.pri)

CONFIG(qt) {
  debugfix=
  CONFIG(debug, debug|release):debugfix=d

  contains(QT, core):addDepend($(QTDIR)bin/QtCore$${debugfix}4.dll)
  contains(QT, gui):addDepend($(QTDIR)bin/QtGui$${debugfix}4.dll)
  contains(QT, network):addDepend($(QTDIR)bin/QtNetwork$${debugfix}4.dll)
  contains(QT, phonon):addDepend($(QTDIR)bin/phonon$${debugfix}4.dll)
  contains(QT, sql):addDepend($(QTDIR)bin/QtSql$${debugfix}4.dll)
  contains(QT, svg):addDepend($(QTDIR)bin/QtSvg$${debugfix}4.dll)
  contains(QT, xml):addDepend($(QTDIR)bin/QtXml$${debugfix}4.dll)
  contains(QT, webkit):addDepend($(QTDIR)bin/QtWebKit$${debugfix}4.dll)
}

for(deploy, DEPLOYFILES):addDepend($$_PRO_FILE_PWD_/$$deploy)
