#include "SceneEditor.hpp"
#include "ui_SceneEditor.h"

#include <CoreFrames/MetaObjectItemModel.hpp>
#include <CoreFrames/LogView.hpp>
#include <CameraController/FirstPersonController.hpp>

#include "SceneItemModel.hpp"

#include <Render/RenderComponentWidget.hpp>

#include <Core/Serializer.hpp>
#include <Core/ClassInfo.hpp>

#include <SceneGraph/Scene.hpp>
#include <SceneGraph/ActorManager.hpp>
#include <SceneGraph/NaiveActorManager.hpp>

#include <MeshSystem/Mesh.hpp>

#include <Render/RenderComponentWidget.hpp>
#include <Render/RenderSystem.hpp>

#include <Pipeline/AmbientLight.hpp>
#include <Pipeline/Fragment.hpp>
#include <Pipeline/InstancedFragment.hpp>
#include <Pipeline/GridFragment.hpp>

#include <Physics/CollideInfo.hpp>

#include <GameSystem/Game.hpp>

#include <QDesktopWidget>
#include <QClipboard>
#include <QMouseEvent>
#include <QMessageBox>
#include <QInputDialog>

Q_REGISTER_RESOURCE_EDITOR(Scene, SceneEditor)

inline QAction* addSeparator(QObject* parent = 0)
{
  QAction* action = new QAction(parent);
  action->setSeparator(true);
  return action;
}

SceneEditor::SceneEditor(QWidget *parent) :
  FrameEditor(parent)
{
  Ui::SceneEditor::setupUi(this);
  logsWidget->setWidget(new LogView(logsWidget));

  connect(QApplication::clipboard(), SIGNAL(changed(QClipboard::Mode)), SLOT(clipboardChanged(QClipboard::Mode)));

  if(QActionGroup* editGroup = new QActionGroup(this))
  {
    editGroup->addAction(actionSelectMode);
    editGroup->addAction(actionMoveMode);
    editGroup->addAction(actionRotateMode);
  }

  if(QActionGroup* gridGroup = new QActionGroup(this))
  {
    gridGroup->addAction(actionXAxis);
    gridGroup->addAction(actionYAxis);
    gridGroup->addAction(actionZAxis);
  }

  if(QActionGroup* copyCutPasteGroup = new QActionGroup(this))
  {
    copyCutPasteGroup->setExclusive(false);
    copyCutPasteGroup->addAction(actionCut);
    copyCutPasteGroup->addAction(actionCopy);
    copyCutPasteGroup->addAction(actionPaste);
  }

  if(QActionGroup* groupGroup = new QActionGroup(this))
  {
    groupGroup->setExclusive(false);
    groupGroup->addAction(actionGroupSelected);
    groupGroup->addAction(actionUngroupSelected);
  }

  m_selectBand = NULL;

  sceneTreeView->addAction(actionCut);
  sceneTreeView->addAction(actionCopy);
  sceneTreeView->addAction(actionPaste);
  sceneTreeView->addAction(addSeparator(sceneTreeView));
  sceneTreeView->addAction(actionReplaceActor);
  sceneTreeView->addAction(actionDeleteActors);
  sceneTreeView->addAction(addSeparator(sceneTreeView));
  sceneTreeView->addAction(actionCloneSelected);
  sceneTreeView->addAction(addSeparator(sceneTreeView));
  sceneTreeView->addAction(actionGroupSelected);
  sceneTreeView->addAction(actionUngroupSelected);
  sceneTreeView->addAction(actionShowGrouped);
  sceneTreeView->addAction(addSeparator(sceneTreeView));
  sceneTreeView->addAction(actionGoCamera);

  m_cameraController.reset(new FirstPersonController(renderComponents()));

  m_sceneItemModel.reset(new SceneItemModel());
  sceneTreeView->setModel(m_sceneItemModel.data());

  connect(sceneTreeView->selectionModel(), SIGNAL(currentChanged(QModelIndex,QModelIndex)), SLOT(currentChanged(QModelIndex)));
  connect(sceneTreeView->selectionModel(), SIGNAL(selectionChanged(QItemSelection,QItemSelection)), SLOT(actorVisibilityChanged()));
  connect(m_sceneItemModel.data(), SIGNAL(graphicsChanged(QModelIndex)), SLOT(actorVisibilityChanged()), Qt::DirectConnection);

  m_actorTypesItemModel.reset(new MetaObjectItemModel(&Actor::staticMetaObject));
  m_actorTypesItemModel->setCheckable(true);
  actorTypesView->setModel(m_actorTypesItemModel.data());
  connect(m_actorTypesItemModel.data(), SIGNAL(checkedChanged(const QMetaObject*,bool)), SLOT(actorVisibilityChanged()));

  sceneLayout->addWidget(renderComponents());

  renderComponents()->installEventFilter(this);
}

SceneEditor::~SceneEditor()
{
}

const QMetaObject * SceneEditor::resourceType() const
{
  return &Scene::staticMetaObject;
}

QSharedPointer<Resource> SceneEditor::resource() const
{
  return m_sceneItemModel->scene();
}

bool SceneEditor::setResource(const QSharedPointer<Resource> &newResource)
{
  if(newResource.isNull())
  {
    m_sceneItemModel->setScene(QSharedPointer<Scene>());
    setPropertyObject(NULL);
    updateUi();
    return true;
  }

  if(newResource.objectCast<Scene>().isNull())
    return false;

  m_sceneItemModel->setScene(newResource.objectCast<Scene>());

  connect(m_sceneItemModel->scene().data(), SIGNAL(objectNameChanged()), SLOT(updateUi()));

  if(QSharedPointer<Actor> camera = m_sceneItemModel->scene()->cameraActor())
  {
    m_cameraController->setOrigin(camera->worldOrigin());
  }
  else
  {
    m_cameraController->setOrigin(m_sceneItemModel->scene()->rootActor()->sphereBounds().Center);
  }

  Mesh::optimizeRender();

  setPropertyObject(m_sceneItemModel->scene().data());

  updateUi();

  return true;
}

bool SceneEditor::eventFilter(QObject *widget, QEvent *event)
{
    if(renderComponents() == widget && m_sceneItemModel->scene())
  {
    QMouseEvent* mouseEvent = (QMouseEvent*)event;

    switch(event->type())
    {
    case QEvent::MouseButtonPress:
      if(mouseEvent->buttons() == Qt::LeftButton)
      {
        if(pivotMousePress(mouseEvent))
          return true;
        if(actionSelectMode->isChecked())
          return selectMousePress(mouseEvent);
        else if(actionMoveMode->isChecked())
          return moveMousePress(mouseEvent);
        else if(actionRotateMode->isChecked())
          return rotateMousePress(mouseEvent);
      }
      return false;

    case QEvent::MouseMove:
      if(mouseEvent->buttons() == Qt::LeftButton)
      {
        if(actionSelectMode->isChecked())
          return selectMouseMove(mouseEvent);
        else if(actionMoveMode->isChecked())
          return moveMouseMove(mouseEvent);
        else if(actionRotateMode->isChecked())
          return rotateMouseMove(mouseEvent);
      }
      return false;

    case QEvent::MouseButtonRelease:
      if(mouseEvent->button() == Qt::LeftButton && mouseEvent->buttons() == Qt::NoButton)
      {
        if(actionSelectMode->isChecked())
          return selectMouseRelease(mouseEvent);
        else if(actionMoveMode->isChecked())
          return moveMouseRelease(mouseEvent);
        else if(actionRotateMode->isChecked())
          return rotateMouseRelease(mouseEvent);
      }
      return false;

    default:
      break;
    }
  }
  return false;
}

void SceneEditor::actorVisibilityChanged()
{
  renderComponents()->update();

  actionCopy->setEnabled(sceneTreeView->selectionModel()->hasSelection());
  actionCut->setEnabled(sceneTreeView->selectionModel()->hasSelection());
}

void SceneEditor::on_actionShowSelected_triggered()
{
  QSet<Actor*> actors = selected();

  foreach(Actor* actor, m_sceneItemModel->all())
  {
    if(actors.contains(actor))
      m_hidden.remove(actor);
    else
      m_hidden.insert(actor);
  }
  actorVisibilityChanged();
}

void SceneEditor::on_actionHideSelected_triggered()
{
  m_hidden.unite(selected());
  sceneTreeView->selectionModel()->clearSelection();
  actorVisibilityChanged();
  setPropertyObject(NULL);
}

void SceneEditor::on_actionInvertSelection_triggered()
{
  QItemSelection newSelection;

  QSet<Actor*> actors = selected();

  foreach(Actor* actor, m_sceneItemModel->all())
  {
    if(m_hidden.contains(actor) || actors.contains(actor))
      continue;
    QModelIndex index = m_sceneItemModel->index(actor);
    if(!index.isValid())
      continue;
    newSelection.select(index, index);
  }

  sceneTreeView->selectionModel()->select(newSelection, QItemSelectionModel::ClearAndSelect);

  setPropertyObject(NULL);
}

void SceneEditor::on_actionShowAll_triggered()
{
  m_hidden.clear();
  actorVisibilityChanged();
  setPropertyObject(NULL);
}

void SceneEditor::on_actionDeselectActors_triggered()
{
  sceneTreeView->selectionModel()->clearSelection();
  actorVisibilityChanged();
  setPropertyObject(NULL);
  actionSelectMode->setChecked(true);
}

void SceneEditor::on_sceneTreeView_doubleClicked(const QModelIndex &index)
{
  if(!index.isValid())
    return;

  setPropertyObject(m_sceneItemModel->toActor(index));
}

void SceneEditor::on_actionDeleteActors_triggered(bool force)
{
  if(force || QMessageBox::question(this, "Delete", "Are you sure?", QMessageBox::Yes|QMessageBox::No, QMessageBox::No) == QMessageBox::Yes)
  {
    QModelIndexList indexList = sceneTreeView->selectionModel()->selectedIndexes();

    sceneTreeView->selectionModel()->clearSelection();

    foreach(QModelIndex index, indexList)
    {
      if(index.isValid())
      {
        Actor* actor = m_sceneItemModel->toActor(index);
        if(!actor)
          continue;
        if(!actor->parentActor())
          continue;
        actor->removeFromScene();
      }
    }

    if(m_sceneItemModel->scene() && m_sceneItemModel->scene()->rootActor())
      m_sceneItemModel->scene()->rootActor()->removeActors();

    actorVisibilityChanged();
    setPropertyObject(NULL);
    actionSelectMode->setChecked(true);
  }
}

void SceneEditor::on_actionShowGrid_triggered()
{
  renderComponents()->update();
}

void SceneEditor::on_actionSnapGrid_triggered()
{
  renderComponents()->update();
}

void SceneEditor::on_actionXAxis_triggered()
{
  renderComponents()->update();
}

void SceneEditor::on_actionYAxis_triggered()
{
  renderComponents()->update();
}

void SceneEditor::on_actionZAxis_triggered()
{
  renderComponents()->update();
}

QSet<Actor *> SceneEditor::selected() const
{
  QSet<Actor*> selected;

  foreach(const QModelIndex& index, sceneTreeView->selectionModel()->selectedIndexes())
  {
    Actor* actor = m_sceneItemModel->toActor(index);
    if(!actor)
      continue;    
    for(ActorManager* manager = actor->parentActor().data(); manager; manager = manager->parentActor().data())
    {
      if(manager->grouped())
      {
        actor = NULL;
        break;
      }
    }

    if(actor)
    {
      selected.insert(actor);
    }
  }
  return selected;
}

void SceneEditor::on_actionCloneSelected_triggered()
{
  if(!m_sceneItemModel->scene())
    return;

  QItemSelection newSelection;

  Actor* newActorSelected = NULL;

  vec3 offset(1.0f, 1.0f, 1.0f);

  offset -= moveAxis() * (moveAxis() ^ offset);

  foreach(Actor* actor, selected())
  {
    if(actor == m_sceneItemModel->scene()->rootActor().data())
      continue;

    QSharedPointer<Actor> newActor(actor->clone());
    if(newActor)
    {
      newActor->setWorldOrigin(actor->worldOrigin() + offset);
      m_sceneItemModel->scene()->rootActor()->addActor(newActor);

      QModelIndex newIndex = m_sceneItemModel->index(newActor.data());

      if(newIndex.isValid())
        newSelection.select(newIndex, newIndex);

      newActorSelected = newActor.data();
    }
  }

  sceneTreeView->selectionModel()->select(newSelection, QItemSelectionModel::ClearAndSelect);

  setPropertyObject(newActorSelected);
}

void SceneEditor::on_actionGroupSelected_triggered()
{
  if(!m_sceneItemModel->scene())
    return;

  QSharedPointer<ActorManager> newManager(new NaiveActorManager());

  foreach(Actor* actor, selected())
  {
    if(!actor->parentActor())
      continue;

    if(ActorManager* manager = qobject_cast<ActorManager*>(actor))
    {
      foreach(const QSharedPointer<Actor>& child, manager->actors())
      {
        matrix transform = child->worldTransform();
        child->setParentActor(newManager);
        child->setWorldTransform(transform);
      }
      manager->setParentActor(ActorManagerRef());
    }
    else
    {
      matrix transform = actor->worldTransform();
      actor->setParentActor(newManager);
      actor->setWorldTransform(transform);
    }
  }

  if(newManager->actors().size())
  {
    newManager->setGrouped(true);
    newManager->setParentActor(m_sceneItemModel->scene()->rootActor());

    QModelIndex newIndex = m_sceneItemModel->index(newManager.data());

    if(newIndex.isValid())
    {
      sceneTreeView->selectionModel()->reset();
      sceneTreeView->selectionModel()->select(newIndex, QItemSelectionModel::ClearAndSelect);
    }

    setPropertyObject(newManager.data());
  }
  else
  {
    sceneTreeView->selectionModel()->clearSelection();

    setPropertyObject(NULL);
  }
}

void SceneEditor::on_actionUngroupSelected_triggered()
{
  if(!m_sceneItemModel->scene())
    return;

  QItemSelection newSelection;

  foreach(Actor* actor, selected())
  {
    if(!actor->parentActor())
      continue;

    if(ActorManager* manager = qobject_cast<ActorManager*>(actor))
    {
      foreach(const QSharedPointer<Actor>& child, manager->actors())
      {
        matrix transform = child->worldTransform();
        child->setParentActor(manager->parentActor());
        child->setWorldTransform(transform);

        QModelIndex newIndex = m_sceneItemModel->index(child.data());

        if(newIndex.isValid())
        {
          newSelection.select(newIndex, newIndex);
        }
      }

      manager->setParentActor(ActorManagerRef());
    }
    else if(0)
    {
      matrix transform = actor->worldTransform();
      actor->setParentActor(m_sceneItemModel->scene()->rootActor());
      actor->setWorldTransform(transform);

      QModelIndex newIndex = m_sceneItemModel->index(actor);

      if(newIndex.isValid())
      {
        newSelection.select(newIndex, newIndex);
      }
    }
  }

  sceneTreeView->selectionModel()->reset();
  sceneTreeView->selectionModel()->select(newSelection, QItemSelectionModel::ClearAndSelect);

  setPropertyObject(NULL);
}

void SceneEditor::on_actionCut_triggered()
{
  on_actionCopy_triggered();
  on_actionDeleteActors_triggered(true);
  setPropertyObject(NULL);
}

void SceneEditor::on_actionCopy_triggered()
{
  QSharedPointer<ActorManager> manager(new NaiveActorManager());

  foreach(Actor* actor, selected())
  {
    QSharedPointer<Actor> newActor(actor->clone());
    if(!newActor)
      continue;

    newActor->setParentActor(manager);
    newActor->setWorldTransform(actor->worldTransform());
  }

  if(manager->actors().isEmpty())
    return;

  QBuffer buffer;

  if(buffer.open(QBuffer::WriteOnly))
  {
    if(manager->actors().size() == 1)
      ::saveToDevice(manager->actors()[0], buffer);
    else
      ::saveToDevice(manager, buffer);

    if(buffer.data().isEmpty())
      return;

    QMimeData* mimeData = new QMimeData();
    mimeData->setData("SceneEditor", buffer.data());

    QApplication::clipboard()->clear();
    QApplication::clipboard()->setMimeData(mimeData);
    actionSelectMode->setChecked(true);
  }
}

void SceneEditor::on_actionPaste_triggered()
{
  if(!m_sceneItemModel->scene())
    return;

  if(const QMimeData* mimeData = QApplication::clipboard()->mimeData())
  {
    if(mimeData->hasFormat("SceneEditor"))
    {
      QItemSelection newSelection;

      QBuffer buffer;
      buffer.setData(mimeData->data("SceneEditor"));
      if(!buffer.open(QBuffer::ReadOnly))
        return;

      QSharedPointer<Actor> newActor = ::loadFromDevice(buffer).objectCast<Actor>();

      if(newActor)
      {
        newActor->setParentActor(m_sceneItemModel->scene()->rootActor());

        QModelIndex newIndex = m_sceneItemModel->index(newActor.data());

        if(newIndex.isValid())
        {
          newSelection.select(newIndex, newIndex);

          setPropertyObject(newActor.data());
        }
      }

      QApplication::clipboard()->clear();

      sceneTreeView->selectionModel()->select(newSelection, QItemSelectionModel::ClearAndSelect);
    }
  }

  updateUi();
}

void SceneEditor::updateUi()
{
  FrameEditor::updateUi();
  clipboardChanged(QClipboard::Clipboard);
  currentChanged(sceneTreeView->selectionModel()->currentIndex());
}

void SceneEditor::clipboardChanged(QClipboard::Mode mode)
{
  if(mode != QClipboard::Clipboard)
    return;

  if(const QMimeData* mimeData = QApplication::clipboard()->mimeData())
  {
    if(mimeData->hasFormat("SceneEditor"))
      actionPaste->setEnabled(true);
    else
      actionPaste->setEnabled(false);
  }
  else
  {
    actionPaste->setEnabled(false);
  }
}

void SceneEditor::on_actionReplaceActor_triggered()
{
  QModelIndex index = sceneTreeView->selectionModel()->currentIndex();
  if(!index.isValid())
    return;

  QSharedPointer<Actor> actor = m_sceneItemModel->toActorRef(index);
  if(!actor)
    return;

  QHash<QString, const QMetaObject*> classes;

  foreach(const QMetaObject* metaObject, ClassInfo::metaObjects(actor->type()))
  {
    if(!metaObject->constructorCount())
      continue;

    classes[metaObject->className()] = metaObject;
  }

  QStringList classNames(classes.keys());

  QString selectedClass = QInputDialog::getItem(this, "Promote actor", "Select class", classNames, classNames.indexOf(actor->metaObject()->className()), false);
  if(selectedClass.isEmpty() || selectedClass == actor->metaObject()->className())
    return;

  const QMetaObject* metaObject = classes[selectedClass];
  if(!metaObject)
    return;

  QSharedPointer<Actor> newActor((Actor*)metaObject->newInstance());
  if(!newActor)
    return;

  sceneTreeView->selectionModel()->clearSelection();

  for(unsigned i = 0; i < metaObject->propertyCount(); ++i)
  {
    QMetaProperty metaProperty(metaObject->property(i));
    if(!metaProperty.isStored(actor.data()))
      continue;

    newActor->setProperty(metaProperty.name(), actor->property(metaProperty.name()));
  }

  if(m_sceneItemModel->scene()->rootActor() == actor)
  {
    if(QSharedPointer<ActorManager> manager = newActor.objectCast<ActorManager>())
    {
      m_sceneItemModel->scene()->setRootActor(manager);
      sceneTreeView->selectionModel()->setCurrentIndex(m_sceneItemModel->index(manager.data()), QItemSelectionModel::ClearAndSelect);
    }
  }
  else
  {
    newActor->setParentActor(actor->parentActor());
    actor->setParentActor(QSharedPointer<ActorManager>());
    sceneTreeView->selectionModel()->setCurrentIndex(m_sceneItemModel->index(newActor.data()), QItemSelectionModel::ClearAndSelect);
  }

  setPropertyObject(newActor.data());
}

void SceneEditor::on_actionShowGrouped_toggled(bool arg1)
{
  QModelIndex index = sceneTreeView->selectionModel()->currentIndex();

  if(index.isValid())
  {
    if(ActorManager* manager = qobject_cast<ActorManager*>(m_sceneItemModel->toActor(index)))
    {
      manager->setGrouped(arg1);
    }
  }
}

void SceneEditor::currentChanged(const QModelIndex &index)
{
  if(index.isValid())
  {
    actionGoCamera->setEnabled(true);

    if(ActorManager* manager = qobject_cast<ActorManager*>(m_sceneItemModel->toActor(index)))
    {
      actionShowGrouped->setEnabled(true);
      actionShowGrouped->setChecked(manager->grouped());
      return;
    }
  }
  else
  {
    actionGoCamera->setEnabled(false);
  }

  actionShowGrouped->setEnabled(false);
  actionShowGrouped->setChecked(false);
}

void SceneEditor::on_actionEditMode_toggled(bool arg1)
{
  renderComponents()->update();
}

void SceneEditor::on_actionBuild_triggered()
{
  if(m_sceneItemModel->scene())
    m_sceneItemModel->scene()->compile();
}

#include <Windows.h>

void SceneEditor::on_actionPlay_triggered()
{
  if(!m_sceneItemModel->scene())
    return;

  Game game(this);

  if(!game.askForResolution())
      return;

  // game.setFixedSize(sizes[selected]);

  //if(selected & 1)
  //  game.showFullScreen();
  //else
  //  game.show();

  if(game.beginGame(m_sceneItemModel->scene()))
  {
    game.run();
    game.endGame();
  }
}

void SceneEditor::on_actionGoCamera_triggered()
{
  if(m_sceneItemModel->scene())
  {
#if 1
    if(sceneTreeView->currentIndex().isValid())
    {
      Actor* actor = (Actor*)sceneTreeView->currentIndex().internalPointer();

      if(actor)
      {
        m_cameraController->setOrigin(actor->worldBoxBounds().center());
        renderComponents()->update();
      }
    }
#else
    if(QSharedPointer<Actor> camera = m_sceneItemModel->scene()->cameraActor())
    {
      m_cameraController->setOrigin(camera->worldOrigin());
      renderComponents()->update();
    }
#endif
  }
}

#include <SceneGraph/SectorActor.hpp>
#include <SceneGraph/PortalActor.hpp>

void SceneEditor::on_actorTypesView_doubleClicked(const QModelIndex &index)
{
  const QMetaObject* metaObject = (const QMetaObject*)index.internalPointer();
  if(!metaObject)
    return;

  if(metaObject == &SectorActor::staticMetaObject)
  {
    QSharedPointer<ActorManager> manager;
    box bounds;

    foreach(Actor* actor, selected())
    {
      if(!actor->extendBounds())
        continue;

      if(manager)
      {
        if(manager != actor->parentActor())
        {
          QMessageBox::warning(this, metaObject->className(), "Actors selected from multiple nodes!");
          return;
        }

        bounds = box::merge(bounds, actor->boxBounds());
      }
      else
      {
        manager = actor->parentActor();
        bounds = actor->boxBounds();
      }
    }

    if(!manager)
    {
      QMessageBox::warning(this, metaObject->className(), "Non movable actor selected!");
      return;
    }

    QSharedPointer<SectorActor> newSector(new SectorActor());
    newSector->setOrigin(bounds.center());
    newSector->setScale(bounds.size() * 0.5f);
    newSector->setParentActor(manager);

    sceneTreeView->selectionModel()->select(m_sceneItemModel->index(newSector.data()), QItemSelectionModel::ClearAndSelect);
    setPropertyObject(newSector.data());
  }
  else
  {
    QSharedPointer<Actor> newActor((Actor*)metaObject->newInstance());
    if(!newActor)
      return;

    newActor->setOrigin(newOrigin());
    newActor->setParentActor(m_sceneItemModel->scene()->rootActor());
    setPropertyObject(newActor.data());
  }
}
