#include "Game.hpp"

#include <Core/Serializer.hpp>

#include <Pipeline/AmbientLight.hpp>
#include <Pipeline/SolidFrame.hpp>
#include <Pipeline/ForwardShadedFrame.hpp>
#include <Pipeline/DeferredShadedFrame.hpp>

#include <Pipeline/BloomComponent.hpp>
#include <Pipeline/HdrComponent.hpp>

#include <Render/RenderSystem.hpp>

#include <Windows.h>
#include <QApplication>
#include <QAction>
#include <QInputDialog>

Game::Game(QWidget *parent)
  : RenderComponentWidget(parent), m_bloomComponent(NULL), m_frame(NULL)
{
  setWindowTitle("Game");
  setWindowModality(Qt::ApplicationModal);
  setFocusPolicy(Qt::ClickFocus);
  move(0, 0);
  setFixedSize(1024, 768);
  setSwapBefore(true);
  // show();

  // select rendering mode
  QString renderMode = getSettings().value("Render/RenderMode", "Deferred").toString();
  if(renderMode == "Solid")
    m_frame = new SolidFrame();
  else if(renderMode == "Forward")
    m_frame = new ForwardShadedFrame();
  else
    m_frame = new DeferredShadedFrame();
  addComponent(m_frame);

  // select hdr mode
  QString hdrMode = getSettings().value("Render/HdrMode", "Full").toString();
  if(hdrMode == "Full")
    m_bloomComponent = new HdrComponent(true);
  else if(hdrMode == "FullNoEdges")
    m_bloomComponent = new HdrComponent(false);
  else if(hdrMode == "Bloom")
    m_bloomComponent = new BloomComponent();

  if(m_bloomComponent)
  {
    m_bloomComponent->setPreset(BloomComponent::Subtle);
    addComponent(m_bloomComponent);
  }

  m_deltaTime = 0.0f;
  m_mouseInvalid = true;

  m_inputMouseSensitivity = M_PI / 2.0f / 300.0f;

  m_fpsFrames = 0;
  m_fps = 0;
}

Game::~Game()
{
}

void Game::run()
{
#if 1
    MSG mssg;

    PeekMessage(&mssg, NULL, 0, 0, PM_NOREMOVE);

    while(mssg.message != WM_QUIT && isGame())
    {
      if(PeekMessage(&mssg, NULL, 0, 0, PM_REMOVE))
      {
        TranslateMessage(&mssg);
        DispatchMessage(&mssg);
      }
      else
      {
        update();
      }
    }
#else
    bool wasActive = false;

    while(isGame())
    {
      if(!isActive())
      {
        QApplication::processEvents(QEventLoop::AllEvents, 100);
        wasActive = false;
      }
      else
      {
        if(!wasActive)
          resetTimer();
        update();
        wasActive = true;
      }

      if(QApplication::hasPendingEvents())
        QApplication::flush();
    }
#endif
}

void Game::prepareComponents()
{
  if(!m_frame)
    return;

  getRenderSystem().resetStats();

  m_frame->clear();

  m_frame->setDeltaTime(m_deltaTime);

  if(m_scene)
  {
    m_frame->setFrameTime(m_scene->gameTime());

    Camera camera;

    if(!m_scene->camera(camera, (float)width() / height()))
      return;

    m_frame->setViewport(geometry());
    m_frame->setCamera(camera);

    QElapsedTimer timer;
    timer.start();
    m_scene->generateFrame(*m_frame);
    m_generateTime = timer.elapsed() / 1000.0f;
  }

  m_frame->addLight(new AmbientLight());
}

void Game::finalizeComponents()
{
    QStringList lines = toStringList();

    if(lines.length())
    {
        getRenderSystem().drawText(vec2(0,0), lines.join("\n"));
    }
}

QStringList Game::toStringList() const
{
    int debugLevel = getSettings().value("Render/DebugLevel", 1).toInt();

    QStringList lines = RenderComponentWidget::toStringList();

    if(lines.size())
        lines.append("");

    if(m_frame && debugLevel > 3)
    {
      lines.append(QString("Origin: (%1, %2, %3)").arg(QString::number(m_frame->camera().origin.X), QString::number(m_frame->camera().origin.Y), QString::number(m_frame->camera().origin.Z)));
      lines.append(QString("At: (%1, %2, %3)").arg(QString::number(m_frame->camera().at.X), QString::number(m_frame->camera().at.Y), QString::number(m_frame->camera().at.Z)));
      lines.append(QString("Up: (%1, %2, %3)").arg(QString::number(m_frame->camera().up.X), QString::number(m_frame->camera().up.Y), QString::number(m_frame->camera().up.Z)));
      lines.append("");
    }

    lines.append(QString("Fps: %1").arg(QString::number(m_fps, 'f', 1)));
    lines.append(QString("DeltaTime: %1ms").arg(QString::number(m_deltaTime * 1000, 'f', 1)));
    lines.append(QString("GenerateTime: %1ms").arg(QString::number(m_generateTime * 1000, 'f', 1)));

    lines.append("");

    if(debugLevel >= 2)
    {
        if(m_scene->cubeShadowFrame())
        {
          lines.append(QString().sprintf("CubeShadows: %i of %i (%iMB)", m_scene->cubeShadowFrame()->shadowUsedCount(), m_scene->cubeShadowFrame()->shadowCount(), m_scene->cubeShadowFrame()->estimatedMemUsage() >> 20));
        }

        if(m_scene->shadowFrame())
        {
          lines.append(QString().sprintf("Shadows: %i of %i (%iMB)", m_scene->shadowFrame()->shadowUsedCount(), m_scene->shadowFrame()->shadowCount(), m_scene->shadowFrame()->estimatedMemUsage() >> 20));
        }

        lines.append("");
    }

    if(debugLevel >= 4)
    {
      lines.append(getRenderSystem().stats().toStringList());
    }

    return lines;
}

void Game::updateInput()
{
  GameInputData data;  

  if(GetAsyncKeyState(VK_ESCAPE) & 0x8000)
  {
    hide();
    return;
  }

#if 1
  // get mouse state
  QPoint center = mapToGlobal(geometry().center());

  if(!m_mouseInvalid)
  {
    QPoint current = QCursor::pos();
    data.dX = (current.x() - center.x()) * m_inputMouseSensitivity;
    data.dY = (current.y() - center.y()) * m_inputMouseSensitivity;
    data.X = current.x();
    data.Y = current.y();
  }

  if(GetAsyncKeyState(VK_SCROLL) & 0x8000)
  {
    setCursor(Qt::ArrowCursor);
    data.X = data.Y = 0;
    data.dX = data.dY = 0;
    data.Fire = false;
  }
  else
  {
    setCursor(Qt::BlankCursor);
  }

  m_mouseInvalid = false;
  QCursor::setPos(center);
#endif

  // get keyboard state
#if 1
  data.Up = GetAsyncKeyState(VK_UP) & 0x8000 ? true : false;
  data.Down = GetAsyncKeyState(VK_DOWN) & 0x8000 ? true : false;
  data.Left = GetAsyncKeyState(VK_LEFT) & 0x8000 ? true : false;
  data.Right = GetAsyncKeyState(VK_RIGHT) & 0x8000 ? true : false;
  data.Fire = QApplication::mouseButtons() & Qt::LeftButton ? true : false;
  data.Jump = GetAsyncKeyState(VK_SPACE) & 0x8000 ? true : false;
#endif

  // set delta time
  data.Delta = m_deltaTime;

  updateInput(data);
}

void Game::updateInput(const GameInputData &data)
{
  if(m_scene)
    m_scene->input(data);
}

void Game::updateTime(float dt)
{
  if(m_scene)
    m_scene->update(dt);
}

void Game::update()
{
  if(!m_timer.isValid())
    return;

  if(m_fpsTimer.elapsed() >= 1000)
  {
    m_fps = m_fpsFrames * 1000.0f / m_fpsTimer.restart();
    m_fpsFrames = 1;
  }
  else
  {
    ++m_fpsFrames;
  }

  m_deltaTime = m_timer.restart() / 1000.0;

  if(hasFocus())
    updateInput();

  updateTime(m_deltaTime);

  //if(isActive())
  repaint();
}

bool Game::askForResolution()
{
    QVector<QSize> sizes;
    sizes.append(QSize(800, 600));
    sizes.append(QSize(1024, 768));
    sizes.append(QSize(1280, 1024));
    sizes.append(QSize(1920, 1440));
    sizes.append(QSize(1280, 800));
    sizes.append(QSize(1360, 768));
    sizes.append(QSize(1680, 1050));
    sizes.append(QSize(1920, 1080));

    QStringList sizeList;
    foreach(QSize size, sizes)
    {
        sizeList << QString("%1x%2 [Windowed]").arg(size.width()).arg(size.height());
        sizeList << QString("%1x%2 [Fullscreen]").arg(size.width()).arg(size.height());
    }

    static int selected = 2;
    bool ok = false;
    selected = sizeList.indexOf(QInputDialog::getItem(this, windowTitle(), "Select resolution:", sizeList, selected, false, &ok));
    if(!ok || selected < 0)
      return false;

    setFixedSize(sizes[selected / 2]);
    if(selected & 1)
        showFullScreen();
    else
        show();

    return true;
}

bool Game::beginGame(const QSharedPointer<Scene> &scene)
{
  if(m_scene)
    return false;

  m_scene = shallowCopy(scene).objectCast<Scene>();
  if(!m_scene)
    return false;

  m_scene->compile();
  m_scene->prepare();

  if(m_scene->beginGame())
  {
    setWindowTitle(QString("Game [%1]").arg(m_scene->objectName()));
    m_timer.start();
    m_fpsTimer.start();
    return true;
  }
  m_scene.clear();
  return false;
}

void Game::endGame()
{
  if(!m_scene)
    return;  

  setWindowTitle("Game");
  m_scene->endGame();
  m_frame->clear();
  m_scene.clear();
  m_timer.invalidate();
}

bool Game::isGame() const
{
  if(!m_scene)
    return false;
  if(isHidden())
    return false;
  return true;
}

bool Game::isActive() const
{
  if(!m_scene)
    return false;
#if 0
  return true;
#else
  if(hasFocus())
    return true;
  return false;
#endif
}

void Game::resetTimer()
{
  if(!isGame())
    return;
  m_timer.start();
}

void Game::closeEvent(QCloseEvent *)
{
  hide();
}
