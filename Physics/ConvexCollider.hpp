#ifndef CONVEXCOLLIDER_HPP
#define CONVEXCOLLIDER_HPP

#include "Collider.hpp"
#include <QVector>

class NxConvexShapeDesc;

class PHYSICS_EXPORT ConvexCollider : public Collider
{
  Q_OBJECT

private:
  NxConvexShapeDesc* m_shapeDesc;

  // Constructor
public:
  ConvexCollider();
  ConvexCollider(const QByteArray& convexCollider);

  // Destructor
public:
  ~ConvexCollider();

  // Methods
public:
  NxShapeDesc* shapeDesc();

  // Static Methods
public:
  static QByteArray cookConvexMesh(
    const void* vertices, unsigned vertexCount, unsigned vertexStride);

  static QByteArray cookConvexMesh(
    const void* vertices, unsigned vertexCount, unsigned vertexStride,
    const void* indices, unsigned indexCount, unsigned indexStride,
    bool use16indices = false);

  //! Tworzy convex mesha z wierzchołków
  template<typename VertexType>
  static QByteArray cookConvexMesh(const QVector<VertexType>& vertices)
  {
    return cookConvexMesh(vertices.constData(), vertices.size(), sizeof(VertexType));
  }

  //! Tworzy convex mesha z 16 bitowych indeksów
  template<typename VertexType>
  static QByteArray cookConvexMesh(
    const QVector<VertexType>& vertices,
    const QVector<ushort>& indices)
  {
    return cookConvexMesh(
          vertices.constData(), vertices.size(), sizeof(VertexType),
          indices.constData(), indices.size(), sizeof(ushort),
          true, false);
  }

  //! Tworzy convex mesha z 32 bitowych indeksów
  template<typename VertexType>
  static QByteArray cookConvexMesh(
                                   const QVector<VertexType>& vertices,
                                   const QVector<unsigned>& indices)
  {
    return cookConvexMesh(
          vertices.constData(), vertices.size(), sizeof(VertexType),
          indices.constData(), indices.size(), sizeof(unsigned),
          false, false);
  }
};

#endif // CONVEXCOLLIDER_HPP
