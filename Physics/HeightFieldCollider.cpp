#include "HeightFieldCollider.hpp"
#include "PhysicsEngine.hpp"

#include <NxHeightField.h>
#include <NxHeightFieldDesc.h>
#include <NxHeightFieldShapeDesc.h>
#include <NxHeightFieldSample.h>

#include <QDebug>
#include <qmath.h>

struct s16tm : public NxHeightFieldSample
{
  s16tm()
  {
  }
  s16tm(qint16 newHeight)
  {
    height = newHeight;
    materialIndex0 = 0;
    tessFlag = false;
    materialIndex1 = 0;
    unused = 0;
  }
};

HeightFieldCollider::HeightFieldCollider(const QVector<qint16>& heights, unsigned columns, unsigned rows)
  : m_shapeDesc(new NxHeightFieldShapeDesc()), m_heightField(NULL)
{
  QVector<s16tm> newHeights(columns * rows, s16tm());

  // copy heightmap to new buffer
  QVector<s16tm>::iterator heightOutput;
  heightOutput = qCopy(heights.begin(), heights.begin() + qMin(newHeights.size(), heights.size()), newHeights.begin());
  qFill(heightOutput, newHeights.end(), s16tm(0));

  // create heightField
  NxHeightFieldDesc fieldDesc;
  fieldDesc.samples = newHeights.data();
  fieldDesc.sampleStride = sizeof(s16tm);
  fieldDesc.nbColumns = columns;
  fieldDesc.nbRows = rows;
  m_heightField = getPhysicsEngine()->createHeightField(fieldDesc);
  if(!m_heightField)
  {
    qDebug() << "[COLLIDER] Invalid heightField data";
    return;
  }

  // create shape
  m_shapeDesc->heightField = m_heightField;
  m_shapeDesc->userData = this;
  m_shapeDesc->localPose.M.rotZ(M_PI/2.0f);
  // m_shapeDesc->group = GROUP_COLLIDABLE_NON_PUSHABLE;
}

HeightFieldCollider::~HeightFieldCollider()
{
  delete m_shapeDesc;

  if(m_heightField)
  {
    getPhysicsEngine()->releaseHeightField(*m_heightField);
    m_heightField = NULL;
  }
}

float HeightFieldCollider::rowScale() const
{
  return m_shapeDesc->rowScale;
}

void HeightFieldCollider::setRowScale(float rowScale)
{
  m_shapeDesc->rowScale = rowScale;
}

float HeightFieldCollider::columnScale() const
{
  return m_shapeDesc->columnScale;
}

void HeightFieldCollider::setColumnScale(float columnScale)
{
  m_shapeDesc->columnScale = columnScale;
}

float HeightFieldCollider::heightScale() const
{
  return m_shapeDesc->heightScale;
}

void HeightFieldCollider::setHeightScale(float heightScale)
{
  m_shapeDesc->heightScale = heightScale;
}

NxShapeDesc * HeightFieldCollider::shapeDesc()
{
  return m_shapeDesc;
}
