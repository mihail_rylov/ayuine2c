#ifndef PHYSICSENGINE_P_HPP
#define PHYSICSENGINE_P_HPP

#include <Math/Vec3.hpp>
#include <Math/Matrix.hpp>

#include <NxVec3.h>
#include <NxExtended.h>
#include <NxMat34.h>

inline vec3 toVec3(const NxVec3& v)
{
  return vec3(v.x, v.y, v.z);
}

inline vec3 toVec3(const NxExtendedVec3& v)
{
  return vec3(v.x, v.y, v.z);
}

inline NxVec3 fromVec3(const vec3& v)
{
  return NxVec3(v.X, v.Y, v.Z);
}

inline NxExtendedVec3 fromVec3e(const vec3& v)
{
  return NxExtendedVec3(v.X, v.Y, v.Z);
}

inline matrix toMatrix(const NxMat34& v)
{
  matrix m;
  v.getRowMajor44(&m.m11);
  return m;
}

inline NxMat34 fromMatrix(const matrix& v)
{
  NxMat34 m;
  m.setRowMajor44(&v.m11);
  return m;
}

#endif // PHYSICSENGINE_P_HPP
