TEMPLATE = lib
TARGET = Physics

DEPLOYFILES = ../lib/dll/NxCharacter.dll ../lib/dll/NxCooking.dll

include(../ayuine2c.pri)

HEADERS += \
    RigidBody.hpp \
    Collider.hpp \
    PhysicsEngine.hpp \
    MemoryStream.hpp \
    Physics.hpp \
    BoxCollider.hpp \
    SphereCollider.hpp \
    ConvexCollider.hpp \
    TriCollider.hpp \
    PhysicsScene.hpp \
    StaticRigidBody.hpp \
    MovableRigidBody.hpp \
    CollideInfo.hpp \
    PhysicsController.hpp \
    PhysicsEngine_p.hpp \
    HeightFieldCollider.hpp

SOURCES += \
    RigidBody.cpp \
    Collider.cpp \
    PhysicsEngine.cpp \
    Physics.cpp \
    BoxCollider.cpp \
    SphereCollider.cpp \
    ConvexCollider.cpp \
    TriCollider.cpp \
    PhysicsScene.cpp \
    StaticRigidBody.cpp \
    MovableRigidBody.cpp \
    CollideInfo.cpp \
    PhysicsController.cpp \
    HeightFieldCollider.cpp

LIBS += -lNxCharacter -lNxCooking -lPhysXLoader -lMath -lCore
