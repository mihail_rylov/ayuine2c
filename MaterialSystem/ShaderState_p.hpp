#ifndef SHADERSTATE_P_HPP
#define SHADERSTATE_P_HPP

#include "MaterialSystem.hpp"

#include <QSharedData>
#include <QSharedPointer>
#include <QVariant>

class Shader;

class MATERIALSYSTEM_EXPORT ShaderStateData : public QSharedData
{
public:
  ShaderStateData();
  ~ShaderStateData();

public:
  QSharedPointer<Shader> materialShader;
  QHash<QByteArray, QVariant> values;
};

#endif // SHADERSTATE_P_HPP
