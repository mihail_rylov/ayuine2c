#include "MaterialShaderCompiler.hpp"
#include "ShaderBlockOutput.hpp"
#include <Render/RenderShader.hpp>

MaterialShaderCompiler::MaterialShaderCompiler()
{
  m_usedLinks = 0;
  lit = false;
  quality = MaterialSystem::High;
}

MaterialShaderCompiler::~MaterialShaderCompiler()
{
}

bool MaterialShaderCompiler::hasVariable(const QString &variable) const
{
  return m_pixelVariables.contains(variable);
}

bool MaterialShaderCompiler::addVariable(const QString &variable)
{
  if(hasVariable(variable))
    return false;
  m_pixelVariables.append(variable);
  return true;
}

static QString stringFormat(const char* prefix, const char* fmt, QString a1, QString a2, QString a3, QString a4, QString a5, QString a6, QString a7, QString a8, QString a9)
{
  QString str(QString("\t") + prefix + fmt);
  str = str.replace("%1", a1);
  str = str.replace("%2", a2);
  str = str.replace("%3", a3);
  str = str.replace("%4", a4);
  str = str.replace("%5", a5);
  str = str.replace("%6", a6);
  str = str.replace("%7", a7);
  str = str.replace("%8", a8);
  str = str.replace("%9", a9);
  return str;
}

void MaterialShaderCompiler::psin(const char *fmt, QString a1, QString a2, QString a3, QString a4, QString a5, QString a6, QString a7, QString a8, QString a9)
{
  m_pixelShaderIn.append(stringFormat("in ", fmt, a1, a2, a3, a4, a5, a6, a7, a8, a9));
}

void MaterialShaderCompiler::psout(const char *fmt, QString a1, QString a2, QString a3, QString a4, QString a5, QString a6, QString a7, QString a8, QString a9)
{
  m_pixelShaderOut.append(stringFormat("out ", fmt, a1, a2, a3, a4, a5, a6, a7, a8, a9));
}

void MaterialShaderCompiler::psuniform(const char *fmt, QString a1, QString a2, QString a3, QString a4, QString a5, QString a6, QString a7, QString a8, QString a9)
{
  m_pixelShaderUniform.append(stringFormat("uniform ", fmt, a1, a2, a3, a4, a5, a6, a7, a8, a9));
}

void MaterialShaderCompiler::psconst(const char *fmt, QString a1, QString a2, QString a3, QString a4, QString a5, QString a6, QString a7, QString a8, QString a9)
{
  m_pixelShaderConst.append(stringFormat("uniform ", fmt, a1, a2, a3, a4, a5, a6, a7, a8, a9));
}

void MaterialShaderCompiler::ps(const char *fmt, QString a1, QString a2, QString a3, QString a4, QString a5, QString a6, QString a7, QString a8, QString a9)
{
  m_pixelShaderBody.append(stringFormat("", fmt, a1, a2, a3, a4, a5, a6, a7, a8, a9));
}

void MaterialShaderCompiler::vsin(const char *fmt, QString a1, QString a2, QString a3, QString a4, QString a5, QString a6, QString a7, QString a8, QString a9)
{
  m_vertexShaderIn.append(stringFormat("in ", fmt, a1, a2, a3, a4, a5, a6, a7, a8, a9));
}

void MaterialShaderCompiler::vsout(const char *fmt, QString a1, QString a2, QString a3, QString a4, QString a5, QString a6, QString a7, QString a8, QString a9)
{
  m_vertexShaderOut.append(stringFormat("out ", fmt, a1, a2, a3, a4, a5, a6, a7, a8, a9));
}

void MaterialShaderCompiler::vsuniform(const char *fmt, QString a1, QString a2, QString a3, QString a4, QString a5, QString a6, QString a7, QString a8, QString a9)
{
  m_vertexShaderUniform.append(stringFormat("uniform ", fmt, a1, a2, a3, a4, a5, a6, a7, a8, a9));
}

void MaterialShaderCompiler::vsconst(const char *fmt, QString a1, QString a2, QString a3, QString a4, QString a5, QString a6, QString a7, QString a8, QString a9)
{
  m_vertexShaderConst.append(stringFormat("uniform ", fmt, a1, a2, a3, a4, a5, a6, a7, a8, a9));
}

void MaterialShaderCompiler::vs(const char *fmt, QString a1, QString a2, QString a3, QString a4, QString a5, QString a6, QString a7, QString a8, QString a9)
{
  m_vertexShaderBody.append(stringFormat("", fmt, a1, a2, a3, a4, a5, a6, a7, a8, a9));
}

QString MaterialShaderCompiler::code() const
{
  QString outScreen = const_cast<MaterialShaderCompiler*>(this)->outScreen();

  QStringList code;

  code.append("#ifdef VERTEXSHADER");
  code.append("float4 VertexShader(");
  if(m_vertexShaderIn.length())
  {
      code.append("\t// input variables");
      code.append(m_vertexShaderIn.join(",\r\n") +
                  (m_vertexShaderOut.length() + m_vertexShaderUniform.length() + m_vertexShaderConst.length() ? "," : ""));
  }
  if(m_vertexShaderOut.length())
  {
      code.append("\t// output variables");
      code.append(m_vertexShaderOut.join(",\r\n") +
                  (m_vertexShaderUniform.length() + m_vertexShaderConst.length() ? "," : ""));
  }
  if(m_vertexShaderUniform.length())
  {
      code.append("\t// global variables");
      code.append(m_vertexShaderUniform.join(",\r\n") +
                  (m_vertexShaderConst.length() ? "," : ""));
  }
  if(m_vertexShaderConst.length())
  {
      code.append("\t// user-defined variables");
      code.append(m_vertexShaderConst.join(",\r\n"));
  }
  code.append("\t) : POSITION0");
  code.append("{");
  code.append(m_vertexShaderBody);
  code.append("\treturn " + outScreen + ";");
  code.append("}");
  code.append("#endif");
  code.append("");

  code.append("#ifdef PIXELSHADER");
  code.append("float4 PixelShader(");
  if(m_pixelShaderIn.length())
  {
      code.append("\t// input variables");
      code.append(m_pixelShaderIn.join(",\r\n") +
                  (m_pixelShaderOut.length() + m_pixelShaderUniform.length() + m_pixelShaderConst.length() ? "," : ""));
  }
  if(m_pixelShaderOut.length())
  {
      code.append("\t// output variables");
      code.append(m_pixelShaderOut.join(",\r\n") +
                  (m_pixelShaderUniform.length() + m_pixelShaderConst.length() ? "," : ""));
  }
  if(m_pixelShaderUniform.length())
  {
      code.append("\t// global variables");
      code.append(m_pixelShaderUniform.join(",\r\n") +
                  (m_pixelShaderConst.length() ? "," : ""));
  }
  if(m_pixelShaderConst.length())
  {
      code.append("\t// user-defined variables");
      code.append(m_pixelShaderConst.join(",\r\n"));
  }
  code.append("\t) : COLOR0");
  code.append("{");
  code.append("\tfloat4 outColor = 0;");
  code.append(m_pixelShaderBody);
  code.append("\treturn outColor;");
  code.append("}");
  code.append("#endif");
  code.append("");

  return code.join("\r\n");
}

bool MaterialShaderCompiler::linkAsTexCoord(const QString &name, ShaderBlock::LinkType type)
{
  if(!ShaderBlock::linkTypeName[type])
    return false;

  QByteArray typeName = ShaderBlock::linkTypeName[type];
  if(!typeName.startsWith("float"))
    return false;

  vsout("%1 link_%2 : TEXCOORD%3", typeName, name, QString::number(m_usedLinks));
  psin("%1 %2 : TEXCOORD%3", typeName, name, QString::number(m_usedLinks));
  vs("link_%1 = %1;", name);
  m_usedLinks++;
  return true;
}

QSharedPointer<RenderShader> MaterialShaderCompiler::shader() const
{
  return QSharedPointer<RenderShader>(new RenderShader(RenderShader::Code, code().toLatin1()));
}

MaterialShaderCompiler::ShaderLink MaterialShaderCompiler::getLink(const QString &name)
{
  if(materialLink)
  {
    if(ShaderBlock::InputLink link = materialLink->input(name.toAscii()))
    {
      return link.varName(*this);
    }
  }

  return ShaderLink();
}

QString MaterialShaderCompiler::getp(MaterialShaderCompiler::PixelShaderValue value)
{
  return "float4(0,0,0,0)";
}
