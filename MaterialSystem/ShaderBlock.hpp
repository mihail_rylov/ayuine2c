#ifndef SHADERBLOCK_HPP
#define SHADERBLOCK_HPP

#include <QObject>
#include <QPoint>
#include <QWeakPointer>
#include <Core/Serializer.hpp>
#include <Core/ObjectRef.hpp>
#include "MaterialSystem.hpp"

class Shader;

class MaterialShaderCompiler;

class MATERIALSYSTEM_EXPORT ShaderBlock : public QObject
{
  Q_OBJECT
  Q_PROPERTY(OutputLinkList links READ links WRITE setLinks DESIGNABLE false)
  Q_PROPERTY(QPoint position READ position WRITE setPosition DESIGNABLE false)
  Q_CLASSINFO("BlockGroup", "Other")

public:
  enum LinkType
  {
    Undefined,
    Float,
    Float2,
    Float3, Euler,
    Float4, Color,
    Matrix,
    Sampler,
    MaxTypes
  };

  struct MATERIALSYSTEM_EXPORT Link
  {
    unsigned index;
    LinkType type;
    const char* name;

    Link() : index(-1), type(Undefined), name(NULL)
    {
    }
    Link(unsigned _index, LinkType _type, const char* _name)
      : index(_index), type(_type), name(_name)
    {
    }

    operator bool () const
    {
      return name != NULL;
    }

    bool operator ! () const
    {
      return name == NULL;
    }

    bool operator == (const QByteArray& other) const
    {
      return name != NULL && name == other;
    }

    bool operator == (const Link& other) const
    {
      return name == other.name && type == other.type && index == other.index;
    }

    bool operator != (const Link& other) const
    {
      return !(*this == other);
    }
  };

  struct MATERIALSYSTEM_EXPORT OutputLink : public Link
  {
    QSharedPointer<ShaderBlock> owner;

    OutputLink()
    {
    }

    OutputLink& operator = (const Link& link)
    {
      (Link&)*this = link;
      owner.clear();
      return *this;
    }

    friend QDataStream& operator << (QDataStream& ds, const OutputLink& link);
    friend QDataStream& operator >> (QDataStream& ds, OutputLink& link);
  };

  struct MATERIALSYSTEM_EXPORT InputLink : public Link
  {
    QSharedPointer<ShaderBlock> owner;
    OutputLink connection;

    InputLink()
    {
    }

    operator bool () const
    {
      return connection;
    }

    bool operator ! () const
    {
      return !connection;
    }

    InputLink& operator = (const Link& link)
    {
      (Link&)*this = link;
      owner.clear();
      connection = OutputLink();
      return *this;
    }

    QString varName(MaterialShaderCompiler& compiler) const;
  };

public:
  static const char* linkTypeTransform[MaxTypes][MaxTypes];
  static const char* linkTypeName[MaxTypes];
  static const char* linkTypeDefaults[MaxTypes];

public:
  explicit ShaderBlock(QObject *parent = 0);
  ~ShaderBlock();

signals:

public slots:
  QVector<OutputLink> links() const;
  void setLinks(const QVector<OutputLink> &links);

  QWeakPointer<Shader> baseMaterialShader() const;
  Shader* rawBaseMaterialShader() const;

  QPoint position() const;
  void setPosition(const QPoint& point);

  unsigned index() const;
  QString unique() const;

signals:
  void moved(const QPoint &newPos = QPoint(), const QPoint &oldPos = QPoint());
  void linkConnected(OutputLink other = OutputLink(), Link selected = Link());
  void linkDisconnected(Link selected = Link());
  void linksUpdated();
  void updated(QObject* object = NULL);

  // Input methods
public:
  virtual unsigned inputCount() const;
  virtual Link inputLink(unsigned index) const;
  InputLink input(unsigned index) const;
  InputLink input(const QByteArray& name) const;
  bool inputHasLink(unsigned index) const;
  bool canConnect(OutputLink other, Link selected);
  bool connect(OutputLink other, Link selected);
  unsigned unlink(const QSharedPointer<ShaderBlock>& block);

  // Output methods
public:
  virtual unsigned outputCount() const;
  virtual Link outputLink(unsigned index) const;
  OutputLink output(unsigned index) const;
  OutputLink output(const QByteArray& name) const;
  virtual bool outputIsValid(unsigned index) const;

  // Const methods
public:
  virtual QPixmap constValuePixmap() const;

public:
  virtual const QMetaObject* blockType() const;
  virtual unsigned blockMaxCount() const;

  // Compiler methods
protected:
  virtual void changed();
  virtual QString buildBlockName() const;
  virtual QString inputVarName(MaterialShaderCompiler& compiler, unsigned index, const QString& default_ = QString()) const;
  virtual QString buildTempVarName(MaterialShaderCompiler& compiler, const QString& name) const;
  virtual QString buildOutputVarName(MaterialShaderCompiler& compiler, unsigned output) const;
  virtual QString outputVarName(MaterialShaderCompiler& compiler, unsigned output) const;
  virtual void compileVarName(MaterialShaderCompiler& compiler, unsigned output, const QString& varName) const;
  void updateLinks();

private:
  QWeakPointer<Shader> m_baseMaterialShader;
  QVector<OutputLink> m_links;
  QPoint m_position;

  friend class Shader;
};

typedef QVector<ShaderBlock::OutputLink> OutputLinkList;

Q_DECLARE_METATYPE(OutputLinkList)
Q_DECLARE_OBJECTREF(ShaderBlock)

#endif // SHADERBLOCK_HPP
