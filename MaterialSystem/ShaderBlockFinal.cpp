#include "ShaderBlockFinal.hpp"

const QMetaObject * ShaderBlockFinal::blockType() const
{
  return &staticMetaObject;
}

unsigned ShaderBlockFinal::blockMaxCount() const
{
  return 1;
}
