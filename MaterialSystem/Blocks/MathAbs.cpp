#include "ShaderBlock.hpp"
#include "MaterialShaderCompiler.hpp"

class MATERIALSYSTEM_EXPORT MaterialShaderBlockMathAbs : public ShaderBlock
{
  Q_OBJECT
  Q_CLASSINFO("BlockGroup", "Math")
  Q_CLASSINFO("BlockName", "Abs")

  enum Inputs
  {
    iIn,
    iMax
  };

  enum Outputs
  {
    oOut,
    oMax
  };

public:
  Q_INVOKABLE MaterialShaderBlockMathAbs()
  {
  }

public:
  unsigned inputCount() const
  {
    return iMax;
  }
  Link inputLink(unsigned index) const
  {
    switch(index)
    {
    case iIn:           return Link(index, Float4, "In");
    default:            return Link();
    }
  }
  unsigned outputCount() const
  {
    return oMax;
  }
  Link outputLink(unsigned index) const
  {
    switch(index)
    {
    case oOut:           return Link(index, Float4, "Out");
    default:             return Link();
    }
  }
  virtual void compileVarName(MaterialShaderCompiler& compiler, unsigned index, const QString& varName) const
  {
    switch(index)
    {
    case oOut:
      compiler.ps("float4 %1 = abs(%2);", varName, inputVarName(compiler, 0));
      break;
    }
  }
};

#include "MathAbs.moc"
