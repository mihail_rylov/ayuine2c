#include "ShaderBlockFinal.hpp"
#include "MaterialShaderCompiler.hpp"

class MATERIALSYSTEM_EXPORT MaterialShaderBlockFinalValue : public ShaderBlockFinal
{
  Q_OBJECT
  Q_CLASSINFO("BlockName", "Value")

  enum Inputs
  {
    iValue,
    iMax
  };

public:
  Q_INVOKABLE MaterialShaderBlockFinalValue()
  {
  }

public:
  unsigned inputCount() const
  {
    return iMax;
  }
  Link inputLink(unsigned index) const
  {
    switch(index)
    {
    case iValue:    return Link(index, Float, "Value");
    default:        return Link();
    }
  }
};

#include "FinalValue.moc"

