#include "ShaderBlock.hpp"
#include "MaterialShaderCompiler.hpp"

class MATERIALSYSTEM_EXPORT MaterialShaderBlockMathFromSpheric : public ShaderBlock
{
  Q_OBJECT
  Q_CLASSINFO("BlockGroup", "Math")
  Q_CLASSINFO("BlockName", "FromSpheric")

  enum Inputs
  {
    iPhi,
    iTheta,
    iRadius,
    iMax
  };

  enum Outputs
  {
    oOut,
    oMax
  };

public:
  Q_INVOKABLE MaterialShaderBlockMathFromSpheric()
  {
  }

public:
  unsigned inputCount() const
  {
    return iMax;
  }
  Link inputLink(unsigned index) const
  {
    switch(index)
    {
    case iRadius:        return Link(index, Float, "Radius");
    case iPhi:           return Link(index, Float, "Phi");
    case iTheta:         return Link(index, Float, "Theta");
    default:             return Link();
    }
  }
  unsigned outputCount() const
  {
    return oMax;
  }
  Link outputLink(unsigned index) const
  {
    switch(index)
    {
    case oOut:           return Link(index, Float3, "Out");
    default:             return Link();
    }
  }
  virtual void compileVarName(MaterialShaderCompiler& compiler, unsigned index, const QString& varName) const
  {
    switch(index)
    {
    case oOut:
      compiler.ps("float3 %1 = %2 * float3(cos(%4) * cos(%3), cos(%4) * sin(%3), sin(%4));",
                  varName,
                  inputVarName(compiler, iRadius, "1.0f"),
                  inputVarName(compiler, iPhi, "0.0f"),
                  inputVarName(compiler, iTheta, "0.0f"));
      break;
    }
  }
};

#include "MathFromSpheric.moc"

