#include "ShaderBlock.hpp"
#include "MaterialShaderCompiler.hpp"

class MATERIALSYSTEM_EXPORT MaterialShaderBlockMathComponentMask : public ShaderBlock
{
  Q_OBJECT
  Q_PROPERTY(bool R READ R WRITE setR)
  Q_PROPERTY(bool G READ G WRITE setG)
  Q_PROPERTY(bool B READ B WRITE setB)
  Q_PROPERTY(bool A READ A WRITE setA)
  Q_CLASSINFO("BlockGroup", "Math")
  Q_CLASSINFO("BlockName", "ComponentMask")

  enum Inputs
  {
    iIn,
    iMax
  };

  enum Outputs
  {
    oOut,
    oMax
  };

public:
  Q_INVOKABLE MaterialShaderBlockMathComponentMask()
  {
    m_R = true;
    m_G = true;
    m_B = true;
    m_A = true;
  }

public:
  bool R() const
  {
    return m_R;
  }
  void setR(bool R)
  {
    m_R = R;
    changed();
  }
  bool G() const
  {
    return m_G;
  }
  void setG(bool G)
  {
    m_G = G;
    changed();
  }
  bool B() const
  {
    return m_B;
  }
  void setB(bool B)
  {
    m_B = B;
    changed();
  }
  bool A() const
  {
    return m_A;
  }
  void setA(bool A)
  {
    m_A = A;
    changed();
  }

public:
  unsigned inputCount() const
  {
    return iMax;
  }
  Link inputLink(unsigned index) const
  {
    switch(index)
    {
    case iIn:           return Link(index, Float4, "In");
    default:            return Link();
    }
  }
  unsigned outputCount() const
  {
    return oMax;
  }
  Link outputLink(unsigned index) const
  {
    switch(index)
    {
    case oOut:           return Link(index, Float4, "Out");
    default:             return Link();
    }
  }
  virtual void compileVarName(MaterialShaderCompiler& compiler, unsigned index, const QString& varName) const
  {
    switch(index)
    {
    case oOut:
      compiler.ps("float4 %1 = %2;", varName, inputVarName(compiler, 0));
      if(!m_R)
          compiler.ps("%1.r = 0.0f;", varName);
      if(!m_G)
          compiler.ps("%1.g = 0.0f;", varName);
      if(!m_B)
          compiler.ps("%1.b = 0.0f;", varName);
      if(!m_A)
          compiler.ps("%1.a = 0.0f;", varName);
      break;
    }
  }

private:
  float m_R, m_G, m_B, m_A;
};

#include "MathComponentMask.moc"
