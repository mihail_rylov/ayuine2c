#include "ShaderBlock.hpp"
#include "MaterialShaderCompiler.hpp"

class MATERIALSYSTEM_EXPORT MaterialShaderBlockEffectLighting : public ShaderBlock
{
  Q_OBJECT
  Q_CLASSINFO("BlockGroup", "Effect")
  Q_CLASSINFO("BlockName", "Lighting")

  enum Inputs
  {
    iLightDir,
    iLightDist,
    iSpecularity,
    iNormal,
    iMax
  };

  enum Outputs
  {
    oDiffuse,
    oSpecular,
    oMax
  };

public:
  Q_INVOKABLE MaterialShaderBlockEffectLighting()
  {
  }

public:
  unsigned inputCount() const
  {
    return iMax;
  }
  Link inputLink(unsigned index) const
  {
    switch(index)
    {
    case iLightDir:		return Link(index, Float3, "LightDir");
    case iLightDist:	return Link(index, Float, "LightDist");
    case iNormal:		return Link(index, Float3, "Normal");
    case iSpecularity:	return Link(index, Float, "Specularity");
    default:            return Link();
    }
  }
  unsigned outputCount() const
  {
    return oMax;
  }
  Link outputLink(unsigned index) const
  {
    switch(index)
    {
    case oDiffuse:		return Link(index, Float, "Diffuse");
    case oSpecular:		return Link(index, Float, "Specular");
    default:            return Link();
    }
  }
  QString compileLightAttn(MaterialShaderCompiler& compiler) const
  {
      QString varName = buildTempVarName(compiler, "lightAttn");
      if(compiler.addVariable(varName))
      {
          compiler.ps("float %1 = 1.0f - saturate(%2);", varName, inputVarName(compiler, iLightDist));
      }
      return varName;
  }
  virtual void compileVarName(MaterialShaderCompiler& compiler, unsigned index, const QString& varName) const
  {
    switch(index)
    {
    case oDiffuse:
        compiler.ps("float %1 = saturate(%2 * dot(%3.xyz, %4.xyz));",
            outputVarName(compiler, oDiffuse),
            compileLightAttn(compiler),
            inputVarName(compiler, iLightDir),
            inputHasLink(iNormal) ? inputVarName(compiler, iNormal) : compiler.getp(MaterialShaderCompiler::pNormal));
        break;

    case oSpecular:
        compiler.ps("float %1 = %2 * pow(saturate(dot(reflect(-%3, %4), %5.xyz)), %6);",
            outputVarName(compiler, oSpecular),
            compileLightAttn(compiler),
            inputVarName(compiler, iLightDir),
            inputHasLink(iNormal) ? inputVarName(compiler, iNormal) : compiler.getp(MaterialShaderCompiler::pNormal),
            compiler.getp(MaterialShaderCompiler::pCameraDir),
            inputVarName(compiler, iSpecularity, "16.0"));
        break;
    }
  }
};

#include "EffectLighting.moc"
