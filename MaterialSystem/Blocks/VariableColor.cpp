#include "ShaderBlockVariable.hpp"
#include "MaterialShaderCompiler.hpp"
#include "Shader.hpp"
#include <Render/RenderSystem.hpp>

#include <QColor>
#include <QPixmap>
#include <Math/Vec4.hpp>

class MATERIALSYSTEM_EXPORT MaterialShaderBlockVariableColor : public MaterialShaderBlockOutputVariable
{
  Q_OBJECT
  Q_PROPERTY(QColor color READ color WRITE setColor)
  Q_CLASSINFO("BlockName", "Color")

  enum Outputs
  {
    oColor,
    oAlpha,
    oMax
  };

public:
  Q_INVOKABLE MaterialShaderBlockVariableColor()
  {
  }

public:
  LinkType constType() const
  {
    return Color;
  }
  QColor color() const
  {
    return m_color;
  }
  void setColor(const QColor& color)
  {
    if(m_color != color)
    {
      m_color = color;
      m_pixmap = QPixmap();
      changed();
      emit constValueChanged(QVariant::fromValue(color));
    }
  }
  QVariant constValue() const
  {
    return m_color;
  }
  void setConstValue(const QVariant& value)
  {
    setColor(value.value<QColor>());
  }
  virtual QPixmap constValuePixmap() const
  {
    if(m_pixmap.isNull())
    {
      m_pixmap = QPixmap(32, 32);
      m_pixmap.fill(m_color);
    }

    return m_pixmap;
  }
  void setRenderVariable(unsigned offset, const QVariant &value, bool onlyUsed)
  {
    if(!rawBaseMaterialShader())
      return;

    unsigned constOffset = offset + rawBaseMaterialShader()->constOffset();

    if(!onlyUsed || getRenderSystem().isConstUsed(constOffset))
    {
      float constValue[4];
      QColor newValue = value.value<QColor>();
      constValue[0] = newValue.redF();
      constValue[1] = newValue.greenF();
      constValue[2] = newValue.blueF();
      constValue[3] = newValue.alphaF();

      getRenderSystem().setConst(constOffset, constValue, 1);
    }
  }

public:
  unsigned outputCount() const
  {
    return oMax;
  }
  Link outputLink(unsigned index) const
  {
    switch(index)
    {
    case oColor:		return Link(index, Float4, "Out");
    case oAlpha:		return Link(index, Float, "Alpha");
    default:            return Link();
    }
  }
  QString outputVarName(MaterialShaderCompiler& compiler, unsigned output) const
  {
    switch(output)
    {
    case oColor:			return constVarName(compiler);
    case oAlpha:			return constVarName(compiler) + ".a";
    default:                return MaterialShaderBlockOutputVariable::outputVarName(compiler, output);
    }
  }

private:
  QColor m_color;
  mutable QPixmap m_pixmap;
};

#include "VariableColor.moc"
