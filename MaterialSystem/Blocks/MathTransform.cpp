#include "ShaderBlock.hpp"
#include "MaterialShaderCompiler.hpp"

class MATERIALSYSTEM_EXPORT MaterialShaderBlockMathTransform : public ShaderBlock
{
  Q_OBJECT
  Q_CLASSINFO("BlockGroup", "Math")
  Q_CLASSINFO("BlockName", "Transform")

  enum Inputs
  {
    iIn,
    iMatrix,
    iMax
  };

  enum Outputs
  {
    oNormal,
    oProjected,
    oTransformed,
    oMax
  };

public:
  Q_INVOKABLE MaterialShaderBlockMathTransform()
  {
  }

public:
  unsigned inputCount() const
  {
    return iMax;
  }
  Link inputLink(unsigned index) const
  {
    switch(index)
    {
    case iIn:           return Link(index, Float3, "In");
    case iMatrix:       return Link(index, Matrix, "Matrix");
    default:            return Link();
    }
  }
  unsigned outputCount() const
  {
    return oMax;
  }
  Link outputLink(unsigned index) const
  {
    switch(index)
    {
    case oNormal:        return Link(index, Float3, "Normal");
    case oProjected:     return Link(index, Float3, "Projected");
    case oTransformed:   return Link(index, Float4, "Transformed");
    default:             return Link();
    }
  }
  virtual void compileVarName(MaterialShaderCompiler& compiler, unsigned index, const QString& varName) const
  {
    switch(index)
    {
    case oNormal:
      compiler.ps("float3 %1 = mul(%2, %3);", varName, inputVarName(compiler, iIn), inputVarName(compiler, iMatrix));
      break;

    case oProjected:
      compiler.ps("float3 %1 = (%2.z > 0.0f) ? %2.xyz / %2.w : 0.0f;", varName, outputVarName(compiler, oTransformed));
      break;

    case oTransformed:
      compiler.ps("float4 %1 = mul(float4(%2, 1.0f), %3);", varName, inputVarName(compiler, iIn), inputVarName(compiler, iMatrix));
      break;

    default:
      ShaderBlock::compileVarName(compiler, index, varName);
      break;
    }
  }
};

#include "MathTransform.moc"
