#include "ShaderBlock.hpp"
#include "MaterialShaderCompiler.hpp"

class MATERIALSYSTEM_EXPORT MaterialShaderBlockEffectQuality : public ShaderBlock
{
  Q_OBJECT
  Q_CLASSINFO("BlockGroup", "Effect")
  Q_CLASSINFO("BlockName", "Quality")

  enum Inputs
  {
      iLow,
      iMedium,
      iHigh,
      iMax
  };

  enum Outputs
  {
      oOut,
      oMax
  };

public:
  Q_INVOKABLE MaterialShaderBlockEffectQuality()
  {
  }

public:
  unsigned inputCount() const
  {
    return iMax;
  }
  Link inputLink(unsigned index) const
  {
    switch(index)
    {
    case iLow:		return Link(index, Float4, "Low");
    case iMedium:   return Link(index, Float4, "Medium");
    case iHigh:		return Link(index, Float4, "High");
    default:        return Link();
    }
  }
  unsigned outputCount() const
  {
    return oMax;
  }
  Link outputLink(unsigned index) const
  {
    switch(index)
    {
    case oOut:		return Link(index, Float4, "Out");
    default:        return Link();
    }
  }
  virtual QString outputVarName(MaterialShaderCompiler& compiler, unsigned) const
  {
    switch(compiler.quality)
    {
    case MaterialSystem::Low:
      return inputVarName(compiler, iLow, "float(0)");

    case MaterialSystem::Medium:
      return inputVarName(compiler, iMedium, "float(0)");

    default:
    case MaterialSystem::High:
      return inputVarName(compiler, iHigh, "float(0)");
    }
  }
};

#include "EffectQuality.moc"
