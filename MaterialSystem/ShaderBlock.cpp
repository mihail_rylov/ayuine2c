#include "ShaderBlock.hpp"
#include "MaterialShaderCompiler.hpp"
#include "Shader.hpp"
#include <Core/Serializer.hpp>
#include <Core/ObjectRef.hpp>
#include <QPixmap>

Q_IMPLEMENT_OBJECTREF(ShaderBlock)
Q_IMPLEMENT_ALIASREF(MaterialShaderBlockRefList, ShaderBlockRefList)

static int registerStreamOperators()
{
  qRegisterMetaTypeStreamOperators<OutputLinkList>();
  qRegisterMetaTypeStreamOperators<ShaderValues>();
  QMetaType::registerTypedef("MaterialShaderValues", qMetaTypeId<ShaderValues>());
  return 0;
}

Q_CONSTRUCTOR_FUNCTION(registerStreamOperators)

QDataStream& operator << (QDataStream& ds, const ShaderBlock::OutputLink& link)
{
  if(link.name && link.owner.data())
    ds << link.owner << link.index;
  else
    ds << ShaderBlockRef() << link.index;
  return ds;
}

QDataStream& operator >> (QDataStream& ds, ShaderBlock::OutputLink& link)
{
  ds >> link.owner >> link.index;
  return ds;
}

ShaderBlock::ShaderBlock(QObject *parent) :
  QObject(parent)
{
}

ShaderBlock::~ShaderBlock()
{
}

unsigned ShaderBlock::inputCount() const
{
  return 0;
}

ShaderBlock::Link ShaderBlock::inputLink(unsigned index) const
{
  return Link();
}

ShaderBlock::InputLink ShaderBlock::input(unsigned index) const
{
  InputLink link;
  link = inputLink(index);
  link.owner = QWeakPointer<ShaderBlock>(const_cast<ShaderBlock*>(this)).toStrongRef();
  if(index < (unsigned)m_links.size())
    link.connection = m_links[index];
  return link;
}

ShaderBlock::InputLink ShaderBlock::input(const QByteArray& name) const
{
  for(unsigned i = inputCount(); i-- > 0; )
  {
    if(inputLink(i) == name)
      return input(i);
  }
  return InputLink();
}

bool ShaderBlock::inputHasLink(unsigned index) const
{
  if(index < (unsigned)m_links.size() && m_links[index].owner)
    return m_links[index].owner->outputIsValid(m_links[index].index);
  return false;
}

bool ShaderBlock::canConnect(OutputLink other, Link selected)
{
  if(input(selected.index) != selected)
    return false;
  if(selected.index >= inputCount())
    return false;

  if(other.owner.data() == NULL)
  {
    return true;
  }

  if(linkTypeTransform[other.type][selected.type])
  {
    QVector<QSharedPointer<ShaderBlock> > blocks;
    blocks.append(other.owner);

    for(unsigned i = 0; i < (unsigned)blocks.size(); ++i)
    {
      for(unsigned j = blocks[i]->inputCount(); j-- > 0; )
      {
        InputLink input = blocks[i]->input(j);
        if(input.owner.data() == NULL)
          continue;
        if(blocks.contains(input.owner))
          continue;

        if(input.owner == this) // circular references
          return false;

        blocks.append(input.owner);
      }
    }
    return true;
  }
  return false;
}

bool ShaderBlock::connect(OutputLink other, Link selected)
{
  if(!canConnect(other, selected))
    return false;

  m_links.resize(inputCount());
  m_links[selected.index] = other;
  if(other)
    emit linkConnected(other, selected);
  else
    emit linkDisconnected(selected);
  emit linksUpdated();
  changed();
  // emit updated();
  return true;
}

unsigned ShaderBlock::outputCount() const
{
  return 0;
}

ShaderBlock::Link ShaderBlock::outputLink(unsigned index) const
{
  return Link();
}

ShaderBlock::OutputLink ShaderBlock::output(unsigned index) const
{
  OutputLink link;
  link = outputLink(index);
  link.owner = QWeakPointer<ShaderBlock>(const_cast<ShaderBlock*>(this)).toStrongRef();
  return link;
}

ShaderBlock::OutputLink ShaderBlock::output(const QByteArray& name) const
{
  for(unsigned i = inputCount(); i-- > 0; )
  {
    if(output(i) == name)
      return output(i);
  }
  return OutputLink();
}

void ShaderBlock::setLinks(const QVector<OutputLink> &links)
{
  m_links = links;
  m_links.resize(inputCount());

  for(unsigned i = 0; i < (unsigned)m_links.size(); ++i)
  {
    OutputLink& link = m_links[i];
    if(link.owner.data())
      link = link.owner->output(link.index);
  }

  emit linksUpdated();
  changed();
  // emit updated();
}

unsigned ShaderBlock::unlink(const QSharedPointer<ShaderBlock> &block)
{
  unsigned count = 0;

  m_links.resize(inputCount());

  for(unsigned i = 0; i < (unsigned)m_links.size(); ++i)
  {
    OutputLink& link = m_links[i];

    if(link.owner == block)
    {
      Link linkTemp = link;
      link = OutputLink();
      emit linkDisconnected(linkTemp);
      ++count;
    }
  }

  if(count)
  {
    emit linksUpdated();
    changed();
    // emit updated();
  }

  return count;
}

QString ShaderBlock::inputVarName(MaterialShaderCompiler &compiler, unsigned index, const QString &defaultValue) const
{
  if(inputHasLink(index))
  {
    InputLink link = input(index);

    if(link.connection)
    {
      if(link.type == link.connection.type)
        return link.connection.owner->outputVarName(compiler, link.connection.index);

      if(const char* transform = linkTypeTransform[link.connection.type][link.type])
      {
        return QString(transform).arg(link.connection.owner->outputVarName(compiler, link.connection.index));
      }
    }
  }

  return defaultValue.size() ? defaultValue : linkTypeDefaults[input(index).type];
}

QString ShaderBlock::buildTempVarName(MaterialShaderCompiler &compiler, const QString &name) const
{
  return QString("tmp_%1_%2").arg(buildBlockName(), name);
}

QString ShaderBlock::buildOutputVarName(MaterialShaderCompiler &compiler, unsigned output) const
{
  Link link = outputLink(output);
  if(link.name)
    return QString("%1_%2").arg(buildBlockName(), link.name);
  return QString("%1_%2").arg(buildBlockName(), QString::number(output));
}

QString ShaderBlock::outputVarName(MaterialShaderCompiler &compiler, unsigned output) const
{
  QString outputName = buildOutputVarName(compiler, output);
  if(compiler.addVariable(outputName))
    compileVarName(compiler, output, outputName);
  return outputName;
}

void ShaderBlock::compileVarName(MaterialShaderCompiler &compiler, unsigned output, const QString &varName) const
{
  Link link = outputLink(output);
  if(!link)
    return;

  QLatin1String defaults(linkTypeDefaults[link.type]);

  if(defaults.latin1())
    compiler.ps("%1 %2 = %3;", QLatin1String(linkTypeName[link.type]), varName, defaults);
  else
    compiler.ps("%1 %2;", QLatin1String(linkTypeName[link.type]), varName);
}

QString ShaderBlock::buildBlockName() const
{
  if(objectName().length())
  {
      char buffer[32];
      int length = 0;
      foreach(QChar c, objectName())
      {
          if(isalpha(c.toAscii()))
          {
              buffer[length++] = c.toAscii();
              if(length == sizeof(buffer))
                  break;
          }
      }

      if(length)
          return unique() + "_" + QByteArray(buffer, length);
  }
  return unique();
}

QString ShaderBlock::InputLink::varName(MaterialShaderCompiler &compiler) const
{
  if(owner)
  {
    return owner->inputVarName(compiler, index);
  }
  return "float(0)";
}

void ShaderBlock::updateLinks()
{
  m_links.resize(inputCount());
  changed();
  emit linksUpdated();
}

const QMetaObject * ShaderBlock::blockType() const
{
  return &staticMetaObject;
}

unsigned ShaderBlock::blockMaxCount() const
{
  return ~0U;
}

QVector<ShaderBlock::OutputLink> ShaderBlock::links() const
{
  return m_links;
}

QWeakPointer<Shader> ShaderBlock::baseMaterialShader() const
{
  return m_baseMaterialShader;
}

QPoint ShaderBlock::position() const
{
  return m_position;
}

void ShaderBlock::setPosition(const QPoint &point)
{
  if(m_position != point)
  {
    QPoint oldPos(point);
    m_position = point;
    emit moved(point, oldPos);
    // emit updated();
  }
}

unsigned ShaderBlock::index() const
{
    if(QSharedPointer<Shader> shader = baseMaterialShader().toStrongRef())
    {
        QVector<QSharedPointer<ShaderBlock> >& blocks = shader->blocks();
        for(int i = 0; i < blocks.size(); ++i)
            if(blocks[i].data() == this)
                return i;
    }
    return ~0;
}

QString ShaderBlock::unique() const
{
    unsigned i = index();

    if(i != ~0)
    {
        if(QSharedPointer<Shader> shader = baseMaterialShader().toStrongRef())
        {
            QString u = shader->unique();
            if(u.length())
                return u + QString::number(i);
        }
    }

    return QString::number((ulong)this, 16);
}

void ShaderBlock::changed()
{
  if(baseMaterialShader())
    baseMaterialShader().toStrongRef()->changed();
}

bool ShaderBlock::outputIsValid(unsigned index) const
{
  return true;
}

QPixmap ShaderBlock::constValuePixmap() const
{
  return QPixmap();
}

Shader * ShaderBlock::rawBaseMaterialShader() const
{
  return m_baseMaterialShader.data();
}
