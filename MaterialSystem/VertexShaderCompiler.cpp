#include "VertexShaderCompiler.hpp"
#include <Render/RenderShaderConst.hpp>

VertexShaderCompiler::VertexShaderCompiler(VertexBuffer::Types vertexType_)
{
  qMemSet(m_vertexValues, 0, sizeof(m_vertexValues));
  qMemSet(m_pixelValues, 0, sizeof(m_pixelValues));
  qMemSet(m_pixelLinks, 0, sizeof(m_pixelLinks));
  vertexType = vertexType_;
}

QString VertexShaderCompiler::getv(VertexShaderCompiler::VertexShaderValue value)
{
  QString valueName = staticMetaObject.enumerator(staticMetaObject.indexOfEnumerator("VertexShaderValue")).valueToKey(value);

  if(m_vertexValues[value])
    return valueName;

  m_vertexValues[value] = true;

  switch(value)
  {
  case vFrame:
    vsuniform("float4 %1 : register(c%2)", valueName, QString::number(VertexShaderConst::Frame));
    break;

  case vTime:
    vs("float %1 = %2.w;", valueName, getv(vFrame));
    break;

  case vPixelSize:
    vs("float2 %1 = %2.xy;", valueName, getv(vFrame));
    break;

  case vProjViewMatrix:
    vsuniform("float4x4 %1 : register(c%2)", valueName, QString::number(VertexShaderConst::ProjView));
    break;

  case vObjectMatrix:
    if(vertexType & VertexBuffer::Instanced)
      vsin("float4x3 %1 : TEXCOORD2", valueName);
    else
      vsuniform("float4x3 %1 : register(c%2)", valueName, QString::number(VertexShaderConst::Object));
    break;

  case vOutScreen:
    if(vertexType & VertexBuffer::Instanced)
    {
      vs("float4 %1 = mul(float4(%2, 1.0f), %3);", valueName, getv(vOrigin), getv(vProjViewMatrix));
    }
    else
    {
      vsuniform("float4x4 projViewObject : register(c%2)", valueName, QString::number(VertexShaderConst::ProjViewObject));
      vs("float4 %1 = mul(float4(%2, 1.0f), projViewObject);", valueName, getv(vLocalOrigin));
    }
    break;

  case vScreen:
    vs("float3 %1 = %2.xyw;", valueName, getv(vOutScreen));
    vs("%1.y = -%2.y;", valueName, valueName);
    break;

  case vLocalOrigin:
    vsin("float3 %1 : POSITION0", valueName);
    break;

  case vOrigin:
  case vVertexOrigin:
    vs("float3 %1 = mul(float4(%3, 1.0f), %2).xyz;", valueName, getv(vObjectMatrix), getv(vLocalOrigin));
    break;

  case vColor:
    if(vertexType & VertexBuffer::Color)
      vsin("float4 %1 : COLOR0", valueName);
    else
      vs("float4 %1 = 0;", valueName);
    break;

  case vNormal:
    if(vertexType & VertexBuffer::Normal)
    {
      vsin("float3 %1 : NORMAL0", valueName);
      vs("%1 = mul(%1, (float3x3)%2).xyz;", valueName, getv(vObjectMatrix));
    }
    else
    {
      vs("float3 %1 = -%2;", valueName, getv(vCameraAt));
    }
    break;

  case vTangent:
    if(vertexType & VertexBuffer::Tangent)
    {
      vsin("float4 t_%1 : TANGENT0", valueName);
      vs("float3 %1 = mul(t_%1.xyz, (float3x3)%2).xyz;", valueName, getv(vObjectMatrix));
      // vs("float3 %1 = t_%1.xyz;", valueName);
    }
    else if(vertexType & VertexBuffer::Normal)
    {
      vs("float3 %1 = %2.zxy;", valueName, getv(vNormal));
      vs("%1 = normalize(%1 - %2 * dot(%2, %1));", valueName, getv(vNormal));
    }
    else
      vs("float3 %1 = %2;", valueName, getv(vCameraRight));
    break;

  case vBinormal:
    if(vertexType & VertexBuffer::Tangent)
      vs("float3 %1 = cross(%2, %3) * t_%3.w;", valueName, getv(vNormal), getv(vTangent));
    else if(vertexType & VertexBuffer::Normal)
      vs("float3 %1 = cross(%2, %3);", valueName, getv(vNormal), getv(vTangent));
    else
      vs("float3 %1 = %2", valueName, getv(vCameraUp));
    break;

  case vTexCoords:
    if(vertexType & VertexBuffer::Coords)
      vsin("float2 %1 : TEXCOORD0", valueName);
    else
      vs("float2 %1 = %2.xz;", valueName, getv(vLocalOrigin));
    break;

  case vMapCoords:
    if(vertexType & VertexBuffer::Coords2)
      vsin("float2 %1 : TEXCOORD1", valueName);
    else
      vs("float2 %1 = float2(0,0);", valueName);
    break;

  case vCameraOrigin:
    vsuniform("float4 t_%1 : register(c%2)", valueName, QString::number(VertexShaderConst::ViewOrigin));
    vs("float3 %1 = t_%1.xyz;", valueName);
    break;

  case vCameraInvDistance:
    vs("float %1 = t_%2.w;", valueName, getv(vCameraOrigin));
    break;

  case vCameraDistance:
    vs("float %1 = 1.0 / %2;", valueName, getv(vCameraInvDistance));
    break;

  case vCameraAt:
    vsuniform("float3 %1 : register(c%2)", valueName, QString::number(VertexShaderConst::ViewAt));
    break;

  case vCameraUp:
    vsuniform("float3 %1 : register(c%2)", valueName, QString::number(VertexShaderConst::ViewUp));
    break;

  case vCameraRight:
    vsuniform("float3 %1 : register(c%2)", valueName, QString::number(VertexShaderConst::ViewRight));
    break;

  case vCameraDir:
    vs("float3 %1 = (%2 - %3) * %4;", valueName, getv(vCameraOrigin), getv(vOrigin), getv(vCameraInvDistance));
    break;

  case vCameraTanDir:
    vs("float3 %1 = mul(%2, float3x3(%3, %4, %5));", valueName,
       getv(vCameraDir),
       getv(vTangent),
       getv(vBinormal),
       getv(vNormal));
    break;

  default:
    break;
  }

  return valueName;
}

QString VertexShaderCompiler::getp(VertexShaderCompiler::VertexShaderValue value)
{
  QString valueName = getv(value);

  if(m_pixelLinks[value])
    return valueName;

  m_pixelLinks[value] = true;

  ShaderBlock::LinkType type = ShaderBlock::Undefined;

  switch(value) {
  case vOutScreen:
  case vFrame:
  case vColor:
    type = ShaderBlock::Float4;
    break;

  case vScreen:
  case vVertexOrigin:
  case vOrigin:
  case vLocalOrigin:
  case vNormal:
  case vTangent:
  case vBinormal:
  case vCameraOrigin:
  case vCameraAt:
  case vCameraUp:
  case vCameraRight:
  case vCameraDir:
  case vCameraTanDir:
    type = ShaderBlock::Float3;
    break;

  case vTexCoords:
  case vMapCoords:
  case vPixelSize:
    type = ShaderBlock::Float2;
    break;

  case vCameraDistance:
  case vCameraInvDistance:
  case vTime:
    type = ShaderBlock::Float;
    break;

  default:
    type = ShaderBlock::Undefined;
    break;
  }

  linkAsTexCoord(valueName, type);
  return valueName;
}

QString VertexShaderCompiler::getp(PixelShaderValue value)
{
  QString valueName = staticMetaObject.enumerator(staticMetaObject.indexOfEnumerator("PixelShaderValue")).valueToKey(value);

  if(m_pixelValues[value])
    return valueName;
  m_pixelValues[value] = true;

  switch(value)
  {
  case pFrame:
    psuniform("float4 %1 : register(c%2)", valueName, QString::number(PixelShaderConst::Frame));
    break;

  case pTime:
    ps("float %1 = %2.w;", valueName, getp(pFrame));
    break;

  case pPixelSize:
    ps("float2 %1 = %2.xy;", valueName, getp(pFrame));
    break;

  case pObjectColor:
    psuniform("float4 %1 : register(c%2)", valueName, QString::number(PixelShaderConst::Color));
    break;

  case pColor:
    ps("float4 %1 = %2;", valueName, getp(vColor));
    break;

  case pScreen:
    ps("float2 %1 = %2.xy / %3.z * 0.5f + 0.5f;", valueName, getp(vScreen), getp(vScreen));
    break;

  case pScreenDepth:
    ps("float %1 = %2.z / %2.w;", valueName, getp(vOutScreen));
    break;

  case pDepth2:
    ps("float %1 = dot(%2, %3);", valueName, getp(vCameraDir), getp(vCameraDir));
    break;

  case pDepth:
    ps("float %1 = sqrt(%2);", valueName, getp(pDepth2));
    break;

  case pOrigin:
    ps("float3 %1 = %2;", valueName, getp(vOrigin));
    break;

  case pLocalOrigin:
    ps("float3 %1 = %2;", valueName, getp(vLocalOrigin));
    break;

  case pTexCoords:
    ps("float2 %1 = %2;", valueName, getp(vTexCoords));
    break;

  case pMapCoords:
    ps("float2 %1 = %2;", valueName, getp(vMapCoords));
    break;

  case pNormal:
    ps("float3 %1 = normalize(%2);", valueName, getp(vNormal));
    break;

  case pTangent:
    ps("float3 %1 = normalize(%2);", valueName, getp(vTangent));
    break;

  case pBinormal:
    ps("float3 %1 = normalize(%2);", valueName, getp(vBinormal));
    break;

  case pCameraDir:
    ps("float3 %1 = normalize(%2);",  valueName, getp(vCameraDir));
    break;

  case pCameraTanDir:
    ps("float3 %1 = normalize(%2);",  valueName, getp(vCameraTanDir));
    break;

  default:
    break;
  }

  return valueName;
}

QString VertexShaderCompiler::outScreen()
{
  return getv(vOutScreen);
}
