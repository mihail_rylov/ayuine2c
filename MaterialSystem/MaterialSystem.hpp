#ifndef MATERIALSYSTEM_HPP
#define MATERIALSYSTEM_HPP

#include <QtCore>

#ifdef MATERIALSYSTEM_EXPORTS
#define MATERIALSYSTEM_EXPORT Q_DECL_EXPORT
#else
#define MATERIALSYSTEM_EXPORT Q_DECL_IMPORT
#endif

struct vec3;
struct sphere;

class MATERIALSYSTEM_EXPORT MaterialSystem
{
  Q_GADGET
  Q_ENUMS(Quality)

public:
  enum Quality
  {
    Low,
    Medium,
    High
  };
  enum { QualityCount = High+1 };

  static MaterialSystem::Quality qualityForSphere(const vec3& viewOrigin, const sphere& sphere);

  static void setDefaultQuality();
  static void setFixedQuality(Quality quality);
};

#endif // MATERIALSYSTEM_HPP
