#include "Mesh.hpp"
#include "MeshSubset.hpp"
#include "MeshVertex.hpp"

#include <MaterialSystem/Material.hpp>

#include <Math/mathlib.hpp>

#include <Physics/CollideInfo.hpp>
#include <Physics/BoxCollider.hpp>
#include <Physics/SphereCollider.hpp>
#include <Physics/TriCollider.hpp>
#include <Physics/ConvexCollider.hpp>

#include <Render/VertexBuffer.hpp>
#include <Render/IndexBuffer.hpp>

Q_IMPLEMENT_METATYPE(MeshIndicesList)
Q_IMPLEMENT_OBJECTREF(Mesh)

static QSet<Mesh*>& meshList()
{
  static QSet<Mesh*> meshList;
  return meshList;
}
	
void Mesh::optimizeRenderVertexBuffers()
{
  unsigned size = 0;
  unsigned count = 0;

  foreach(Mesh* mesh, meshList())
  {
    if(mesh->vertices().isEmpty())
      continue;
    size += mesh->vertices().size();
    count++;
  }

  if(!size)
    return;

  QSharedPointer<StaticVertexBuffer<MeshVertex> > buffer(new StaticVertexBuffer<MeshVertex>(size));

  if(MeshVertex* locked = buffer->lock(size))
  {
    unsigned offset = 0;

    foreach(Mesh* mesh, meshList())
    {
      if(mesh->vertices().isEmpty())
        continue;

      qMemCopy(locked + offset, mesh->vertices().constData(), sizeof(MeshVertex) * mesh->vertices().size());

      mesh->m_renderVertexBuffer = buffer;
      mesh->m_renderVertexOffset = offset;
      offset += mesh->vertices().size();
    }

    buffer->unlock();

    qDebug() << "[MESH] Optimized " << count << " meshes with " << size << " vertices";
  }
}

void Mesh::optimizeRenderIndexBuffers()
{
  unsigned count = 0;
  unsigned size = 0;

  foreach(Mesh* mesh, meshList())
  {
    if(mesh->indices().isEmpty())
      continue;

    if(mesh->indices().size() <= USHRT_MAX)
    {
      size += mesh->indices().size();
      ++count;
    }
  }

  if(!size)
    return;

  QSharedPointer<StaticIndexBuffer<unsigned short> > buffer(new StaticIndexBuffer<unsigned short>(size));

  if(unsigned short* locked = buffer->lock(size))
  {
    unsigned offset = 0;

    foreach(Mesh* mesh, meshList())
    {
      if(mesh->vertices().isEmpty())
        continue;
      if(mesh->indices().size() > USHRT_MAX)
        continue;

      qCopy(mesh->indices().begin(), mesh->indices().end(), locked + offset);

      mesh->m_renderIndexBuffer = buffer;
      mesh->m_renderIndexOffset = offset;

      offset += mesh->indices().size();
    }

    buffer->unlock();

    qDebug() << "[MESH] Optimized " << count << " meshes with "  << size << " indices";
  }
}

void Mesh::optimizeRender()
{
  optimizeRenderVertexBuffers();
  optimizeRenderIndexBuffers();
}

Mesh::Mesh()
{
  m_renderVertexOffset = 0;
  m_renderIndexOffset = 0;
  meshList().insert(this);
}

Mesh::~Mesh()
{
  meshList().remove(this);
}

const QVector<MeshVertex> & Mesh::vertices() const
{
  return m_vertices;
}

void Mesh::setVertices(const QVector<MeshVertex> &vertices)
{
  m_vertices = vertices;
  if(m_vertices.isEmpty())
    m_renderVertexBuffer.clear();
  else
    m_renderVertexBuffer = QSharedPointer<VertexBuffer>(new StaticVertexBuffer<MeshVertex>(m_vertices));
  m_renderVertexOffset = 0;
}

const QVector<unsigned> & Mesh::indices() const
{
  return m_indices;
}

void Mesh::setIndices(const QVector<unsigned> &indices)
{
  m_indices = indices;

  if(m_indices.isEmpty())
  {
    m_renderIndexBuffer.clear();
  }
  else if(m_indices.size() <= USHRT_MAX)
  {
    QVector<unsigned short> indices16(m_indices.size());
    qCopy(m_indices.begin(), m_indices.end(), indices16.begin());
    m_renderIndexBuffer = QSharedPointer<IndexBuffer>(new StaticIndexBuffer<unsigned short>(indices16));
  }
  else
  {
    m_renderIndexBuffer = QSharedPointer<IndexBuffer>(new StaticIndexBuffer<unsigned>(m_indices));
  }
  m_renderIndexOffset = 0;
}

const QVector<QSharedPointer<MeshSubset> > & Mesh::subsets() const
{
  return m_subsets;
}

void Mesh::setSubsets(const QVector<QSharedPointer<MeshSubset> > &subsets)
{
  m_subsets = subsets;

  foreach(QSharedPointer<MeshSubset> subset, m_subsets)
  {
    subset->m_mesh = this;
  }
}

void Mesh::addSubset(const QSharedPointer<MeshSubset> &subset)
{
  if(!subset)
    return;
  m_subsets.append(subset);
  subset->m_mesh = this;
}

const box & Mesh::boxBounds() const
{
  return m_boxBounds;
}

void Mesh::setBoxBounds(const box &bounds)
{
  m_boxBounds = bounds;
}

const sphere & Mesh::sphereBounds() const
{
  return m_sphereBounds;
}

void Mesh::setSphereBounds(const sphere &bounds)
{
  m_sphereBounds = bounds;
}

Mesh::ColliderType Mesh::colliderType() const
{
  return m_colliderType;
}

void Mesh::setColliderType(Mesh::ColliderType colliderType)
{
  if(m_colliderType == colliderType)
    return;

  m_colliderType = colliderType;
  buildColliderObject();
}

float Mesh::colliderMass() const
{
  return m_colliderMass;
}

void Mesh::setColliderMass(float colliderMass)
{
  m_colliderMass = colliderMass;
}

const QVector<vec3> & Mesh::colliderVertices() const
{
  return m_colliderVertices;
}

void Mesh::setColliderVertices(const QVector<vec3> &colliderVertices)
{
  m_colliderVertices = colliderVertices;
}

const QVector<unsigned> & Mesh::colliderIndices() const
{
  return m_colliderIndices;
}

void Mesh::setColliderIndices(const QVector<unsigned> &colliderIndices)
{
  m_colliderIndices = colliderIndices;
}

const QSharedPointer<Collider> & Mesh::colliderObject() const
{
  if(m_colliderObject.isNull())
  {
    const_cast<Mesh*>(this)->buildColliderObject();
  }
  return m_colliderObject;
}

void Mesh::setColliderObject(const QSharedPointer<Collider> &colliderObject)
{
  m_colliderObject = colliderObject;
}

void Mesh::generateColliderVertices()
{
  if(m_colliderVertices.size() && m_colliderIndices.size())
    return;

  m_colliderVertices.clear();
  m_colliderVertices.reserve(m_vertices.size());
  m_colliderIndices.clear();
  m_colliderIndices.reserve(m_vertices.size());;

  foreach(QSharedPointer<MeshSubset> subset, m_subsets)
  {
    if(!subset->material() || !subset->material()->collidable())
      continue;

    if(subset->drawer().Topology != PrimitiveDrawer::TriList)
      continue;

    const IndexedPrimitiveDrawer& drawer = subset->drawer();
    unsigned* indices = &m_indices[drawer.StartIndex];

    for(unsigned i = 0; i < drawer.PrimitiveCount; ++i)
    {
      unsigned ii = i * 3;
      const MeshVertex& v1 = m_vertices[drawer.BaseIndex + indices[ii + 0]];
      const MeshVertex& v2 = m_vertices[drawer.BaseIndex + indices[ii + 1]];
      const MeshVertex& v3 = m_vertices[drawer.BaseIndex + indices[ii + 2]];

      m_colliderIndices.push_back(m_colliderVertices.size());
      m_colliderVertices.push_back(v1.origin);
      m_colliderIndices.push_back(m_colliderVertices.size());
      m_colliderVertices.push_back(v2.origin);
      m_colliderIndices.push_back(m_colliderVertices.size());
      m_colliderVertices.push_back(v3.origin);
    }
  }
}

inline bool testEdge(const vec3& a, const vec3& b, const vec3& pt, float eps = 0.1f)
{
  float lena = (pt - a).length();
  float lenb = (pt - b).length();
  float len = (b - a).length();
  float diff = lena + lenb - len;
  return diff < eps;
}

inline bool testEdges(const vec3& a, const vec3& b, const vec3& c, const vec3& pt, float eps = 0.1f)
{
  if(testEdge(a, b, pt, eps) ||
     testEdge(a, c, pt, eps) ||
     testEdge(b, c, pt, eps))
    return true;
  return false;
}

inline bool inTri(const vec3& a, const vec3& b, const vec3& c, const vec3& pt)
{
  vec3 d0 = c - a;
  vec3 d1 = b - a;
  vec3 d2 = pt - a;

  float d00 = d0 ^ d0;
  float d01 = d0 ^ d1;
  float d02 = d0 ^ d2;
  float d11 = d1 ^ d1;
  float d12 = d1 ^ d2;

  float idenom = 1.0f / (d00 * d11 - d01 * d01);
  float u = (d11 * d02 - d01 * d12) * idenom;
  float v = (d00 * d12 - d01 * d02) * idenom;

  if(u < 0 || v < 0 || u + v > 1)
    return false;
  return true;
}

bool Mesh::collide(const ray& dir, CollideInfo &collide)
{
  bool found = false;

  foreach(QSharedPointer<MeshSubset> subset, m_subsets)
  {
    if(!subset->material())
    {
      if(!subset->material()->collidable() && ~collide.flags & CollideInfo::Picking)
        continue;
    }

    float time = 0.0f;

    if(subset->sphereBounds().test(dir.Origin))
    {
      time = subset->sphereBounds().collide(dir);
      if(time < 0.0f || (found && collide.hitTime < time))
        continue;
    }

    if(subset->drawer().Topology != PrimitiveDrawer::TriList)
      continue;

    const IndexedPrimitiveDrawer& drawer = subset->drawer();
    unsigned* indices = &m_indices[drawer.StartIndex];

    for(unsigned i = 0; i < drawer.PrimitiveCount; ++i)
    {
      unsigned ii = i * 3;
      const MeshVertex& v1 = m_vertices[drawer.BaseIndex + indices[ii + 0]];
      const MeshVertex& v2 = m_vertices[drawer.BaseIndex + indices[ii + 1]];
      const MeshVertex& v3 = m_vertices[drawer.BaseIndex + indices[ii + 2]];

      vec4 plane(v1.origin, v2.origin, v3.origin);

      // ignore back face
      if((plane ^ dir.Origin) < 0.0f)
        continue;

      time = plane.collide(dir);

      if(time <= 0.0f || time > collide.hitTime)
        continue;

      vec3 hit = dir.hit(time);

      if(collide.flags & CollideInfo::Edges)
      {
        if(!testEdges(v1.origin, v2.origin, v3.origin, hit))
          continue;
      }

      else
      {
        if(!inTri(v1.origin, v2.origin, v3.origin, hit))
          continue;
      }

      collide.hitTime = time;
      collide.hit = hit;
      collide.collider = QWeakPointer<QObject>(this).toStrongRef();
      collide.material = subset->material();

      if(collide.flags & CollideInfo::Fast)
        return true;

      found = true;
    }
  }
  return found;
}

void Mesh::generateFrame(Frame &frame, const matrix &objectTransform)
{
  foreach(QSharedPointer<MeshSubset> subset, m_subsets)
  {
    subset->generateFrame(frame, objectTransform);
  }
}

void Mesh::generateDebugFrame(Frame &frame, const matrix &objectTransform, QObject *owner)
{
  foreach(QSharedPointer<MeshSubset> subset, m_subsets)
  {
    subset->generateDebugFrame(frame, objectTransform, owner);
  }
}

void Mesh::generateColorPickFrame(ColorPickFrame &frame, const matrix &objectTransform, QObject *owner)
{
  foreach(QSharedPointer<MeshSubset> subset, m_subsets)
  {
    subset->generateColorPickFrame(frame, objectTransform, owner);
  }
}

const QSharedPointer<VertexBuffer> & Mesh::renderVertexBuffer() const
{
  return m_renderVertexBuffer;
}

const QSharedPointer<IndexBuffer> & Mesh::renderIndexBuffer() const
{
  return m_renderIndexBuffer;
}

void Mesh::buildColliderObject()
{
  generateColliderVertices();

  if(m_colliderVertices.isEmpty())
  {
    m_colliderObject.clear();
    return;
  }

  switch(m_colliderType)
  {
  default:
  case ColliderDisabled:
    m_colliderObject.clear();
    break;

#ifndef NO_PHYSICS
  case ColliderBox:
    m_colliderObject = QSharedPointer<Collider>(new BoxCollider(box(m_colliderVertices.constData(), m_colliderVertices.size())));
    break;

  case ColliderSphere:
    m_colliderObject = QSharedPointer<Collider>(new SphereCollider(sphere(m_colliderVertices.constData(), m_colliderVertices.size())));
    break;

  case ColliderConvex:
    m_colliderObject = QSharedPointer<Collider>(new ConvexCollider(ConvexCollider::cookConvexMesh(m_colliderVertices)));
    break;

  case ColliderExact:
    m_colliderObject = QSharedPointer<Collider>(new TriCollider(TriCollider::cookTriangleMesh(m_colliderVertices, m_colliderIndices)));
    break;
#endif
  }
}

unsigned Mesh::renderVertexOffset() const
{
  return m_renderVertexOffset;
}

unsigned Mesh::renderIndexOffset() const
{
  return m_renderIndexOffset;
}
