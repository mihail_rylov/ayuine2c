#include "MeshViewer.hpp"
#include "ui_MeshViewer.h"

#include <MeshSystem/Mesh.hpp>
#include <MeshSystem/MeshSystem.hpp>

#include <Pipeline/WireFrame.hpp>
#include <Pipeline/SolidFrame.hpp>
#include <Pipeline/BoxFragment.hpp>
#include <Pipeline/AmbientLight.hpp>
#include <Pipeline/PointLight.hpp>

#include <Render/RenderSystem.hpp>
#include <Render/RenderComponentWidget.hpp>

#include <QDebug>
#include <QFileDialog>

#include "CameraController/ModelViewController.hpp"

#include <QAbstractItemModel>

class MeshItemModel : public QAbstractItemModel
{
  Q_OBJECT

public:
  MeshItemModel(const QSharedPointer<Mesh>& mesh)
  {
    m_mesh = mesh;
  }

private slots:
  void objectNameChanged(QObject* object)
  {
    if(Mesh* mesh = qobject_cast<Mesh*>(object))
    {
      QModelIndex parentIndex = createIndex(0, 0, mesh);
      emit dataChanged(parentIndex, parentIndex);
    }
    else if(MeshSubset* meshSubset = qobject_cast<MeshSubset*>(object))
    {
      QWeakPointer<MeshSubset> meshSubsetPtr(meshSubset);
      QModelIndex parentIndex = createIndex(m_mesh->subsets().indexOf(meshSubsetPtr.toStrongRef()), 0, meshSubset);
      emit dataChanged(parentIndex, parentIndex);
    }
  }

public:
  QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const
  {
    if(row < 0 || column < 0)
      return QModelIndex();

    if(parent.isValid())
    {
      QObject* object = (QObject*)parent.internalPointer();

      if(Mesh* mesh = qobject_cast<Mesh*>(object))
      {
        if(mesh->subsets().size() <= row)
          return QModelIndex();
        connect(mesh->subsets()[row].data(), SIGNAL(objectNameChanged(QObject*)), SLOT(objectNameChanged(QObject*)));
        return createIndex(row, column, mesh->subsets()[row].data());
      }
      else
      {
        return QModelIndex();
      }
    }
    else
    {
      if(row == 0)
      {
        connect(m_mesh.data(), SIGNAL(objectNameChanged(QObject*)), SLOT(objectNameChanged(QObject*)));
        return createIndex(row, column, m_mesh.data());
      }
      else
        return QModelIndex();
    }
  }

  QModelIndex parent(const QModelIndex &child) const
  {
    if(child.isValid())
    {
      QObject* object = (QObject*)child.internalPointer();

      if(qobject_cast<MeshSubset*>(object))
        return createIndex(0, 0, m_mesh.data());
      else
        return QModelIndex();
    }
    else
    {
      return QModelIndex();
    }
  }

  int rowCount(const QModelIndex &parent = QModelIndex()) const
  {
    if(parent.isValid())
    {
      QObject* object = (QObject*)parent.internalPointer();

      if(Mesh* mesh = qobject_cast<Mesh*>(object))
        return mesh->subsets().size();
      else
        return 0;
    }
    return 1;
  }

  int columnCount(const QModelIndex &parent = QModelIndex()) const
  {
    if(parent.isValid())
      return 1;
    return 1;
  }

  QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const
  {
    if(!index.isValid())
      return QVariant();

    QObject* object = (QObject*)index.internalPointer();

    if(Mesh* mesh = qobject_cast<Mesh*>(object))
    {
      switch(role)
      {
      case Qt::DisplayRole:
        return mesh->objectName().length() ? mesh->objectName() : "Mesh";

      default:
        break;
      }
    }
    else if(MeshSubset* meshSubset = qobject_cast<MeshSubset*>(object))
    {
      switch(role)
      {
      case Qt::DisplayRole:
        return meshSubset->objectName().length() ? meshSubset->objectName() : ("Subset " + QString::number(index.row()+1));

      default:
        break;
      }
    }
    return QVariant();
  }

private:
  QSharedPointer<Mesh> m_mesh;
};

Q_REGISTER_RESOURCE_EDITOR(Mesh, MeshViewer)

MeshViewer::MeshViewer(QWidget *parent) :
  FrameEditor(parent)
{
  Ui::MeshViewer::setupUi(this);

  m_cameraController.reset(new ModelViewController(renderComponents()));

  meshLayout->addWidget(renderComponents());
}

MeshViewer::~MeshViewer()
{
}

void MeshViewer::prepareComponents()
{
  FrameEditor::prepareComponents();

  m_frame->clear();

  if(!m_mesh)
    return;

  QVector<QObject*> selectedObjects;

  foreach(const QModelIndex& model, objectsView->selectionModel()->selectedIndexes())
  {
    QObject* object = (QObject*)model.internalPointer();
    if(!object)
      continue;
    selectedObjects.append(object);
  }

  if(actionShowSelected->isChecked())
  {
    // generate frame
    if(selectedObjects.contains(m_mesh.data()))
    {
      m_mesh->generateFrame(*m_frame, mat4Identity);
    }
    else
    {
      foreach(QObject* object, selectedObjects)
      {
        if(MeshSubset* meshSubset = qobject_cast<MeshSubset*>(object))
        {
          meshSubset->generateFrame(*m_frame, mat4Identity);
        }
      }
    }
  }
  else
  {
    // generate frame
    m_mesh->generateFrame(*m_frame, mat4Identity);
  }

  if(actionShowBox->isChecked())
  {
    foreach(QObject* object, selectedObjects)
    {
      if(Mesh* mesh = qobject_cast<Mesh*>(object))
      {
        addMeshBox(*mesh, *m_frame);
      }
      else if(MeshSubset* meshSubset = qobject_cast<MeshSubset*>(object))
      {
        addMeshSubsetBox(*meshSubset, *m_frame);
        meshSubset->generateFrame(*m_frame, mat4Identity);
      }
    }
  }

  if(m_mesh && m_cameraController)
  {
    QScopedPointer<BoxFragment> boxFragment(new BoxFragment());
    boxFragment->setBounds(box(m_cameraController->origin(), 0.02f));
    boxFragment->setColor(Qt::darkGray);
    boxFragment->setFixedState(RenderSystem::FixedConst);
    boxFragment->setRasterizerState(RenderSystem::CullNone);
    boxFragment->setFillMode(RenderSystem::Solid);
    m_frame->addFragment(boxFragment.take());

    QScopedPointer<PointLight> pointLight(new PointLight());
    pointLight->setOrigin(m_cameraController->origin());
    pointLight->setRadius(m_mesh->sphereBounds().Radius * 2.0f);
    pointLight->lightState().setMaterialShader("SimpleLightingModel.grcz");
    pointLight->lightState().set("lightPosition", vec4(pointLight->origin(), 1.0f / pointLight->radius()));
    m_frame->addLight(pointLight.take());

    m_frame->addLight(new AmbientLight());
  }
}

void MeshViewer::finalizeComponents()
{
  QStringList lines;

  if(m_mesh)
  {
    lines.append(QString("Vertices: %1").arg(QString::number(m_mesh->vertices().size())));
    lines.append(QString("Indices: %1").arg(QString::number(m_mesh->indices().size())));
    lines.append(QString("Subsets: %1").arg(QString::number(m_mesh->subsets().size())));
    lines.append(QString("BoundingBox: (%1, %2, %3) (%4, %5, %6)").arg(QString::number(m_mesh->boxBounds().Mins.X), QString::number(m_mesh->boxBounds().Mins.Y), QString::number(m_mesh->boxBounds().Mins.Z),
                                                                       QString::number(m_mesh->boxBounds().Maxs.X), QString::number(m_mesh->boxBounds().Maxs.Y), QString::number(m_mesh->boxBounds().Maxs.Z)));
    lines.append(QString("BoundingSphere: (%1, %2, %3) (%4)").arg(QString::number(m_mesh->sphereBounds().Center.X), QString::number(m_mesh->sphereBounds().Center.Y), QString::number(m_mesh->sphereBounds().Center.Z),
                                                                  QString::number(m_mesh->sphereBounds().Radius)));

    lines.append("");
  }

  lines.append(QString("Origin: (%1, %2, %3)").arg(QString::number(m_cameraController->origin().X), QString::number(m_cameraController->origin().Y), QString::number(m_cameraController->origin().Z)));
  lines.append(QString("Zoom: %1").arg(QString::number(m_cameraController->zoom())));

  if(PerspectiveController* perspectiveController = qobject_cast<PerspectiveController*>(m_cameraController.data()))
  {
    lines.append(QString("Angles: (%1, %2, %3)").arg(QString::number(perspectiveController->angles().Yaw), QString::number(perspectiveController->angles().Pitch), QString::number(perspectiveController->angles().Roll)));
  }

  lines.append("");

  getRenderSystem().drawText(vec2(0,0), lines.join("\n"));
}

const QMetaObject * MeshViewer::resourceType() const
{
  return &Mesh::staticMetaObject;
}

QSharedPointer<Resource> MeshViewer::resource() const
{
  return m_mesh;
}

bool MeshViewer::setResource(const QSharedPointer<Resource> &newResource)
{
  if(newResource.isNull())
  {
    m_mesh.clear();
    setPropertyObject(NULL);
    updateUi();
    objectsView->setModel(NULL);
    m_meshItemModel.reset();
    return true;
  }

  if(newResource.objectCast<Mesh>().isNull())
    return false;

  m_mesh = newResource.objectCast<Mesh>();

  if(m_mesh)
  {
    connect(m_mesh.data(), SIGNAL(objectNameChanged()), SLOT(updateUi()));
    m_cameraController->setOrigin(m_mesh->sphereBounds().Center);
    m_cameraController->setZoom(m_mesh->sphereBounds().Radius);
    setPropertyObject(m_mesh.data());
    updateUi();
    objectsView->setModel(NULL);
    m_meshItemModel.reset(new MeshItemModel(m_mesh));
    objectsView->setModel(m_meshItemModel.data());
    return m_mesh;
  }
  return false;
}

void MeshViewer::updateSelected(const QModelIndex &modelIndex)
{
  if(!modelIndex.isValid())
    return;

  QObject* object = (QObject*)modelIndex.internalPointer();

  if(Mesh* mesh = qobject_cast<Mesh*>(object))
  {
    setPropertyObject(mesh);
  }
  else if(MeshSubset* meshSubset = qobject_cast<MeshSubset*>(object))
  {
    setPropertyObject(meshSubset);
  }
}

void MeshViewer::addMeshBox(Mesh &mesh, Frame &frame)
{
  QScopedPointer<BoxFragment> meshBox(new BoxFragment());
  meshBox->setBounds(mesh.boxBounds());
  meshBox->setColor(Qt::lightGray);
  meshBox->setFixedState(RenderSystem::FixedConst);
  meshBox->setRasterizerState(RenderSystem::CullNone);
  meshBox->setFillMode(RenderSystem::Wire);
  frame.addFragment(meshBox.take());
}

void MeshViewer::addMeshSubsetBox(MeshSubset &meshSubset, Frame &frame)
{
  QScopedPointer<BoxFragment> subsetBox(new BoxFragment());
  subsetBox->setBounds(meshSubset.boxBounds());
  subsetBox->setColor(Qt::darkGray);
  subsetBox->setFixedState(RenderSystem::FixedConst);
  subsetBox->setRasterizerState(RenderSystem::CullNone);
  subsetBox->setFillMode(RenderSystem::Wire);
  frame.addFragment(subsetBox.take());
}

#include "MeshViewer.moc"
