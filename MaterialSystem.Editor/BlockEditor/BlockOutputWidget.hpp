#ifndef BLOCKOUTPUTWIDGET_HPP
#define BLOCKOUTPUTWIDGET_HPP

#include <QCheckBox>

#include <MaterialSystem/ShaderBlock.hpp>

class BlockWidget;

class BlockOutputWidget : public QCheckBox
{
  Q_OBJECT
public:
  explicit BlockOutputWidget(QWidget *parent = 0);
  ~BlockOutputWidget();

public:
  ShaderBlock::Link link() const;
  void setLink(ShaderBlock::OutputLink link);
  QSharedPointer<ShaderBlock> block() const;
  BlockWidget* blockWidget() const;

protected:
  void mousePressEvent(QMouseEvent* event);

private:
  ShaderBlock::Link m_link;
};

#endif // BLOCKOUTPUTWIDGET_HPP
