#ifndef BlockWidget_HPP
#define BlockWidget_HPP

#include <QFrame>
#include <QPushButton>

#include <MaterialSystem/ShaderBlock.hpp>

namespace Ui {
class BlockWidget;
}

class BlockInputWidget;
class BlockOutputWidget;
class ResourceEditor;
class BlockArea;

class BlockCaption : public QPushButton
{
public:
  explicit BlockCaption(QWidget* parent = 0);

protected:
  void mousePressEvent(QMouseEvent* event);
  void mouseMoveEvent(QMouseEvent* event);
  void mouseReleaseEvent(QMouseEvent* event);

private:
  QPoint offset;
};

class BlockWidget : public QWidget
{
  Q_OBJECT

public:
  explicit BlockWidget(const QSharedPointer<ShaderBlock> &block, QWidget *parent = 0);
  ~BlockWidget();

public slots:
  bool selected() const;
  void setSelected(bool selected);

  QSharedPointer<ShaderBlock> block() const;
  void updateInputs();
  void updateOutputs();
  void updateCaption();
  void updateContent();
  void updateUi();

  BlockInputWidget* input(unsigned index) const;
  unsigned inputCount() const;

  BlockOutputWidget* output(unsigned index) const;
  unsigned outputCount() const;

  BlockArea* blockArea() const;
  ResourceEditor* resourceEditor() const;

protected:
  void focusInEvent(QFocusEvent *);
  void moveEvent(QMoveEvent *moveEvent);

private:
  Ui::BlockWidget *ui;
  QSharedPointer<ShaderBlock> m_block;
  QVector<BlockInputWidget*> m_inputs;
  QVector<BlockOutputWidget*> m_outputs;
};

#endif // BlockWidget_HPP
