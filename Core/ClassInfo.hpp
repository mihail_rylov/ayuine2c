#ifndef CLASSINFO_HPP
#define CLASSINFO_HPP

#include <QMetaObject>
#include <QMetaProperty>
#include <QString>
#include <QDataStream>
// #include <QIcon>
#include "Core.hpp"

namespace Qt
{
class QIcon;
}

class CORE_EXPORT ClassInfo
{
public:
  ClassInfo(const QMetaObject* metaObject);
  virtual ~ClassInfo();

public:
  static void registerMetaObject(const QMetaObject* metaObject);
  static void unregisterMetaObject(const QMetaObject* metaObject);
  static QList<const QMetaObject*> metaObjects();
  static QList<const QMetaObject*> metaObjects(const QMetaObject* baseMetaObject);

public:
  static QSharedPointer<QObject> createNewInstance(const QByteArray& name);
  static const QMetaObject* getMetaObject(const QByteArray& name);
  static const QMetaObject* getMetaObject(const QByteArray& name, const QMetaObject* baseMetaObject);
  static QIcon getIcon(const QMetaObject* metaObject);
  static QIcon getIcon(const QByteArray& name);

protected:
  const QMetaObject* m_metaObject;
};

template<typename T>
class ClassInfoT : public ClassInfo
{
public:
  ClassInfoT() : ClassInfo(&T::staticMetaObject)
  {
  }
};

#ifdef _MSC_VER
#define Q_REGISTER_CLASS(Class) \
  static ClassInfoT<Class> __ClassInfo##Class;
#else
#define Q_REGISTER_CLASS(Class) \
  static ClassInfoT<Class> __ClassInfo##Class __attribute__ ((used, init_priority (100)));
#endif

class CORE_EXPORT GadgetInfo
{
public:
  GadgetInfo(const QMetaObject* metaObject, int typeId);
  virtual ~GadgetInfo();

public:
  const QMetaObject* metaObject() const;
  int typeId() const;

protected:
  virtual QVariant property(const QVariant& value, const char* name) const = 0;
  virtual bool setProperty(QVariant& value, const char* name, const QVariant& newValue) const = 0;

public:
  static QList<const GadgetInfo*> allGadgets();

public:
  static bool staticIsGadget(const QVariant& value);
  static const GadgetInfo* staticGadgetInfo(const QVariant& value);
  static const QMetaObject* staticMetaObject(const QVariant& value);
  static QVariant staticProperty(const QVariant& value, const char* name);
  static bool staticSetProperty(QVariant& value, const char* name, const QVariant& newValue);
  static const GadgetInfo* staticGadgetInfo(const QByteArray& name);
  static const QMetaObject* staticMetaObject(const QByteArray& name);

protected:
  const QMetaObject* m_metaObject;
  int m_typeId;
};

template<typename T>
class QValueType : public QObject
{
public:
    const QMetaObject *metaObject() const
    {
        return &T::staticMetaObject;
    }

    int qt_metacall(QMetaObject::Call _c, int _id, void **_a)
    {
        QMetaObjectExtraData* extradata = (QMetaObjectExtraData*)T::staticMetaObject.d.extradata;
        if(extradata)
            extradata->static_metacall(this, _c, _id, _a);
        return _id;
    }

public:
    QValueType()
    {
    }
    QValueType(const T& ptr) : d_ptr(ptr)
    {
    }

public:
    operator const T& () const
    {
        return d_ptr;
    }

public:
    T d_ptr;
};

template<typename T>
class GadgetInfoT : public GadgetInfo
{
public:
  GadgetInfoT() : GadgetInfo(&T::staticMetaObject, qMetaTypeId<T>())
  {
  }

public:
  QVariant property(const QVariant& value, const char* name) const
  {
    if(!value.canConvert<T>())
      return QVariant();

    QValueType<T> valueType(*(T*)value.data());
    return valueType.property(name);
  }

  bool setProperty(QVariant& value, const char* name, const QVariant& newValue) const
  {
    if(!value.canConvert<T>())
      return false;

    QValueType<T> valueType(*(T*)value.data());
    if(valueType.setProperty(name, newValue))
    {
      *(T*)value.data() = valueType.d_ptr;
      return true;
    }
    return false;
  }
};

#ifdef _MSC_VER
#define Q_REGISTER_GADGET(Class) \
  static GadgetInfoT<Class> __GadgetInfo##Class;
#else
#define Q_REGISTER_GADGET(Class) \
  static GadgetInfoT<Class> __GadgetInfo##Class __attribute__ ((used, init_priority (100)));
#endif

#endif // CLASSINFO_HPP
