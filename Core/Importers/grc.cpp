#include "ResourceImporter.hpp"
#include "Serializer.hpp"

class GenericResourceImporter : public ResourceImporter
{
public:
  GenericResourceImporter()
  {
    registerNativeImporter("grc");
    registerExporter("grc");
  }

  QSharedPointer<Resource> load(const QSharedPointer<QIODevice>& device, const QByteArray& extension)
  {
    if(!device)
      return QSharedPointer<Resource>();

    QSharedPointer<QObject> object(::loadFromDevice(*device));
    QSharedPointer<Resource> resource(object.objectCast<Resource>());

    if(resource)
    {
      resource->setFileName(device->objectName());
      return resource;
    }

    return QSharedPointer<Resource>();
  }

  const QMetaObject* metaObject(const QSharedPointer<QIODevice> &device, const QByteArray &extension)
  {
    if(!device)
      return NULL;
    return ::peakMetaObjectFromDevice(*device);
  }

  bool save(QSharedPointer<Resource> &resource, const QSharedPointer<QIODevice> &device, const QByteArray &extension)
  {
    if(!device)
      return false;
    saveToDevice(resource, *device);
    return true;
  }
};

static GenericResourceImporter genericResourceImporter;
