#include "ClassInfo.hpp"
#include "Resource.hpp"
#include <QObject>
#include <QHash>
#include <QMetaType>
#include <QDebug>
#include <QIcon>
#include <windows.h>

static QHash<QByteArray, const QMetaObject*>& staticClasses()
{
  static QHash<QByteArray, const QMetaObject*> array;
  return array;
}

inline bool isBaseOf(const QMetaObject* superObject, const QMetaObject* metaObject)
{
  while(metaObject)
  {
    if(metaObject == superObject)
      return true;
    metaObject = metaObject->superClass();
  }
  return false;
}

ClassInfo::ClassInfo(const QMetaObject* metaObject) : m_metaObject(metaObject)
{
  Q_ASSERT(metaObject);

  if(!metaObject)
    return;

  if(staticClasses().contains(metaObject->className()))
    return;

  // qDebug() << "[CLASSINFO] Registering " << metaObject->className();

  // add to module list
  staticClasses()[metaObject->className()] = metaObject;
}

ClassInfo::~ClassInfo()
{
  if(!m_metaObject)
    return;

  // qDebug() << "[CLASSINFO] Deregistering " << metaObject->className();

  // remove module from list
  if(staticClasses()[m_metaObject->className()] == m_metaObject)
    staticClasses().remove(m_metaObject->className());
}

const QMetaObject* ClassInfo::getMetaObject(const QByteArray &name)
{
  if(staticClasses().contains(name))
  {
    return staticClasses()[name];
  }
  return NULL;
}

const QMetaObject* ClassInfo::getMetaObject(const QByteArray &name, const QMetaObject* baseMetaObject)
{
  const QMetaObject* metaObject = getMetaObject(name);
  if(!metaObject)
    return NULL;

  for(const QMetaObject* subMetaObject = metaObject; subMetaObject; subMetaObject = subMetaObject->superClass())
  {
    if(subMetaObject == baseMetaObject)
    {
      return metaObject;
    }
  }
  return NULL;
}

QSharedPointer<QObject> ClassInfo::createNewInstance(const QByteArray &name)
{
  if(const QMetaObject* object = getMetaObject(name))
  {
    return QSharedPointer<QObject>(object->newInstance());
  }
  return QSharedPointer<QObject>();
}

QList<const QMetaObject*> ClassInfo::metaObjects(const QMetaObject* baseMetaObject)
{
  if(!baseMetaObject)
    return metaObjects();

  QList<const QMetaObject*> subMetaObjects;

  foreach(const QMetaObject* metaObject, metaObjects())
  {
    for(const QMetaObject* subMetaObject = metaObject; subMetaObject; subMetaObject = subMetaObject->superClass())
    {
      if(subMetaObject == baseMetaObject)
      {
        subMetaObjects.append(metaObject);
        break;
      }
    }
  }
  return subMetaObjects;
}

QIcon ClassInfo::getIcon(const QMetaObject *metaObject)
{
  static QHash<const QMetaObject*, QIcon> icons;

  if(!metaObject)
    return QIcon();

  if(icons.contains(metaObject))
    return icons[metaObject];

  QIcon icon;

  int index = metaObject->indexOfClassInfo("IconFileName");
  if(index >= 0)
  {
    icon = QIcon(metaObject->classInfo(index).value());
  }

  icons[metaObject] = icon;
  return icon;
}

QIcon ClassInfo::getIcon(const QByteArray& name)
{
  return getIcon(getMetaObject(name));
}

QList<const QMetaObject *> ClassInfo::metaObjects()
{
  return staticClasses().values();
}
