#include "FirstPersonController.hpp"
#include <QWidget>
#include <QMouseEvent>

FirstPersonController::FirstPersonController(QWidget *parent) :
  PerspectiveController(parent)
{
  m_angularKeyVelocity = Pi / 30.0f;
  m_linearKeyVelocity = 0.5f;
}

QString FirstPersonController::section() const
{
  return "firstPersonController";
}

float FirstPersonController::angularKeyVelocity() const
{
  return m_angularKeyVelocity;
}

void FirstPersonController::setAngularKeyVelocity(float angularKeyVelocity)
{
  m_angularKeyVelocity = angularKeyVelocity;
}

float FirstPersonController::linearKeyVelocity() const
{
  return m_linearKeyVelocity;
}

void FirstPersonController::setLinearKeyVelocity(float linearKeyVelocity)
{
  m_linearKeyVelocity = linearKeyVelocity;
}

void FirstPersonController::modelOnSphere(const sphere &center)
{
    setOrigin(center.Center - vec3(center.Radius * 1.0f,0,0));
}

bool FirstPersonController::updateCamera(QRect &viewport, Camera &camera)
{
  if(QWidget* widget = qobject_cast<QWidget*>(parent()))
  {
    viewport = widget->geometry();

    // Wyznacz wsp�czynnik ekranu
    float aspect = (float)viewport.width() / viewport.height();

    // Wyznacz ustawienia kamery
    Camera::fppCamera(camera, m_origin + m_offset, m_angles, Deg2Rad * m_fovy, aspect, 10000.0f);
    return true;
  }
  return false;
}

bool FirstPersonController::mousePress(QWidget *widget, QMouseEvent &event)
{
  if(QWidget::mouseGrabber() == widget)
    return false;

  if(event.buttons() & Qt::RightButton)
  {
    widget->grabMouse();
    widget->setFocus();
    m_mouse = event.globalPos();
    return true;
  }
  return false;
}

bool FirstPersonController::mouseRelease(QWidget *widget, QMouseEvent &event)
{
  if(QWidget::mouseGrabber() != widget)
    return false;
  if(event.buttons() & Qt::RightButton)
    return false;
  widget->releaseMouse();
  return true;
}

bool FirstPersonController::mouseLost(QWidget *widget, QMouseEvent &event)
{
  widget->releaseMouse();
  return true;
}

bool FirstPersonController::mouseMove(QWidget *widget, QMouseEvent &event)
{
  if(event.buttons() & Qt::RightButton)
  {
    QPoint delta = event.globalPos() - m_mouse;
    QCursor::setPos(m_mouse);

    float s = (float)delta.x() / widget->width();
    float t = - (float)delta.y() / widget->height();

    if(event.buttons() & Qt::LeftButton)
    {
      // Poruszaj si� lewo/prawo
      m_origin += m_angles.right() * linearVelocity() * s;

      if(event.modifiers() & Qt::ControlModifier)
      {
        // Poruszaj si� g�ra/d�
        m_origin += m_angles.up() * linearVelocity() * t;
      }
      else
      {
        // Poruszaj si� prz�d/ty�
        m_origin += m_angles.at() * linearVelocity() * t;
      }
    }
    else
    {
      // Obr�t kamery
      m_angles.Yaw += angularVelocity() * s;
      m_angles.Pitch += angularVelocity() * t;

      // Przytnij kamer�
      m_angles.Pitch = qBound<float>(m_angles.Pitch, -Pi / 2.0f, Pi / 2.0f);
    }

    widget->update();
    return true;
  }

  return false;
}

bool FirstPersonController::keyPress(QWidget *widget, QKeyEvent &event)
{
  switch(event.key())
  {
  case Qt::Key_Up:
      m_origin += m_angles.at() * linearKeyVelocity();
      break;

  case Qt::Key_Down:
      m_origin -= m_angles.at() * linearKeyVelocity();
      break;

  case Qt::Key_Left:
      m_origin -= m_angles.right() * linearKeyVelocity();
      break;

  case Qt::Key_Right:
      m_origin += m_angles.right() * linearKeyVelocity();
      break;

  case Qt::Key_PageUp: // look up
      m_angles.Pitch += angularKeyVelocity();
      break;

  case Qt::Key_PageDown: // look down
      m_angles.Pitch -= angularKeyVelocity();
      break;

  case Qt::Key_Home: // look left
      m_angles.Yaw -= angularKeyVelocity();
      break;

  case Qt::Key_End: // look right
      m_angles.Yaw += angularKeyVelocity();
      break;

  default:
    return false;
  }

  widget->update();
  return true;
}
