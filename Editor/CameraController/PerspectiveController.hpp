#ifndef PERSPECTIVECONTROLLER_HPP
#define PERSPECTIVECONTROLLER_HPP

#include "CameraController.hpp"

#include <Math/Euler.hpp>

class EDITOR_EXPORT PerspectiveController : public CameraController
{
  Q_OBJECT
public:
  explicit PerspectiveController(QWidget *parent = 0);

protected:
  virtual QString section() const;

public:
  const euler& angles() const;
  void setAngles(const euler& angles);

  float fovy() const;
  void setFovy(float fovy);

  vec3 screenToWorld(QPoint point);
  QPoint worldToScreen(const vec3& point);
  ray direction(QPoint point);

  QStringList toStringList() const;

public:
  float angularVelocity() const;
  void setAngularVelocity(float angularVelocity);

  float linearVelocity() const;
  void setLinearVelocity(float linearVelocity);

protected:
  float m_fovy;
  euler m_angles;
  float m_angularVelocity;
  float m_linearVelocity;
};

#endif // PERSPECTIVECONTROLLER_HPP
