#include "CameraController.hpp"
#include <QWidget>
#include <QMouseEvent>
#include <QKeyEvent>

#include <Pipeline/Frame.hpp>

CameraController::CameraController(QWidget *widget) :
    QObject(widget)
{
  m_zoom = 2.0f;
  m_minZoom = 1.0f;
  m_maxZoom = 20.0f;

  if(widget)
  {
    widget->installEventFilter(this);
    widget->setMouseTracking(true);
  }
}

CameraController::~CameraController()
{
}

const vec3& CameraController::origin() const
{
    return m_origin;
}

void CameraController::setOrigin(const vec3& origin)
{
    m_origin = origin;
}

const vec3& CameraController::offset() const
{
    return m_offset;
}

void CameraController::setOffset(const vec3& offset)
{
    m_offset = offset;
}

float CameraController::zoom() const
{
    return m_zoom;
}

void CameraController::setZoom(float zoom)
{
    m_zoom = zoom;
}

void CameraController::modelOnSphere(const sphere &center)
{
}

void CameraController::setMinZoom(float minZoom)
{
    m_minZoom = minZoom;
    m_zoom = qMax(m_zoom, m_minZoom);
}

void CameraController::setMaxZoom(float maxZoom)
{
    m_maxZoom = maxZoom;
    m_zoom = qMin(m_zoom, m_maxZoom);
}

bool CameraController::event(QEvent *event)
{
  if(event)
  {
    switch(event->type())
    {
    case QEvent::ParentAboutToChange:
      if(parent())
        parent()->removeEventFilter(this);
      break;

    case QEvent::ParentChange:
      if(parent())
        parent()->installEventFilter(this);
      break;

    default:
      break;
    }
  }
  return false;
}

bool CameraController::eventFilter(QObject *object, QEvent *event)
{
  QWidget* widget = qobject_cast<QWidget*>(object);
  if(!widget || !event)
    return false;
  if(object != parent())
    return false;

  switch(event->type())
  {
  case QEvent::MouseButtonPress:
    return mousePress(widget, *(QMouseEvent*)event);

  case QEvent::MouseButtonRelease:
   return mouseRelease(widget, *(QMouseEvent*)event);

  case QEvent::MouseMove:
    return mouseMove(widget, *(QMouseEvent*)event);

  case QEvent::KeyPress:
    return keyPress(widget, *(QKeyEvent*)event);

  case QEvent::MouseTrackingChange:
    return mouseLost(widget, *(QMouseEvent*)event);

  default:
    return false;
  }
}

bool CameraController::mousePress(QWidget *widget, QMouseEvent &event)
{
  return false;
}

bool CameraController::mouseRelease(QWidget *widget, QMouseEvent &event)
{
  return false;
}

bool CameraController::mouseDblClick(QWidget *widget, QMouseEvent &event)
{
  return false;
}

bool CameraController::mouseMove(QWidget *widget, QMouseEvent &event)
{
  return false;
}

bool CameraController::mouseLost(QWidget *widget, QMouseEvent &event)
{
  return false;
}

bool CameraController::keyPress(QWidget *widget, QKeyEvent &event)
{
  return false;
}

bool CameraController::loadSettings()
{
  return true;
}

bool CameraController::saveSettings(bool save)
{
  return true;
}

bool CameraController::updateFrame(Frame &frame)
{
  QRect viewport;
  Camera camera;

  if(updateCamera(viewport, camera))
  {
    frame.setCamera(camera);
    frame.setViewport(viewport);
    return true;
  }
  return false;
}

QStringList CameraController::toStringList() const
{
  QStringList lines;
  lines.append(QString("Origin: (%1, %2, %3)").arg(QString::number(origin().X), QString::number(origin().Y), QString::number(origin().Z)));
  lines.append(QString("Zoom: %1").arg(QString::number(zoom())));
  return lines;
}
