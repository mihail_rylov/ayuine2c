#include "EulerProperty.hpp"

#include <Math/Euler.hpp>

Q_IMPLEMENT_PROPERTYTYPE(euler, EulerProperty)

EulerProperty::EulerProperty(const QString& name, QObject* propertyObject, QObject* parent)
    : EditableProperty(&EulerProperty::staticMetaObject, name, propertyObject, parent)
{
}

float EulerProperty::yaw() const
{
  return value().value<euler>().Yaw * Rad2Deg;
}

void EulerProperty::setYaw(float yaw)
{
  setValue(QVariant::fromValue(euler(yaw * Deg2Rad, pitch() * Deg2Rad, roll() * Deg2Rad)));
}

float EulerProperty::pitch() const
{
  return value().value<euler>().Pitch * Rad2Deg;
}

void EulerProperty::setPitch(float pitch)
{
  setValue(QVariant::fromValue(euler(yaw() * Deg2Rad, pitch * Deg2Rad, roll() * Deg2Rad)));
}

float EulerProperty::roll() const
{
  return value().value<euler>().Roll * Rad2Deg;
}

void EulerProperty::setRoll(float roll)
{
  setValue(QVariant::fromValue(euler(yaw() * Deg2Rad, pitch() * Deg2Rad, roll * Deg2Rad)));
}
