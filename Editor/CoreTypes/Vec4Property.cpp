#include "Vec4Property.hpp"

#include <Math/Vec4.hpp>

Q_IMPLEMENT_PROPERTYTYPE(vec4, Vec4Property)

Vec4Property::Vec4Property(const QString& name, QObject* propertyObject, QObject* parent)
    : EditableProperty(&Vec4Property::staticMetaObject, name, propertyObject, parent)
{
}

float Vec4Property::x() const
{
  return value().value<vec4>().X;
}

void Vec4Property::setX(float x)
{
  setValue(QVariant::fromValue(vec4(x, y(), z(), w())));
}

float Vec4Property::y() const
{
  return value().value<vec4>().Y;
}

void Vec4Property::setY(float y)
{
  setValue(QVariant::fromValue(vec4(x(), y, z(), w())));
}

float Vec4Property::z() const
{
  return value().value<vec4>().Z;
}

void Vec4Property::setZ(float z)
{
  setValue(QVariant::fromValue(vec4(x(), y(), z, w())));
}

float Vec4Property::w() const
{
  return value().value<vec4>().W;
}

void Vec4Property::setW(float w)
{
  setValue(QVariant::fromValue(vec4(x(), y(), z(), w)));
}
