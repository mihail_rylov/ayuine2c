#include "Vec3Property.hpp"

#include <Math/Vec3.hpp>

Q_IMPLEMENT_PROPERTYTYPE(vec3, Vec3Property)

Vec3Property::Vec3Property(const QString& name, QObject* propertyObject, QObject* parent)
    : EditableProperty(&Vec3Property::staticMetaObject, name, propertyObject, parent)
{
}

float Vec3Property::x() const
{
  return value().value<vec3>().X;
}

void Vec3Property::setX(float x)
{
  setValue(QVariant::fromValue(vec3(x, y(), z())));
}

float Vec3Property::y() const
{
  return value().value<vec3>().Y;
}

void Vec3Property::setY(float y)
{
  setValue(QVariant::fromValue(vec3(x(), y, z())));
}

float Vec3Property::z() const
{
  return value().value<vec3>().Z;
}

void Vec3Property::setZ(float z)
{
  setValue(QVariant::fromValue(vec3(x(), y(), z)));
}
