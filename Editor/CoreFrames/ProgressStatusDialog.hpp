#ifndef PROGRESSSTATUSDIALOG_HPP
#define PROGRESSSTATUSDIALOG_HPP

#include <QProgressDialog>
#include <Core/ProgressStatus.hpp>

#include "Editor.hpp"

class EDITOR_EXPORT ProgressStatusDialog : public QProgressDialog
{
  Q_OBJECT
public:
  explicit ProgressStatusDialog(QWidget *parent = 0);
  explicit ProgressStatusDialog(const QString & labelText, const QString & cancelButtonText, QWidget *parent = 0);

signals:

public slots:

private slots:
  void valueChanged(ProgressStatus* progressStatus);
  void textChanged(ProgressStatus* progressStatus);

private:
  ProgressStatus m_progressStatus;
};

#endif // PROGRESSSTATUSDIALOG_HPP
