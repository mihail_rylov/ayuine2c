#include "ResourceBrowser.hpp"
#include "ui_ResourceBrowser.h"

#include "ResourceEditor.hpp"
#include "ResourceIconProvider.hpp"
#include "LogView.hpp"

#include <Core/Resource.hpp>
#include <Core/ResourceImporter.hpp>
#include <Core/ClassInfo.hpp>

#include <QFileSystemModel>
#include <QMessageBox>
#include <QSplitter>
#include <QInputDialog>
#include <QFileDialog>
#include <QSortFilterProxyModel>
#include <QTreeView>

class FileFilterModel : public QSortFilterProxyModel
{
public:
  FileFilterModel(QObject *parent = 0) : QSortFilterProxyModel(parent)
  {
    foreach(QString name, ResourceImporter::importExtensions())
    {
      m_suffixList.insert(name);
    }
  }

public:
  const QModelIndex& rootIndex()
  {
    return m_rootIndex;
  }

  QModelIndex rootFilterIndex()
  {
    return mapFromSource(m_rootIndex);
  }

  void setRootIndex(const QModelIndex& index)
  {
    setSourceModel(sourceModel());
    m_rootIndex = index;
  }

protected:
  virtual bool filterAcceptsRow(int source_row, const QModelIndex & source_parent) const
  {
    if(!source_parent.isValid())
      return true;

    QModelIndex index = source_parent.child(source_row, 0);
    if(index == m_rootIndex)
      return true;

    QFileSystemModel* model = (QFileSystemModel*)sourceModel();
    if(model->isDir(index))
      return false;

    QString suffix = model->fileInfo(index).suffix().toLower();
    if(m_suffixList.contains(suffix))
      return true;
    return false;
  }

private:
  QModelIndex m_rootIndex;
  QSet<QString> m_suffixList;
};

class DirFilterModel : public QSortFilterProxyModel
{
public:
  DirFilterModel(QObject *parent = 0) : QSortFilterProxyModel(parent)
  {
  }

public:
  const QModelIndex& rootIndex()
  {
    return m_rootIndex;
  }

  QModelIndex rootFilterIndex()
  {
    return mapFromSource(m_rootIndex);
  }

  void setRootIndex(const QModelIndex& index)
  {
    m_rootIndex = index;
  }

protected:
  virtual bool filterAcceptsRow(int source_row, const QModelIndex & source_parent) const
  {
    if(!source_parent.isValid())
      return true;

    QModelIndex index = source_parent.child(source_row, 0);

    QFileSystemModel* model = (QFileSystemModel*)sourceModel();
    if(!model->isDir(index))
      return false;

    while(index.isValid())
    {
      if(index == m_rootIndex)
        return true;
      index = index.parent();
    }

    return false;
  }

private:
  QModelIndex m_rootIndex;
};

ResourceBrowser* ResourceBrowser::resourceBrowser = NULL;

inline QAction* addSeparator(QObject* parent = 0)
{
  QAction* action = new QAction(parent);
  action->setSeparator(true);
  return action;
}

ResourceBrowser::ResourceBrowser(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::ResourceBrowser)
{
  if(resourceBrowser == NULL)
    resourceBrowser = this;

  ui->setupUi(this);
  ui->logsWidget->setWidget(new LogView(ui->logsWidget));

  iconProvider = new ResourceIconProvider(this);

  connect(iconProvider, SIGNAL(iconLoaded(QString)), SLOT(iconLoaded(QString)), Qt::QueuedConnection);

  if(QSplitter* splitter = new QSplitter(this))
  {
    QList<int> sizes;
    sizes.append(50);
    sizes.append(350);

    splitter->addWidget(ui->dirList);
    splitter->addWidget(ui->fileList);
    splitter->setSizes(sizes);
    setCentralWidget(splitter);
  }

  fsModel = new QFileSystemModel(this);
  fsModel->setFilter(QDir::Dirs | QDir::Files | QDir::NoDotAndDotDot);
  fsModel->setIconProvider(iconProvider);

  fileFilterModel = new FileFilterModel(this);
  fileFilterModel->setSourceModel(fsModel);

  dirFilterModel = new DirFilterModel(this);
  dirFilterModel->setSourceModel(fsModel);

  ui->dirList->setModel(dirFilterModel);
  ui->dirList->hideColumn(1);
  ui->dirList->hideColumn(2);
  ui->dirList->hideColumn(3);
  ui->dirList->addAction(ui->actionNewFolder);
  ui->dirList->addAction(addSeparator(this));
  ui->dirList->addAction(ui->actionFolderRename);
  ui->dirList->addAction(ui->actionDeleteFolder);
  ui->dirList->addAction(addSeparator(this));
  ui->dirList->addAction(ui->actionShowExplorer);
  ui->dirList->addAction(addSeparator(this));
  ui->dirList->addAction(ui->actionSelectFolder);
  ui->dirList->expandToDepth(1);

  ui->fileList->setModel(fileFilterModel);
  ui->fileList->addAction(ui->actionNew);
  ui->fileList->addAction(addSeparator(this));
  ui->fileList->addAction(ui->actionOpen);
  ui->fileList->addAction(addSeparator(this));
  ui->fileList->addAction(ui->actionRename);
  ui->fileList->addAction(ui->actionDelete);
  ui->fileList->addAction(addSeparator(this));
  ui->fileList->addAction(ui->actionListView);

  connect(ui->fileList->selectionModel(), SIGNAL(currentChanged(QModelIndex,QModelIndex)), SLOT(updateUi()));

  setPath(Resource::getResourceDir().absolutePath());

  // fsModel->setNameFilterDisables(false);
  // fsModel->setNameFilters(nameFilters);
}

ResourceBrowser::~ResourceBrowser()
{
  if(resourceBrowser == this)
    resourceBrowser = NULL;

  delete ui;
}

void ResourceBrowser::updateUi()
{
  if(ui->fileList->currentIndex().isValid())
  {
    ui->actionDelete->setEnabled(true);
    ui->actionRename->setEnabled(true);
  }
  else
  {
    ui->actionDelete->setEnabled(false);
    ui->actionRename->setEnabled(false);
  }

  if(ui->dirList->currentIndex().isValid())
  {
    ui->actionNewFolder->setEnabled(true);
    ui->actionFolderRename->setEnabled(ui->dirList->currentIndex() != dirFilterModel->rootFilterIndex());
    ui->actionDeleteFolder->setEnabled(ui->dirList->currentIndex() != dirFilterModel->rootFilterIndex());
  }
  else
  {
    ui->actionNewFolder->setEnabled(false);
    ui->actionFolderRename->setEnabled(false);
    ui->actionDeleteFolder->setEnabled(false);
  }

  if(ui->fileList->currentIndex().isValid() || ui->dirList->currentIndex().isValid())
  {
    ui->actionShowExplorer->setEnabled(true);
  }
  else
  {
    ui->actionShowExplorer->setEnabled(false);
  }
}

void ResourceBrowser::setPath(const QString &path)
{
  QModelIndex index = fsModel->setRootPath(path);

  dirFilterModel->setRootIndex(index);
  fileFilterModel->setRootIndex(index);

  ui->dirList->setRootIndex(dirFilterModel->mapFromSource(index.parent()));
  ui->fileList->setRootIndex(fileFilterModel->mapFromSource(index));

  updateUi();
}

void ResourceBrowser::iconLoaded(const QString &filePath)
{
  QModelIndex index = fsModel->index(filePath);

  if(index.isValid())
  {
    fsModel->setIconProvider(iconProvider);
    ui->fileList->metaObject()->invokeMethod(ui->fileList, "_q_layoutChanged");
    // ui->fileList->update(fileFilterModel->mapFromSource(index));
    // ui->fileList->setRootIndex(ui->fileList->rootIndex());
  }
}

void ResourceBrowser::on_dirList_doubleClicked(const QModelIndex &index)
{
}

void ResourceBrowser::on_fileList_doubleClicked(const QModelIndex &index)
{
  QString resourceFileName = fsModel->filePath(fileFilterModel->mapToSource(index));
  ResourceEditor* resourceEditor = ResourceEditor::findOrOpenEditor(resourceFileName);

  if(resourceEditor)
  {
    resourceEditor->show();
    resourceEditor->activateWindow();
  }
  else
  {
    QMessageBox::warning(this, "Open file", QString("Failed to open %1").arg(resourceFileName));
  }
}
void ResourceBrowser::on_dirList_clicked(const QModelIndex &folderIndex)
{
  iconProvider->clearPendingList();

  QModelIndex index = dirFilterModel->mapToSource(folderIndex);

  fileFilterModel->setRootIndex(index);

  ui->fileList->clearSelection();
  ui->fileList->setRootIndex(fileFilterModel->mapFromSource(index));

  ui->dirList->expand(folderIndex);

  ui->statusbar->clearMessage();

  updateUi();
}

void ResourceBrowser::on_actionNew_triggered()
{
  QHash<QString, const QMetaObject*> metaObjects;

  foreach(const QMetaObject* metaObject, ClassInfo::metaObjects(&Resource::staticMetaObject))
  {
    if(!metaObject->constructorCount())
      continue;
    if(ResourceEditor::resourceEditor(metaObject))
      metaObjects[metaObject->className()] = metaObject;
  }

  if(metaObjects.isEmpty())
    return;

  QInputDialog inputDialog(NULL);

  inputDialog.setWindowTitle("New file");
  inputDialog.setLabelText("Select type:");
  inputDialog.setOption(QInputDialog::UseListViewForComboBoxItems);
  inputDialog.setComboBoxItems(metaObjects.keys());

  if(inputDialog.exec() == QDialog::Accepted)
  {
    if(inputDialog.textValue().isEmpty())
      return;

    if(!metaObjects.contains(inputDialog.textValue()))
      return;

    QSharedPointer<Resource> newResource(static_cast<Resource*>(metaObjects[inputDialog.textValue()]->newInstance()));

    if(ResourceEditor* newEditor = ResourceEditor::openEditor(newResource))
    {
      newEditor->show();
      newEditor->activateWindow();
    }
  }
}

void ResourceBrowser::on_actionOpen_triggered()
{
  QString resourceFileName = QFileDialog::getOpenFileName(this, "Open File", Resource::getResourceDir().absolutePath(), ResourceImporter::importFilters());

  if(resourceFileName.isEmpty())
    return;

  if(ResourceEditor* newEditor = ResourceEditor::openEditor(resourceFileName))
  {
    newEditor->show();
    newEditor->activateWindow();
  }
}

void ResourceBrowser::on_actionDelete_triggered()
{
  QModelIndex index = fileFilterModel->mapToSource(ui->fileList->currentIndex());

  if(!index.isValid())
    return;

  QFileInfo info = fsModel->fileInfo(index);

  if(!info.exists())
    return;

  if(QMessageBox::question(this, info.baseName(), "Are you sure?", QMessageBox::Yes|QMessageBox::No, QMessageBox::No) ==
     QMessageBox::Yes)
  {
    if(!fsModel->remove(index))
    {
      QMessageBox::warning(this, info.completeBaseName(), "Failed to delete!");
    }
  }
}

void ResourceBrowser::on_actionRename_triggered()
{
  QModelIndex index = fileFilterModel->mapToSource(ui->fileList->currentIndex());

  if(!index.isValid())
    return;

  QFileInfo info = fsModel->fileInfo(index);

  if(!info.exists())
    return;

  QString newName = QInputDialog::getText(this, info.completeBaseName(), "Rename file", QLineEdit::Normal, info.completeBaseName());
  if(newName.isEmpty())
    return;

  if(!fsModel->setData(index, newName + "." + info.suffix(), Qt::EditRole))
  {
    QMessageBox::warning(this, info.completeBaseName(), "Failed to rename!");
  }
}

void ResourceBrowser::on_actionListView_toggled(bool arg1)
{
  if(arg1)
    ui->fileList->setViewMode(QListView::ListMode);
  else
    ui->fileList->setViewMode(QListView::IconMode);
}

void ResourceBrowser::on_actionNewFolder_triggered()
{
  QModelIndex index = dirFilterModel->mapToSource(ui->dirList->currentIndex());

  if(!index.isValid())
    return;

  QFileInfo info = fsModel->fileInfo(index);

  if(!info.exists())
    return;

  QString newFolder = QInputDialog::getText(this, info.completeBaseName(), "New folder", QLineEdit::Normal, QString());
  if(newFolder.isEmpty())
    return;

  fsModel->mkdir(index, newFolder);
}

void ResourceBrowser::on_actionDeleteFolder_triggered()
{
  QModelIndex index = dirFilterModel->mapToSource(ui->dirList->currentIndex());

  if(!index.isValid())
    return;

  QFileInfo info = fsModel->fileInfo(index);

  if(!info.exists())
    return;

  if(QMessageBox::question(this, info.baseName(), "Are you sure?", QMessageBox::Yes|QMessageBox::No, QMessageBox::No) ==
     QMessageBox::Yes)
  {
    fsModel->rmdir(index);
  }
}

void ResourceBrowser::on_actionFolderRename_triggered()
{
  QModelIndex index = dirFilterModel->mapToSource(ui->dirList->currentIndex());

  if(!index.isValid())
    return;

  QFileInfo info = fsModel->fileInfo(index);

  if(!info.exists())
    return;

  QString newName = QInputDialog::getText(this, info.completeBaseName(), "Rename folder", QLineEdit::Normal, info.completeBaseName());
  if(newName.isEmpty())
    return;

  fsModel->setData(index, newName, Qt::EditRole);
}

void ResourceBrowser::on_fileList_clicked(const QModelIndex &fileIndex)
{
  if(fileIndex.isValid())
  {
    QModelIndex index = fileFilterModel->mapToSource(fileIndex);

    QFileInfo info = fsModel->fileInfo(index);
    QString name = Resource::relativeFilePath(info.filePath());
    QString type = fsModel->type(index);

    ui->statusbar->showMessage(QString("%1 [%2]").arg(name, type));
  }
  else
  {
    ui->statusbar->clearMessage();
  }
}

void ResourceBrowser::on_actionShowExplorer_triggered()
{
  QString filePath;
  bool select = false;

  if(ui->fileList->currentIndex().isValid())
  {
    filePath = fsModel->filePath(fileFilterModel->mapToSource(ui->fileList->currentIndex()));
    select = true;
  }
  else if(ui->dirList->currentIndex().isValid())
  {
    filePath = fsModel->filePath(dirFilterModel->mapToSource(ui->dirList->currentIndex()));
    select = false;
  }
  else
  {
    return;
  }

#ifdef Q_WS_MAC
  QStringList args;
  args << "-e";
  args << "tell application \"Finder\"";
  args << "-e";
  args << "activate";
  args << "-e";
  args << "select POSIX file \""+filePath+"\"";
  args << "-e";
  args << "end tell";
  QProcess::startDetached("osascript", args);
#endif

#ifdef Q_WS_WIN
  QStringList args;
  if(select)
    args << "/select," << QDir::toNativeSeparators(filePath);
  else
    args << QDir::toNativeSeparators(filePath);
  QProcess::startDetached("explorer", args);
#endif
}

void ResourceBrowser::on_actionSelectFolder_triggered()
{
  QString newFolder = QFileDialog::getExistingDirectory(this, "Select folder", Resource::getResourceDir().absolutePath());
  if(newFolder.isEmpty())
    return;

  setPath(newFolder);
}
