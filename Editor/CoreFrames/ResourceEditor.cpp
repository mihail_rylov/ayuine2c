#include "ResourceEditor.hpp"
#include "ui_ResourceEditor.h"
#include "ProgressStatusDialog.hpp"

#include <CoreTypes/EditableProperty.hpp>

#include <Core/Resource.hpp>
#include <Core/ResourceImporter.hpp>
#include <Core/ClassInfo.hpp>

#include <QFileDialog>
#include <QInputDialog>
#include <QList>

static QList<ResourceEditor*>& activeResourceEditors()
{
  static QList<ResourceEditor*> activeResourceEditors;
  return activeResourceEditors;
}

ResourceEditor::ResourceEditor(QWidget *parent) :
    QMainWindow(parent), ui(new Ui::ResourceEditor())
{
  ui->setupUi(this);
  activeResourceEditors().append(this);
  ui->propertyGrid->registerCustomPropertyCB(EditableProperty::createPropertyType);
}

ResourceEditor::~ResourceEditor()
{
  activeResourceEditors().removeOne(this);
  delete ui;
}

void ResourceEditor::updateUi()
{
  QString className;
  QString objectName;

  if(resource())
  {
    className = resource()->metaObject()->className();
    objectName = resource()->objectName();
    ui->action_Save->setDisabled(resource()->isLocal());
    ui->action_SaveAs->setDisabled(false);
  }
  else
  {
    className = resourceType()->className();
    ui->action_Save->setDisabled(true);
    ui->action_SaveAs->setDisabled(true);
  }

  if(objectName.isNull())
    setWindowTitle(className);
  else
    setWindowTitle(QString("%1 [%2]").arg(className, objectName));
}

void ResourceEditor::newFile()
{
  QHash<QString, const QMetaObject*> metaObjects;

  foreach(const QMetaObject* metaObject, ClassInfo::metaObjects(&Resource::staticMetaObject))
  {
    if(!metaObject->constructorCount())
      continue;
    if(resourceEditor(metaObject))
      metaObjects[metaObject->className()] = metaObject;
  }

  if(metaObjects.isEmpty())
    return;

  QInputDialog inputDialog(NULL);

  // inputDialog.setCaption("New file");
  inputDialog.setLabelText("Select type:");
  inputDialog.setOption(QInputDialog::UseListViewForComboBoxItems);
  inputDialog.setComboBoxItems(metaObjects.keys());

  if(inputDialog.exec() == QDialog::Accepted)
  {
    if(inputDialog.textValue().isEmpty())
      return;

    if(!metaObjects.contains(inputDialog.textValue()))
      return;

    QSharedPointer<Resource> newResource(static_cast<Resource*>(metaObjects[inputDialog.textValue()]->newInstance()));

    // if(!setResource(newResource))
    {
      if(ResourceEditor* newEditor = openEditor(newResource))
      {
        newEditor->show();
      }
    }
  }
}

ResourceEditor* ResourceEditor::openFile(QWidget* parent)
{
  QString resourceFileName = QFileDialog::getOpenFileName(parent, "Open File", Resource::getResourceDir().absolutePath(), ResourceImporter::importFilters());
  if(resourceFileName.isEmpty())
    return NULL;
  return findOrOpenEditor(resourceFileName);
}

void ResourceEditor::saveFile()
{
  if(resource().isNull())
    return;

  ProgressStatusDialog progressDialog("Saving...", QString(), this);
  progressDialog.show();

  resource()->saveFile();
}

void ResourceEditor::saveAsFile()
{
  if(resource().isNull())
    return;

  QString saveFile = QFileDialog::getSaveFileName(this, "Save File", Resource::getResourceDir().absolutePath(), ResourceImporter::exportFilters(resourceType()));
  if(saveFile.isEmpty())
    return;

  ProgressStatusDialog progressDialog("Saving...", QString(), this);
  progressDialog.show();

  resource()->saveFile(saveFile);
  updateUi();
}

QHash<const QMetaObject *, const QMetaObject *> & ResourceEditor::resourceEditors()
{
  static QHash<const QMetaObject *, const QMetaObject *> editors;
  return editors;
}

bool ResourceEditor::registerEditor(const QMetaObject *resourceMetaObject, const QMetaObject *editorMetaObject)
{
  if(resourceEditors().contains(resourceMetaObject))
    return false;

#if 0
  if(editorMetaObject->constructorCount() == 0)
  {
    qDebug() << "[EDITOR] Class doesn't define contructors " << editorMetaObject->className();
    return false;
  }
#endif

  // qDebug() << "[EDITOR] Registering " << editorMetaObject->className() << " for " << resourceMetaObject->className();

  resourceEditors()[resourceMetaObject] = editorMetaObject;
  return true;
}

void ResourceEditor::unregisterEditor(const QMetaObject *editorMetaObject)
{
  if(!editorMetaObject)
    return;

  // qDebug() << "[EDITOR] Unregistering " << editorMetaObject->className();

  foreach(const QMetaObject* resourceMetaObject, resourceEditors().keys(editorMetaObject))
  {
    resourceEditors().remove(resourceMetaObject);
  }
}

const QMetaObject * ResourceEditor::resourceEditor(const QMetaObject *metaObject)
{
  while(metaObject)
  {
    if(resourceEditors().contains(metaObject))
      return resourceEditors()[metaObject];
    metaObject = metaObject->superClass();
  }
  return NULL;
}

ResourceEditor* ResourceEditor::openEditor(const QString& fileName)
{  
  ProgressStatusDialog progressDialog("Loading...", QString(), NULL);
  progressDialog.show();

  QSharedPointer<Resource> newResource = Resource::load(fileName);
  if(newResource.isNull())
    return NULL;

  if(ResourceEditor* newEditor = openEditor(newResource))
  {
    newEditor->show();
    return newEditor;
  }
  return NULL;
}

ResourceEditor* ResourceEditor::openEditor(const QSharedPointer<Resource> &resource)
{
  if(resource.isNull())
    return NULL;

  const QMetaObject* editorMetaObject = resourceEditor(resource->metaObject());
  if(!editorMetaObject)
    return NULL;

  QScopedPointer<ResourceEditor> editor(static_cast<ResourceEditor*>(editorMetaObject->newInstance()));
  if(editor.isNull())
    return NULL;

  if(editor->setResource(resource))
  {
    editor->show();
    return editor.take();
  }
  return NULL;
}

void ResourceEditor::closeEvent(QCloseEvent *)
{
  delete this;
}

void ResourceEditor::setPropertyObject(QObject *object)
{
  if(object == NULL)
  {
    ui->propertyGrid->setObject(resource().data());
  }
  else
  {
    ui->propertyGrid->setObject(object);
  }
}

void ResourceEditor::openFile()
{
  openFile(this);
}

void ResourceEditor::showEvent(QShowEvent *event)
{
  QMainWindow::showEvent(event);
  updateUi();
}

ResourceEditor * ResourceEditor::findEditor(const QString &fileName)
{
  QFileInfo fileInfo(fileName);

  foreach(ResourceEditor* resourceEditor, activeResourceEditors())
  {
    if(!resourceEditor->resource())
      continue;

    QString resourceFileName = resourceEditor->resource()->fileName();
    if(resourceFileName.isEmpty())
      continue;

    QFileInfo resourceFileInfo(resourceFileName);
    if(fileInfo == resourceFileInfo)
      return resourceEditor;
  }

  return NULL;
}

ResourceEditor * ResourceEditor::findOrOpenEditor(const QString &fileName)
{
  if(ResourceEditor* resourceEditor = findEditor(fileName))
  {
    return resourceEditor;
  }

  if(ResourceEditor* resourceEditor = openEditor(fileName))
  {
    return resourceEditor;
  }

  return NULL;
}
