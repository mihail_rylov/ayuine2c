TEMPLATE = lib
TARGET = Editor

include(../ayuine2c.pri)

LIBS += -lMath -lCore -lRender -lPipeline

HEADERS += \
		Editor.hpp \
    CameraController/PerspectiveController.hpp \
    CameraController/OrthoController.hpp \
    CameraController/ModelViewController.hpp \
    CameraController/FirstPersonController.hpp \
    CameraController/CameraController.hpp \
    CoreFrames/ResourceEditor.hpp \
    CoreFrames/FrameEditor.hpp \
    CoreTypes/Vec4Property.hpp \
    CoreTypes/Vec3Property.hpp \
    CoreTypes/Vec2Property.hpp \
    CoreTypes/SphereProperty.hpp \
    CoreTypes/ResourceProperty.hpp \
    CoreTypes/qmetaobjectbuilder_p.h \
		CoreTypes/ObjectProperty.hpp \
    CoreTypes/EulerProperty.hpp \
    CoreTypes/EditableProperty.hpp \
    CoreTypes/BoxProperty.hpp \
    PropertyEditor/QVariantDelegate.h \
    PropertyEditor/QPropertyModel.h \
    PropertyEditor/QPropertyEditorWidget.h \
    PropertyEditor/PropertyCategory.hpp \
    PropertyEditor/Property.h \
    PropertyEditor/EnumProperty.h \
    PropertyEditor/ColorCombo.h \
    CoreFrames/ResourcePreviewGenerator.hpp \
		CoreFrames/ResourceIconProvider.hpp \
    CoreFrames/ResourceBrowser.hpp \
    CoreFrames/ProgressStatusDialog.hpp \
		CoreFrames/MetaObjectItemModel.hpp \
    CoreFrames/LogView.hpp

SOURCES += \
		Editor.cpp \
    CameraController/PerspectiveController.cpp \
    CameraController/OrthoController.cpp \
    CameraController/ModelViewController.cpp \
    CameraController/FirstPersonController.cpp \
    CameraController/CameraController.cpp \
    CoreFrames/ResourceEditor.cpp \
    CoreFrames/FrameEditor.cpp \
    CoreTypes/Vec4Property.cpp \
    CoreTypes/Vec3Property.cpp \
    CoreTypes/Vec2Property.cpp \
    CoreTypes/SphereProperty.cpp \
    CoreTypes/ResourceProperty.cpp \
    CoreTypes/qmetaobjectbuilder.cpp \
		CoreTypes/ObjectProperty.cpp \
    CoreTypes/EulerProperty.cpp \
    CoreTypes/EditableProperty.cpp \
    CoreTypes/BoxProperty.cpp \
    PropertyEditor/QVariantDelegate.cpp \
    PropertyEditor/QPropertyModel.cpp \
    PropertyEditor/QPropertyEditorWidget.cpp \
    PropertyEditor/PropertyCategory.cpp \
    PropertyEditor/Property.cpp \
    PropertyEditor/EnumProperty.cpp \
    PropertyEditor/ColorCombo.cpp \
    CoreFrames/ResourcePreviewGenerator.cpp \
		CoreFrames/ResourceIconProvider.cpp \
    CoreFrames/ResourceBrowser.cpp \
    CoreFrames/ProgressStatusDialog.cpp \
		CoreFrames/MetaObjectItemModel.cpp \
    CoreFrames/LogView.cpp

FORMS += \
    CoreFrames/ResourceEditor.ui \
		CoreFrames/FrameEditor.ui \
		CoreFrames/ResourceBrowser.ui \
    CoreFrames/LogView.ui

RESOURCES += \
		CoreEditor/Images.qrc
