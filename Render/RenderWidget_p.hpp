#ifndef QRENDERWIDGET_P_HPP
#define QRENDERWIDGET_P_HPP

#include "Render_p.hpp"

#include <d3d9.h>

struct IDirect3DSwapChain9;

class RenderComponent;

class RenderWidgetPrivate
{
public:
  boost::intrusive_ptr<IDirect3DSwapChain9> m_swapChain;
  boost::intrusive_ptr<IDirect3DSurface9> m_renderTarget, m_depthTarget;
  D3DPRESENT_PARAMETERS m_d3dpp;
  QVector<RenderComponent*> m_components;
  bool m_swapBefore;

public:
  RenderWidgetPrivate();
  ~RenderWidgetPrivate();
};

#endif // QRENDERWIDGET_P_HPP
