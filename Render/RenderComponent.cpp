#include "RenderComponent.hpp"
#include "RenderComponentWidget.hpp"

RenderComponent::RenderComponent()
{
  m_renderWidget = NULL;
}

RenderComponent::~RenderComponent()
{
  if(m_renderWidget)
  {
    m_renderWidget->removeComponent(this);
    Q_ASSERT(m_renderWidget == NULL);
  }
}

int RenderComponent::beginOrder()
{
  return -1;
}

int RenderComponent::drawOrder()
{
  return 0;
}

bool RenderComponent::begin()
{
  return true;
}

void RenderComponent::resizeEvent(const QSize &newSize)
{
  m_size = newSize;
}

RenderComponentWidget * RenderComponent::renderWidget() const
{
  return m_renderWidget;
}

const QSize & RenderComponent::size() const
{
  return m_size;
}

bool RenderComponent::setWidgetRenderTarget()
{
  if(m_renderWidget)
    return m_renderWidget->setRenderTarget();
  return false;
}

bool RenderComponent::setWidgetDepthTarget()
{
  if(m_renderWidget)
    return m_renderWidget->setDepthTarget();
  return false;
}

RenderTarget * RenderComponent::depthTarget() const
{
  return NULL;
}

RenderTarget * RenderComponent::normalTarget() const
{
  return NULL;
}

QStringList RenderComponent::toStringList() const
{
  return QStringList();
}
