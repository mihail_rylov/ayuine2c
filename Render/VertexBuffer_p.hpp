#ifndef VERTEXBUFFER_P_HPP
#define VERTEXBUFFER_P_HPP

#include "Render_p.hpp"
#include "VertexBuffer.hpp"

struct IDirect3DVertexBuffer9;

class VertexBufferPrivate
{
public:
  boost::intrusive_ptr<IDirect3DVertexBuffer9> m_handle;
  unsigned m_count;
  unsigned m_stride;
  bool m_dynamic;
  bool m_locked;
  unsigned m_offset;

public:
  VertexBufferPrivate();
  ~VertexBufferPrivate();
  bool create(unsigned count, unsigned stride, bool dynamic);
  bool create(const void* data, unsigned count, unsigned stride);
  void* lock(unsigned offset, unsigned count, bool nooverwrite = false);
  void unlock();
};

#endif // VERTEXBUFFER_P_HPP
