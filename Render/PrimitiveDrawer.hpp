#ifndef PRIMITIVEDRAWER_HPP
#define PRIMITIVEDRAWER_HPP

#include <QObject>
#include <QDataStream>
#include <QMetaType>

class BasePrimitiveDrawer
{
  Q_GADGET
  Q_ENUMS(PrimitiveTopology)

public:
  enum PrimitiveTopology
  {
    Unknown,
    PointList,
    LineList,
    LineStrip,
    TriList,
    TriStrip,
    TriFan
  };
  enum PrimitiveType
  {
    Undefined,
    Vertices,
    Indices
  };

public:
  PrimitiveType Type;
  PrimitiveTopology Topology;

public:
  BasePrimitiveDrawer()
  {
    Type = Undefined;
    Topology = Unknown;
  }
};

class PrimitiveDrawer : public BasePrimitiveDrawer
{
  Q_GADGET

public:
  quint32 StartVertex;
  quint32 PrimitiveCount;

public:
  PrimitiveDrawer()
  {
    Type = Vertices;
    StartVertex = 0;
    PrimitiveCount = 0;
  }
  PrimitiveDrawer(PrimitiveTopology topology, quint32 startVertex, quint32 primitiveCount)
  {
    Type = Vertices;
    Topology = topology;
    StartVertex = startVertex;
    PrimitiveCount = primitiveCount;
  }

public:
  friend QDataStream& operator << (QDataStream& ds, const PrimitiveDrawer& drawer)
  {
    return ds << (const unsigned&)drawer.Topology << drawer.StartVertex << drawer.PrimitiveCount;
  }
  friend QDataStream& operator >> (QDataStream& ds, PrimitiveDrawer& drawer)
  {
    return ds >> (unsigned&)drawer.Topology >> drawer.StartVertex >> drawer.PrimitiveCount;
  }
};

Q_DECLARE_METATYPE(PrimitiveDrawer)

class IndexedPrimitiveDrawer : public BasePrimitiveDrawer
{
  Q_GADGET

public:
  qint32 BaseIndex;
  quint32 MinIndex;
  quint32 NumVertices;
  quint32 StartIndex;
  quint32 PrimitiveCount;

public:
  IndexedPrimitiveDrawer()
  {
    Type = Indices;
    BaseIndex = 0;
    MinIndex = 0;
    NumVertices = 0;
    StartIndex = 0;
    PrimitiveCount = 0;
  }
  IndexedPrimitiveDrawer(PrimitiveTopology topology, quint32 minIndex, quint32 numVertices, quint32 startIndex, quint32 primitiveCount, qint32 baseIndex = 0)
  {
    Type = Indices;
    Topology = topology;
    BaseIndex = baseIndex;
    MinIndex = minIndex;
    NumVertices = numVertices;
    StartIndex = startIndex;
    PrimitiveCount = primitiveCount;
  }

public:
  friend QDataStream& operator << (QDataStream& ds, const IndexedPrimitiveDrawer& drawer)
  {
    return ds << (const unsigned&)drawer.Topology << drawer.BaseIndex << drawer.MinIndex << drawer.NumVertices << drawer.StartIndex << drawer.PrimitiveCount;
  }
  friend QDataStream& operator >> (QDataStream& ds, IndexedPrimitiveDrawer& drawer)
  {
    return ds >> (unsigned&)drawer.Topology >> drawer.BaseIndex >> drawer.MinIndex >> drawer.NumVertices >> drawer.StartIndex >> drawer.PrimitiveCount;
  }
};

Q_DECLARE_METATYPE(IndexedPrimitiveDrawer)

#endif // PRIMITIVEDRAWER_HPP
