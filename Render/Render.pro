TEMPLATE = lib
TARGET = Render

DEPLOYFILES += ../lib/dll/d3dx9_31.dll \
    ../lib/dll/d3dx9_39.dll

include(../ayuine2c.pri)

HEADERS += \
    IndexBuffer.hpp \
    IndexBuffer_p.hpp \
    Render.hpp \
    RenderSystem.hpp \
    VertexBuffer.hpp \
    VertexBuffer_p.hpp \
    Render_p.hpp \
    RenderSystem_p.hpp \
    PrimitiveDrawer.hpp \
    RenderWidget.hpp \
    RenderWidget_p.hpp \
    GraphicsFont.hpp \
    RenderTarget.hpp \
    Texture.hpp \
    Texture_p.hpp \
    RenderTarget_p.hpp \
    RenderComponent.hpp \
    RenderComponentWidget.hpp \
    RenderShaderConst.hpp \
    TriStripper/tri_stripper.h \
    TriStripper/stdafx.h \
    TriStripper/heap_array.h \
    TriStripper/graph_array.h \
    TriStripper/cache_simulator.h \
    RenderShader.hpp \
    RenderShader_p.hpp \
    RenderTexture.hpp \
    RenderTexture_p.hpp \
    RenderQuery.hpp \
    RenderQuery_p.hpp

SOURCES += \
    IndexBuffer.cpp \
    Render.cpp \
    RenderSystem.cpp \
    VertexBuffer.cpp \
    Texture.cpp \
    RenderWidget.cpp \
    GraphicsFont.cpp \
    RenderTarget.cpp \
    RenderComponent.cpp \
    RenderComponentWidget.cpp \
    PrimitiveDrawer.cpp \
    BoxShape.cpp \
    SphereShape.cpp \
    QuadShape.cpp \
    ConeShape.cpp \
    TriStripper/tri_stripper.cpp \
    RenderShader.cpp \
    RenderTexture.cpp \
    RenderQuery.cpp

DISTFILES += ../lib/dll/d3dx9_31.dll \
    ../lib/dll/d3dx9_39.dll

LIBS += -ld3d9 -ld3dx9 -ldxerr9 -lMath -lCore -lUser32 -lGdi32

RESOURCES += \
    Resources.qrc

OTHER_FILES += \
    SimpleShader.fx \
    chandelier.dds
