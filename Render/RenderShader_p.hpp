#ifndef RENDERSHADER_P_HPP
#define RENDERSHADER_P_HPP

#include "Render_p.hpp"

#include <QDataStream>
#include <QHash>
#include <QVector>
#include <QBitArray>

struct IDirect3DVertexShader9;
struct IDirect3DPixelShader9;
struct ID3DXConstantTable;
typedef struct _D3DXCONSTANT_DESC D3DXCONSTANT_DESC;

struct ShaderCompiled
{
public:
  QByteArray m_vertexCode, m_pixelCode;

public:
  operator bool () const
  {
    return m_vertexCode.length() != 0 && m_pixelCode.length() != 0;
  }

public:
  friend QDataStream& operator << (QDataStream& ds, const ShaderCompiled& compiled)
  {
    QHash<QByteArray, QByteArray> values;
    values["DirectX9 VertexShader"] = compiled.m_vertexCode;
    values["DirectX9 PixelShader"] = compiled.m_pixelCode;
    ds << values;
    return ds;
  }
  friend QDataStream& operator >> (QDataStream& ds, ShaderCompiled& compiled)
  {
    QHash<QByteArray, QByteArray> values;
    ds >> values;
    compiled.m_vertexCode = values["DirectX9 VertexShader"];
    compiled.m_pixelCode = values["DirectX9 PixelShader"];
    return ds;
  }
};

class ShaderConst;
class RenderTexture;

struct ShaderConstDesc
{  
  enum Type
  {
    Undefined,
    Float4,
    Sampler
  };

  QByteArray name;
  Type type;
  int vertexOffset;
  int vertexSize;
  int vertexCount;
  int pixelOffset;
  int pixelSize;
  int pixelCount;
  QByteArray defaultValue;

  ShaderConstDesc()
  {
    vertexOffset = -1;
    vertexSize = 0;
    pixelOffset = -1;
    pixelSize = 0;
    type = Undefined;
  }
  bool operator == (const ShaderConstDesc& other) const
  {
    return name == other.name;
  }
  bool operator < (const ShaderConstDesc& other) const
  {
    return name < other.name;
  }
};

struct ShaderSamplerDesc
{
  QByteArray name;
  int vertexIndex;
  int pixelIndex;

  ShaderSamplerDesc()
  {
    vertexIndex = -1;
    pixelIndex = -1;
  }
  bool operator == (const ShaderSamplerDesc& other) const
  {
    return name == other.name;
  }
  bool operator < (const ShaderSamplerDesc& other) const
  {
    return name < other.name;
  }
};

class RenderShaderPrivate : public QObject
{
  Q_OBJECT

public:
  ShaderCompiled m_compiled;
  boost::intrusive_ptr<IDirect3DVertexShader9> m_vertexShader;
  boost::intrusive_ptr<IDirect3DPixelShader9> m_pixelShader;
  boost::intrusive_ptr<ID3DXConstantTable> m_vertexShaderConsts;
  boost::intrusive_ptr<ID3DXConstantTable> m_pixelShaderConsts;
  QVector<ShaderConstDesc> m_shaderConsts;
  QVector<ShaderSamplerDesc> m_shaderSamplers;
  QBitArray m_shaderUsedConsts;
  QBitArray m_shaderUsedSamplers;

  enum Type
  {
    PixelShader,
    VertexShader
  };

public:
  RenderShaderPrivate();
  ~RenderShaderPrivate();
  bool create();
  bool create(const QByteArray& shaderCompiled);
  bool createFromCode(const QByteArray& shaderCode, const QByteArray& shaderTechnique);
  bool createFromFile(const QString& shaderFile, const QByteArray& shaderTechnique);

public slots:
  void unlink(QObject* object);

private:
  void processSampler(const D3DXCONSTANT_DESC& desc, Type type);
  void processConst(const D3DXCONSTANT_DESC& desc, Type type, ShaderConstDesc::Type constType);
  void process(ID3DXConstantTable* constantTable, Type type);
};

#endif // RENDERSHADER_P_HPP
