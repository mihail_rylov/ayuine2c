#ifndef RENDER_HPP
#define RENDER_HPP

#include <QtGlobal>

#ifdef RENDER_EXPORTS
#define RENDER_EXPORT Q_DECL_EXPORT
#else
#define RENDER_EXPORT Q_DECL_IMPORT
#endif

#endif // RENDER_HPP
