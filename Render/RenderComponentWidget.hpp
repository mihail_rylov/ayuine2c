#ifndef RENDERCOMPONENTWIDGET_HPP
#define RENDERCOMPONENTWIDGET_HPP

#include "RenderWidget.hpp"

class RenderComponent;
class RenderTarget;
class RenderQuery;

class RENDER_EXPORT RenderComponentWidget : public QRenderWidget
{
  Q_OBJECT

public:
  explicit RenderComponentWidget(QWidget *parent = 0);
  ~RenderComponentWidget();

protected:
  void resizeEvent(QResizeEvent *);
  void showEvent(QShowEvent *);

public:
  bool render();

signals:
  virtual void prepareComponents();
  virtual void finalizeComponents();

public:
  const QVector<RenderComponent *> &components() const;
  void setComponents(const QVector<RenderComponent *> &m_components);
  bool addComponent(QScopedPointer<RenderComponent> &renderComponent);
  bool addComponent(RenderComponent *renderComponent);
  bool removeComponent(RenderComponent *renderComponent);
  bool removeComponent(unsigned index);

public:
  virtual RenderTarget* depthTarget() const;
  virtual RenderTarget* normalTarget() const;
  virtual QStringList toStringList() const;

public:
  float drawTime() const;

private:
  QVector<RenderComponent *> m_components;
  QScopedPointer<RenderQuery> m_event;
  float m_drawTime;
};

#endif // RENDERCOMPONENTWIDGET_HPP
